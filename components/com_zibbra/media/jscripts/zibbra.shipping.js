(function() {
	
	Bpost = function() {
	
		this._uri = null;
		this._settings = {};
		
	}; // end function
	
	Bpost.prototype.uri = function(uri) {
	
		this._uri = uri;
	
	}; // end function
	
	Bpost.prototype.set = function(key,value) {
	
		if(typeof(this._settings[key])==="object") {
		
			this._settings[key].push(value);
		
		}else if(typeof(this._settings[key])==="string") {
	
			var cur = this._settings[key];
			this._settings[key] = [];
			this._settings[key].push(cur);
			this._settings[key].push(value);
		
		}else{
	
			this._settings[key] = value;
		
		} // end if
	
	}; // end function
	
	Bpost.prototype.submit = function() {
	
		var form = new Element("form#bpost_frontend_form",{
			method: "post",
			action: this._uri,
			target: "bpost_frontend_frame"
		});
		
		for(var i in this._settings) {
	
			if(typeof(this._settings[i])==="object") {
		
				for(var j=0; j<this._settings[i].length; j++) {
					
					new Element("input",{
						type: "hidden",
						name: i,
						value: this._settings[i][j]
					}).inject(form);					
				
				} // end for
		
			}else{

				new Element("input",{
					type: "hidden",
					name: i,
					value: this._settings[i]
				}).inject(form);
			
			} // end if
		
		} // end for
		
		$(document.body).inject(form,"after");
		form.submit();
		
	}; // end function
	
	Bpost.prototype.dismiss = function() {
		
		location.href = "index.php?option=com_zibbra&task=checkout.confirm";
	
	}; // end function
	
})(); // end class