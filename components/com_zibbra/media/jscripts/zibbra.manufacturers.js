(function() {
	
	Zibbra.Manufacturers = function() {
		
	}; // end function
	
	Zibbra.Manufacturers.prototype.dispatch = function() {
		
		$$("#zibbra-manufacturers > ul.tabs > li").addEvent("click",function(e) {
			
	         var id = this.get("html");
	         
	         $$("#zibbra-manufacturers > ul.tabs > li").removeClass("selected");
	         $$("#zibbra-manufacturers > div.list").hide();
	         $("zibbra-manufacturers-"+id).show();
	         this.addClass("selected");
	         
	    }); // end click
		
	}; // end function
	
})(); // end class

zibbra.registerModule(new Zibbra.Manufacturers());