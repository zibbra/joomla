/*!
 * zibbra.slider.js
 */ 
(function() {

    Zibbra.Slider = function(options) {
    
        this.selector = typeof(options.selector)==="string" ? options.selector : ".slider";
        this.min = typeof(options.min)==="number" ? options.min : 0;
        this.max = typeof(options.max)==="number" ? options.max : 100;
        this.step = typeof(options.step)==="number" ? options.step : 1;
        this.range = typeof(options.range)==="boolean" ? options.range : false;
        this.input = {
        	min: typeof(options.input_min)!=="undefined" ? options.input_min : null,
            max: typeof(options.input_max)!=="undefined" ? options.input_max : null
        };
        this.onChange = typeof(options.onChange)==="function" ? options.onChange : function() { return false; };
        this.values = {
            min: typeof(options.value_min)==="number" ? options.value_min : this.min,
            max: typeof(options.value_max)==="number" ? options.value_max : this.max
        };
        this._handles = {
        	min: null,
        	max: null
        };
        this._container = null;
        this._indicator = null;
        this._ratio = null;
        this._el = null;
    
    }; // end function
    
    Zibbra.Slider.prototype.init = function() {
    
        this._el = $$(this.selector);
        
        if(this._el!==null && this._el.length==1 && (this._el=this._el[0])!==false) {

        	this._calculateRatio();
        	this._drawContainer();
        	this._drawHandles();
        	this._drawIndicator();
        	this._registerHandlers();
        	this._updateHandles();    		
        	this._updateIndicator();
        	this._updateInputs();
            
            return this;
        
        } // end if
    
        return false;
    
    }; // end function
    
    Zibbra.Slider.prototype._calculateRatio = function() {
    	
    	var width = this._el.clientWidth;
    	var height = this._el.clientHeight;
    	var range = this.max - this.min;
    	
    	this._ratio = range / (width - 16);
    	
    }; // end function
    
    Zibbra.Slider.prototype._drawContainer = function() {
        
        this._container = new Element("div",{
        	"class": "slider-container",
        	styles: {
	        	position: "absolute",
	        	zIndex: 996
        	}
        }).inject(this._el);
    	
    }; // end function
    
    Zibbra.Slider.prototype._drawHandles = function() {
        
        this._handles.min = new Element("div",{
        	"class": "slider-handle slider-handle-min",
        	styles: {
	        	position: "absolute",
	        	zIndex: 998
        	}
        }).inject(this._el);
        
        this._handles.max = new Element("div",{
        	"class": "slider-handle slider-handle-max",
        	styles: {
	        	position: "absolute",
	        	zIndex: 999
        	}
        }).inject(this._el);
    
    }; // end function
    
    Zibbra.Slider.prototype._drawIndicator = function() {
        
        this._indicator = new Element("div",{
        	"class": "slider-indicator",
        	styles: {
	        	position: "absolute",
	        	zIndex: 997
        	}
        }).inject(this._el);
    
    }; // end function
    
    Zibbra.Slider.prototype._registerHandlers = function() {

    	var self = this;
    	var width = this._el.clientWidth;
    	//var height = this._el.clientHeight;
    	var limit = width - 16;
    	
        new Drag(this._handles.min,{
        	limit: {
        		x: [0,limit],
        		y: [0,0]
        	},
        	onComplete: function() {
        		self.onChange(self.values.min,self.values.max);
        	},
        	onDrag: function() {
                
                var left = self._handles.min.offsetLeft - 1;
                var min = Math.round(self.min + (left * self._ratio));
        		
        		if(min>=self.min) {
            		
            		if(min<self.values.max) {
                        
                		self.values.min = min;
            		
            		}else{
            			
    	        		self.values.min = self.values.max - 1;
    	        		self._updateHandles();
            			
            		} // end if
        		
        		}else{
        			
	        		self.values.min = self.min;
        			
        		} // end if
        		
        		self._updateIndicator();
        		self._updateInputs();
        		
        }}); // end drag
        
        new Drag(this._handles.max,{
        	limit: {
        		x: [0,limit],
        		y: [0,0]
        	},
        	onComplete: function() {
        		self.onChange(self.values.min,self.values.max);
        	},
        	onDrag: function() {
                
                var left = self._handles.max.offsetLeft;
                var max = Math.round(self.min + (left * self._ratio));
                
        		self.values.max = max;
        		
        		if(max<=self.max) {
            		
            		if(max>=self.values.min) {
                        
                		self.values.max = max;
            		
            		}else{
            			
    	        		self.values.max = self.values.min;
    	        		self._updateHandles();
            			
            		} // end if
        		
        		}else{
        			
	        		self.values.max = self.max;
        			
        		} // end if
        		
        		self._updateIndicator();
        		self._updateInputs();
        		
        }}); // end drag
        
        if(this.input.min!==null) {
        	
        	$$(this.input.min).addEvent("change",function() {
        		
        		if($(this).value>=self.min) {
            		
            		if($(this).value<=self.values.max) {
            		
    	        		self.values.min = $(this).value;
            		
            		}else{
            			
    	        		self.values.min = self.values.max;
    	        		self._updateInputs();
            			
            		} // end if
        		
        		}else{
        			
	        		self.values.min = self.min;
	        		self._updateInputs();
        			
        		} // end if
        		
        		self._updateIndicator();
        		self._updateHandles();
        		
        	}); // end change
        	
        } // end if
        
        if(this.input.max!==null) {

        	$$(this.input.max).addEvent("change",function() {
        		
        		if($(this).value<=self.max) {
            		
            		if($(this).value>=self.values.min) {
            		
    	        		self.values.max = $(this).value;
            		
            		}else{
            			
    	        		self.values.max = self.values.min;
    	        		self._updateInputs();
            			
            		} // end if
        		
        		}else{
        			
	        		self.values.max = self.max;
	        		self._updateInputs();
        		
        		} // end if
        		
        		self._updateIndicator();
        		self._updateHandles();
        		
        	}); // end change
        	
        } // end if
    
    }; // end function
    
    Zibbra.Slider.prototype._updateIndicator = function() {
    	
    	var left = Math.round((this.values.min - this.min) / this._ratio) + (this._el.clientHeight / 2) + 2;
    	var max = (this.values.max - this.min) / this._ratio;
    	var width = Math.round(max - left + (this._el.clientHeight / 2) + 2);
    
        this._indicator.set("styles",{left:left+"px",width:width+"px"});
    
    }; // end function
    
    Zibbra.Slider.prototype._updateHandles = function() {
    	
    	var min = Math.round((this.values.min - this.min) / this._ratio);
    	var max = Math.round((this.values.max - this.min) / this._ratio);
    	
    	this._handles.min.set("styles",{left:min+"px"});
    	this._handles.max.set("styles",{left:max+"px"});
    
    }; // end function
    
    Zibbra.Slider.prototype._updateInputs = function() {
        
        if(this.input.min!==null) {
        	
        	var min = this.values.min+"";
        	
        	$$(this.input.min).set("value",this.values.min).set("size",(min.length>1 ? min.length - 1 : 1));
        	
        } // end if
        
        if(this.input.max!==null) {
        	
        	var max = this.values.max+"";

        	$$(this.input.max).set("value",this.values.max).set("size",(max.length>1 ? max.length - 1 : 1));
        	
        } // end if
        
        this._updateLabels();
    
    }; // end function
    
    Zibbra.Slider.prototype._updateLabels = function() {
    	
    	var range = this.max - this.min;
    	var ratio = range / 100;
    	var percentage = {
    		left: Math.round((this.values.min - this.min) / ratio),
    		min: 0,
    		middle: 0,
    		max: 0,
    		right: Math.round((range - (this.values.max - this.min)) / ratio)
    	};
    	
    	percentage.middle = 100 - (percentage.left + percentage.right);
    	
    	var row = $$(this.input.min).getParent().getParent().getParent();
    	var row_width = row.getComputedSize()[0].totalWidth;
    	var min_width = $$(this.input.min).getComputedSize()[0].totalWidth;
    	var max_width = $$(this.input.max).getComputedSize()[0].totalWidth;
    	
    	percentage.min = Math.round((min_width / row_width) * 100);
    	percentage.max = Math.round((max_width / row_width) * 100);    	
    	percentage.middle = percentage.middle - (percentage.min + percentage.max);

    	row.getElement("td.slider-left-spacer").set("width",percentage.left+"%");
    	row.getElement("td.slider-min").set("width",percentage.min+"%");
    	row.getElement("td.slider-middle-spacer").set("width",percentage.middle+"%");
    	row.getElement("td.slider-max").set("width",percentage.max+"%");
    	row.getElement("td.slider-right-spacer").set("width",percentage.right+"%");
    
    }; // end function

})(); // end class