(function() {
	
	Zibbra.Checkout = function() {
		
		this.shipping = null;
		this.payment = null;
		this.comments = null;
		
	}; // end function
	
	Zibbra.Checkout.prototype.dispatch = function() {

		this._initForm();
		this._initShipping();
		this._initPayment();
		this._initComments();
		this.checkForm();
		
	}; // end function
	
	Zibbra.Checkout.prototype._initForm = function() {
		
		var self = this;
		
		$("zibbra-checkout-form").addEvent("submit",function() {
			
			self._showLoading();
			
			return true;
			
		}); // end onsubmit
		
	}; // end function
	
	Zibbra.Checkout.prototype._initShipping = function() {
		
		var self = this;
		var shipping = $("zibbra-checkout-shipping");
		var values = shipping.getElements("input:checked");
		var toggles = shipping.getElements("input");
		
		if(values.length) {
			
			this.shipping = values[0].value;
			
		}else{
			
			this.shipping = null;
			
		} // end if
		
		toggles.addEvent("click",function() {
			
			if(self.shipping!==this.value) {
				
				self.shipping = this.value;
				self.update();
				
			} // end if
			
		}); // end click
		
	}; // end function
	
	Zibbra.Checkout.prototype._initPayment = function() {

		var self = this;
		var payment = $("zibbra-checkout-payment");
		var values = payment.getElements("input:checked");
		var toggles = payment.getElements("input");
		
		if(values.length) {
			
			this.payment = values[0].value;
			
		}else{
			
			this.payment = null;
			
		} // end if
		
		toggles.addEvent("click",function() {
			
			if(self.payment!==this.value) {
				
				self.payment = this.value;
				self.update();
				
			} // end if
			
		}); // end click
		
	}; // end function
	
	Zibbra.Checkout.prototype._initComments = function() {
		
		var self = this;
		var comments = $("zibbra-checkout-comments");
		
		if(comments!==null) {
		
			var textarea = comments.getElements("textarea");
			
			if(textarea!==null) {
			
				textarea.addEvent("chnage",function() { self._onChangeComments(this.value); }); // end chnage
				textarea.addEvent("keyup",function() { self._onChangeComments(this.value); }); // end keyup
				textarea.addEvent("blur",function() { self._onChangeComments(this.value); }); // end blur
			
			} // end if
		
		} // end if
		
	}; // end function
	
	Zibbra.Checkout.prototype._onChangeComments = function(value) {
		
		if(this.comments!==value) {
			
			this.comments = value;
			
		} // end if
		
	}; // end function
	
	Zibbra.Checkout.prototype.update = function() {
		
		var self = this;
		
		var request = new Request({
			url: "/index.php?option=com_zibbra&task=checkout.update",
			data: {
				"shipping": this.shipping,
				"payment": this.payment,
				"comments": this.comments
			},
			method: "post",
			onSuccess: function(response) {
				
				self._onUpdate(response);
				
			},
			onFailure: function(xhr) { console.log(xhr); }
		});
		
		this._showLoading();
		
		request.send();
		
	}; // end function
	
	Zibbra.Checkout.prototype._showLoading = function() {

		var overlay = new Element("div",{class:"overlay loading"});
		var text = new Element("div",{html:zibbra._("loading")}).inject(overlay);
		
		$$("#zibbra-checkout").grab(overlay,"top");
		
		var ph = overlay.getSize().y;
		var dh = text.getSize().y;
		var mh = (ph - dh) / 2;
		
		text.set('styles',{ 'top':mh });
		
	}; // end function
	
	Zibbra.Checkout.prototype._onUpdate = function(response) {
		
		//$$("#main").set("html",response);
		$$("#zibbra-checkout").getParent().set("html",response);
		
		this.dispatch();
		
	}; // end function
	
	Zibbra.Checkout.prototype.checkForm = function() {
		
		var submit = $("zibbra-checkout-confirm").getElement("input[type='submit']");
		var login = $("username");
		
		if(login!=null || this.shipping==null || this.payment==null) {
			
			submit.set("disabled","disabled").addClass("disabled");
			return false;
			
		}else{
			
			submit.removeProperties("disabled").removeClass("disabled");			
			return true;
			
		} // end if
		
	}; // end function	
	
})(); // end class

zibbra.registerModule(new Zibbra.Checkout());