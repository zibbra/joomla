(function() {
	
	Zibbra.Catalog = function() {
		
		this.slider = null;
		this.params = {
			price: null,
			properties: null,
			manufacturers: null,
			in_stock: null,
			page: null,
			limit: null,
			ajax: null,
			st: null,
			sd: null
		};
		
	}; // end function
	
	Zibbra.Catalog.prototype.dispatch = function() {
		
		this._initState();
		this._initSlider();
		this._initProperties();
		this._initManufacturers();
		this._initStock();
		this._initSort();
		this._initLimit();
		this._updatePagination();
		
	}; // end function
	
	Zibbra.Catalog.prototype._initState = function() {

		var uri = new URI(location.href);
		var data = uri.getData();
		
		for(var i in data) {
			
			if(typeof(this.params[i])!="undefined") {
				
				if(i=="properties") {
					
					this.params.properties = {};
					
					for(var id in data.properties) {
						
						this.params.properties[id] = [];
						
						for(var value in data.properties[id]) {
							
							this.params.properties[id].push(data.properties[id][value]);
							
						} // end for
						
					} // end for
					
				}else{
				
					this.params[i] = data[i];
				
				} // end if
				
			} // end if
			
		} // end if
		
	}; // end function
	
	Zibbra.Catalog.prototype._initSlider = function() {
		
		var self = this;
		
		var options = {
			selector: "#price_range > div.slider-controls",
			input_min: "#price_min",
			input_max: "#price_max",
			min: parseInt($$("#price_min").get("value")[0]),
			max: parseInt($$("#price_max").get("value")[0]),
			onChange: function(min,max) {
				self._onChangePrice(min,max);
			}
		};
		
		if(this.params.price!==null) {
			
			var price = this.params.price.split("-");
			
			options.value_min = parseInt(price[0]);
			options.value_max = parseInt(price[1]);
			
		} // end if
		
		this.slider = new Zibbra.Slider(options).init();
		
	}; // end function
	
	Zibbra.Catalog.prototype._initProperties = function() {
		
		var self = this;
		var properties = $$("div.zibbra-filter.list");
		
		for(var i=0; i<properties.length; i++) {
			
			if(properties[i].id!="manufacturer" && properties[i].id!="in_stock") {

				var options = properties[i].getElements("div.list > input");
				var count = 0;
				
				for(var j=0; j<options.length; j++) {
					
					var propertyid = options[j].get("name");
					
					if(this.params.properties!==null && typeof(this.params.properties[propertyid])!="undefined") {
	
						var value = options[j].get("value");
						var ischecked = this.params.properties[propertyid].indexOf(value)>=0;
						
						if(ischecked) {
							
							options[j].set("checked","checked");
							count++;
							
						} // end if
						
					} // end if
					
					options[j].addEvent("click",function() {
						
						self._onSelectProperty(this);
						
					}); // end click
					
					// Check if count = 0
					
					var inputid = options[j].get("id");
					var label = options[j].getParent().getElement("label[for='"+inputid+"']");
					var labelCount = parseInt(label.getElement("span.suffix").getElement("span.count").get("html"));
					
					if(labelCount==0) {
		
						options[j].set("disabled","disabled").addClass("disabled");
						label.addClass("disabled");
		
					} // end if
					
				} // end for
				
				if(count==0 && Zibbra.Catalog.FILTERS_COLLAPSED) {
					
					if(typeof(properties[i].fx)==="undefined") {
						
						properties[i].fx = new Fx.Slide(properties[i].getElement("div.list"),{duration:1000,transition:Fx.Transitions.Pow.easeOut});
						
					} // end if
					
					properties[i].fx.hide();
					
					properties[i].addClass("collapsed");
					
				} // end if
			
			} // end if
			
		} // end for
		
		for(var i=0; i<properties.length; i++) {
			
			//if(Zibbra.Catalog.FILTERS_COLLAPSED) {
			
				var list = properties[i].getElement("div.list");
				
				if(list!==null) {
					
					properties[i].fx = new Fx.Slide(properties[i].getElement("div.list"),{duration:1000,transition:Fx.Transitions.Pow.easeOut});
					
					properties[i].getElement("div.header").addEvent("click",function() {
						
						this.getParent().fx.toggle();
							
							this.getParent().toggleClass("collapsed");
						
					}); // end click
					
				} // end if
			
			//} // end if
			
		} // end for
		
	}; // end function
	
	Zibbra.Catalog.prototype._initManufacturers = function() {
		
		if($("manufacturer")!==null) {

			var self = this;
			var options = $("manufacturer").getElements("div.list > input");
			
			if(this.params.manufacturers!==null||1) {
				
				for(var i=0; i<options.length; i++) {

					var key = options[i].getAttribute("id").match(/\d+$/gi);
					var label = $("manufacturer").getElement("label[for='manufacturer-"+key+"']");
					var count = parseInt(label.getElement("span.suffix").getElement("span.count").get("html"));
					
					if(!count) {
						
						options[i].set("disabled","disabled");
						label.addClass("disabled");
						
					} // end if
					
					for(var j in this.params.manufacturers) {
						
						if(parseInt(this.params.manufacturers[j])==parseInt(options[i].get("value"))) {
							
							options[i].set("checked","checked");
							break;
							
						} // end if
						
					} // end for
					
				} // end for
				
			} // end if
			
			options.addEvent("click",function() {
				
				self._onSelectManufacturer(this);
				
			}); // end click
			
		} // end if
		
	}; // end function
	
	Zibbra.Catalog.prototype._initStock = function() {
		
		if($("in_stock")!==null) {

			var self = this;
			var options = $("in_stock").getElements("div.list > input");
			
			if(this.params.in_stock!==null) options.set("checked","checked");
			
			options.addEvent("click",function() {
				
				self._onSelectStock(this);
				
			}); // end click
			
		} // end if
		
	}; // end function
	
	Zibbra.Catalog.prototype._initLimit = function() {
		
		var self = this;
		
		if($("zibbra-catalog-limit")!==null) {
		
			$("zibbra-catalog-limit").set("value",this.params.limit!==null ? this.params.limit : Zibbra.Catalog.DEFAULT_LIMIT);
			
			if($("zibbra-catalog-limit").get("value")=="") {
				
				$$("#zibbra-catalog-limit > option:last-of-type").set("selected","selected");
				
			} // end if
			
			$("zibbra-catalog-limit").addEvent("change",function() {
				
				self.params.limit = parseInt(this.get("value"));
				self._updateCatalog();
				
			}); // end onChange
		
		} // end if
		
	}; // end function
	
	Zibbra.Catalog.prototype._initSort = function() {
		
		var self = this;

		var sort = $("zibbra-catalog-sort");
		
		if(sort!==null) {
			
			if(this.params.st===null) {
				
				this.params.st = Zibbra.Catalog.DEFAULT_SORT_TYPE;
				this.params.sd = Zibbra.Catalog.DEFAULT_SORT_DIR;
				
			} // end if
		
			var links = $("zibbra-catalog-sort").getElements("a");
			
			if(links!==null) {
				
				if(!$("zibbra-catalog-sort-"+this.params.st).hasClass("active")) {
	
					$("zibbra-catalog-sort").getElements("a").removeClass("active");
					$("zibbra-catalog-sort-"+this.params.st).addClass("active");
				
				} // end if
				
				if(!$("zibbra-catalog-sort-"+this.params.st).hasClass(this.params.sd)) {
	
					$("zibbra-catalog-sort-"+this.params.st).removeClass("asc").removeClass("desc").addClass(this.params.sd);
				
				} // end if
	
				for(var i=0; i<links.length; i++) {
					
					links[i].addEvent("click",function() {
						
						if(!this.hasClass("active")) {
							
							$("zibbra-catalog-sort").getElements("a").removeClass("active");
							
							this.addClass("active");
							this.addClass("asc");
							self.params.sd = "asc";
							
						}else{
							
							if(this.hasClass("asc")) {
								
								this.addClass("desc").removeClass("asc");
								self.params.sd = "desc";
								
							}else{
								
								this.addClass("asc").removeClass("desc");
								self.params.sd = "asc";
								
							} // end if
							
						} // end if
						
						self.params.st = this.get("id").replace("zibbra-catalog-sort-","");
	
						self._updateCatalog();
						
					}); // end onClick
					
				} // end for
			
			} // end if
		
		} // end if
		
	}; // end function
	
	Zibbra.Catalog.prototype._onChangePrice = function(min,max) {
		
		this.params.price = min+"-"+max;

		this._updateCatalog();
		
	}; // end function
	
	Zibbra.Catalog.prototype._onSelectProperty = function(el) {
		
		var propertyid = el.get("name");
		var value = el.get("value");
		var checked = el.get("checked");
		
		if(checked) {
			
			if(this.params.properties===null) {
				
				this.params.properties = {};
				
			} // end if
			
			if(typeof(this.params.properties[propertyid])=="undefined") {
				
				this.params.properties[propertyid] = [];
				
			} // end if
			
			this.params.properties[propertyid].push(value);
			
		}else{
			
			if(typeof(this.params.properties[propertyid])!="undefined") {
				
				this.params.properties[propertyid].splice(this.params.properties[propertyid].indexOf(value),1);
				
				if(this.params.properties[propertyid].length==0) {
					
					delete this.params.properties[propertyid];
					
				} // end if
				
				if(Object.keys(this.params.properties).length==0) {
					
					this.params.properties = null;
					
				} // end if
				
			} // end if
			
		} // end if

		this._updateCatalog();
		
	}; // end function
	
	Zibbra.Catalog.prototype._onSelectManufacturer = function(el) {
		
		var toggles = el.getParent().getElements("input");
		
		this.params.manufacturers = [];
		
		for(var i=0; i<toggles.length; i++) {
		
			if(toggles[i].get("checked")) {
				
				this.params.manufacturers.push(toggles[i].get("value"));
				
			} // end if

		} // end for
		
		this._updateCatalog();
		
	}; // end function
	
	Zibbra.Catalog.prototype._onSelectStock = function(el) {
		
		if(el.get("checked")) {
			
			this.params.in_stock = "Y";
			
		}else{
			
			this.params.in_stock = null;
			
		} // end if

		this._updateCatalog();
		
	}; // end function
	
	Zibbra.Catalog.prototype._getURI = function(o) {
		
		var uri = new URI(location.href);
		var params = Object.clone(this.params);
		
		if(typeof(o)!=="undefined") for(var i in o) params[i] = o[i];
		
		uri.set("data",params);
		/*
		console.log("Original link: "+location.href);
		console.log("New link with params: "+uri.toString());
		*/
		return uri;
		
	}; // end function
	
	Zibbra.Catalog.prototype._updateCatalog = function() {
		
		var self = this;
		
		this._updateState();
		
		var request = new Request({
			url: this._getURI({ajax:true}),
			method: "get",
			onSuccess: function(response) {
				
				//$$("#main").set("html",response);
				$$("#zibbra-catalog-table").getParent().set("html",response);
				
				self._updatePagination();
				self._initSlider();
				self._initProperties();
				self._initManufacturers();
				self._initStock();
				self._initSort();
				self._initLimit();
				
				if(Zibbra.Catalog._arrCallback.length>0) {
					
					for(var i=0; i<Zibbra.Catalog._arrCallback.length; i++) {
						
						Zibbra.Catalog._arrCallback[i](this);
						
					} // end for		
					
				} // end if
				
			},
			onFailure: function(xhr) { if(typeof(console)!="undefined") console.log(xhr); }
		});
		
		this._showLoading();
		
		request.send();
		
	}; // end function
	
	Zibbra.Catalog.prototype._showLoading = function() {

		var overlay = new Element("div",{"class":"overlay loading"});
		var text = new Element("div",{html:zibbra._("loading")}).inject(overlay);
		
		$$("#zibbra-catalog").grab(overlay,"top");
		
		var ph = overlay.getSize().y;
		var dh = text.getSize().y;
		var mh = (ph - dh) / 2;
		
		text.set('styles',{ 'top':mh });
		
	}; // end function
	
	Zibbra.Catalog.prototype._updateState = function() {
		
		if(typeof(window.history.replaceState)!="undefined") window.history.replaceState("catalog","",this._getURI().toString());
		
	}; // end function
	
	Zibbra.Catalog.PAGES = null;
	
	Zibbra.Catalog.prototype._updatePagination = function() {
		
		var self = this;
		var timer = null;
		var done = false;
		var container = $("pagination-page");
		
		if(container!==null) {
			
			if(this.params.page===null) {
				
				this.params.page = 1;
				
			} // end if
			
			$("pagination-page").set("value",this.params.page);
			
			var onchange = function() {
				
				if(done) return;
				
				if(timer!==null) {
					
					clearTimeout(timer);
					timer = null;
					
				} // end if
				
				var page = parseInt(this.get("value"));
				
				if(page>Zibbra.Catalog.PAGES) {

					page = Zibbra.Catalog.PAGES;
					this.set("value",Zibbra.Catalog.PAGES);
					
				} // end if
				
				if(page<1) {

					page = 1;
					this.set("value",1);
					
				} // end if
				
				setTimeout(function() { done = false; },500);
				
				timer = setTimeout(function() {
					
					var params = {
						page: page,
						st: self.params.st,
						sd: self.params.sd
					};
					
					var uri = self._getURI(params).toString();
					
					location.href = uri;
					
				},500);
				
			}; // end function
			
			$("pagination-page").addEvent("change",onchange);
			$("pagination-page").addEvent("blur",onchange);
			$("pagination-page").addEvent("keyup",onchange);
			
		} // end if
		
		/*
		 * 
		 * $(pagination-page)
		
		var links = $$("#main div.pagination > a");
		
		if(links.length>0) {
		
			var current = parseInt($$("#main div.pagination > span.current").get("html"));
			var max = 0;
			
			for(var i=0; i<links.length; i++) {
				
				var page = links[i].get("class");
				
				switch(page) {
	
					case "page-first": page = 1;break;
					case "page-previous": page = current - 1;break;
					case "page": {
						var value = parseInt(links[i].get("html"));
						page = value;
						if(value>max) max = value;
					};break;
					case "page-next": page = current + 1;break;
					case "page-last": page = max;break;
				
				}; // end switch
				
				links[i].set("href",this._getURI({page:page}).toString());
				
			} // end for
		
		} // end if
		
		*/
		
	}; // end function
	
	Zibbra.Catalog.registerCallback = function(func) {
		
		Zibbra.Catalog._arrCallback.push(func);
		
	}; // end function
	
	Zibbra.Catalog._arrCallback = [];
	Zibbra.Catalog.FILTERS_COLLAPSED = true;
	Zibbra.Catalog.DEFAULT_LIMIT = 5;
	Zibbra.Catalog.DEFAULT_SORT_TYPE = "times_sold";
	Zibbra.Catalog.DEFAULT_SORT_DIR = "desc";
	
})(); // end class

zibbra.registerModule(new Zibbra.Catalog());