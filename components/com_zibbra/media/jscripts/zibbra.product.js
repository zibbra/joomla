(function() {
	
	Zibbra.Product = function() {
		
		this._images = [];
		this._variations = {length:0};
		this._variationsCount = null;
		
	}; // end function
	
	Zibbra.Product.prototype.dispatch = function() {
		
		this._initAlbum();
		this._initVariations();
		
	}; // end function
	
	Zibbra.Product.prototype._initAlbum = function() {
		
		var self = this;
		
		this._images = $("zibbra-product").getElements("div.zibbra-product-album-thumbnail");
		
		for(var i=0; i<this._images.length; i++) {
			
			var id = this._images[i].id.replace("zibbra-product-image-thumbnail-","");
			var thumb = $("zibbra-product-image-thumbnail-"+id);
			
			thumb.addEvent("click",function() {
				
				self._onClickImage(this);
				
			}); // end click
			
		} // end for
		
		$("zibbra-product-image-preview").addEvent("mouseenter",function() {
			
			self._onZoomImage(this);
			
		}).addEvent("mouseleave",function() {
			
			self._onStopZoom(this);
			
		}); // end events
		
	}; // end function
	
	Zibbra.Product.prototype._onClickImage = function(img) {
		
		if(Zibbra.Product.URI!=null) {
		
			var id = img.id.replace("zibbra-product-image-thumbnail-","");			
			var uri = Zibbra.Product.URI+"?id="+id+"&type=product&width=300&height=300";
			
			$("zibbra-product-image-preview").set("src",uri);
			
			for(var i=0; i<this._images.length; i++) {
				
				$(this._images[i].id).removeClass("selected");
				
			} // end for
			
			$("zibbra-product-image-thumbnail-"+id).addClass("selected");
		
		} // end if
		
	}; // end function
	
	Zibbra.Product.prototype._onZoomImage = function(img) {
		
		var zoom = $("zibbra-product-image-zoom");
		
		if(zoom==null) {
			
			zoom = new Element("div",{
				id: "zibbra-product-image-zoom"
			}).setStyle("display","none");
			
			zoom.inject($("zibbra-product")).set("reveal");
			
		} // end if
		
		var uri = $("zibbra-product-image-preview").src.replace(/300/g,600);
		var dim = $("zibbra-product-image-preview").getCoordinates();
		var pos = $("zibbra-product-image-preview").getPosition();
		var img = new Element("img",{src:uri}).inject(zoom);
		
		zoom.setStyle("height",dim.height);
		
		zoom.reveal();
		
		document.removeEvent("mousemove").addEvent("mousemove",function(e) {
			
			var x = (e.page.x - pos.x) * -1;
			var y = (e.page.y - pos.y) * -1;
			
			img.setStyle("left",x);
			img.setStyle("top",y);
			
		}); // end mousemove
		
	}; // end function
	
	Zibbra.Product.prototype._onStopZoom = function(img) {
		
		document.removeEvents("mousemove");
		
		var container = $("zibbra-product-image-zoom");
		
		if(container!==null) {
		
			container.set("tween",{
			    onComplete: function(el) {
			    	el.remove();
			    }
			});
			
			container.fade(0);
		
		} // end if
		
	}; // end function
	
	Zibbra.Product.prototype._initVariations = function() {
		
		var self = this;
		var container = $("zibbra-product-variations");
		
		if(container!==null) {
			
			var variations = container.getElements("select");
			
			if(variations.length>0) {
				
				// Store count
				
				this._variationsCount = variations.length;
				
				// Disable button
				
				$$("input.zibbra-product-add-to-cart").set("disabled","disabled").addClass("disabled");
				
				// Add change event on the dropdowns
				
				for(var i=0; i<variations.length; i++) {
					
					variations[i].addEvent("change",function() {
						
						self._onChangeVariation(this.get("id").replace("variations_",""),this.value);
						
					}); // end click
					
				} // end for
				
			} // end if
			
		} // end if
		
	}; // end function
	
	Zibbra.Product.prototype._onChangeVariation = function(id,value) {
		
		var self = this;
		
		// Disable button
		
		$$("input.zibbra-product-add-to-cart").set("disabled","disabled").addClass("disabled");
		
		// Update stored variations object
		
		id = parseInt(id);
		
		if(value=="") {
			
			if(typeof(this._variations[id])!="undefined") {
				
				delete(this._variations[id]);
				this._variations.length--;
				
			} // end if
			
		}else{
			
			if(typeof(this._variations[id])=="undefined") {

				this._variations.length++;
				
			} // end if
			
			this._variations[id] = parseInt(value);
			
		} // end if
		
		// Enable button
		
		if(this._variations.length==this._variationsCount) {
			
			$$("input.zibbra-product-add-to-cart").set("disabled",null).removeClass("disabled");
			
		}else{
			
			// Load the variation combinations
			
			var data = null;
			
			if(this._variations.length>0) {
				
				data = Object.clone(this._variations);
				delete(data.length);
				
			} // end if
			
			var request = new Request.JSON({
				url: "/index.php?option=com_zibbra&task=product.getVariations",
				data: {
					"id": $("productid").value,
					"variations": data
				},
				method: "post",
				onSuccess: function(response) {
					
					self._onLoadVariations(response);
					
				},
				onFailure: function(xhr) { console.log(xhr); }
			});
			
			// Disable dropdowns
			
			$("zibbra-product-variations").getElements("select").set("disabled","disabled");
			
			request.send();
			
		} // end if
		
	}; // end function
	
	Zibbra.Product.prototype._onLoadVariations = function(response) {
		
		for(var i=0; i<response.length; i++) {
			
			var cbo = $("variations_"+response[i].id);
			
			cbo.set("html","");
			
			new Element("option",{"value":"","html":"Selecteer een optie"}).inject(cbo);
			
			for(var j=0; j<response[i].options.length; j++) {
				
				var option = new Element("option",{"value":response[i].options[j].id,"html":response[i].options[j].name}).inject(cbo);
				
				if(typeof(this._variations[response[i].id])!="undefined" && this._variations[response[i].id]==response[i].options[j].id) {
					
					option.set("selected","selected");
					
				} // end if
				
				if(response[i].options[j].in_stock!=="Y") {
					
					option.set("disabled","disabled");
					
				} // end if
				
			} // end for
			
			// Enable dropdown again
			
			cbo.set("disabled",null);
			
		} // end for
		
	}; // end function
	
	Zibbra.Product.URI = null;
	
})(); // end class

zibbra.registerModule(new Zibbra.Product());