(function() {
	
	Zibbra.Register = function() {
		
	}; // end function
	
	Zibbra.Register.prototype.dispatch = function() {
		
		document.formvalidator.setHandler("passwordverify",function(value) {
			
	        return ($("jform_account_new_password").get("value") == value);
	        
	    });
		
		document.formvalidator.setHandler("website",function(value) {
			
			regex = new RegExp("^(http[s]?:\\/\\/(www\\.)?)([0-9A-Za-z-\\.@:%_\+~#=]+)+((\\.[a-zA-Z]{2,3})+)(/(.)*)?(\\?(.)*)?");
			return regex.test(value);
			
		});
		
		document.formvalidator.setHandler("phone",function(value) {
			
			//regex = new RegExp("/^[0-9\\.\\-\\+\\s\\/\\(\\)]{6,}$");
			regex = new RegExp("^\\+\\d{1,4}\\s{0,1}\\(\\d{1,4}\\)\\s{0,1}\\d{5,11}$");
			return regex.test(value);
			
		});
		
		document.formvalidator.setHandler("mobile",function(value) {
			
			regex = new RegExp("^\\+\\d{1,4}\\s{0,1}\\(\\d{1,4}\\)\\s{0,1}\\d{5,11}$");
			return regex.test(value);
			
		});
		
		document.formvalidator.setHandler("zipcode",function(value) {
			
			return (value.match(/^[a-z0-9 \-]{4,}$/i)!==null) && (value.match(/[\d]/g).length>=3);
			
		});
		
		var toggle = function() {
			
			var elements = $("jform_shipping_toggle").getParent().getParent().getElements("dd.text,dt.text,dd.country,dt.country");
			elements.toggle();
			
		};
		
		$("jform_shipping_toggle").addEvent("click",function() {
			
			toggle();
			
			var fields = ["jform_shipping_street","jform_shipping_streetnr","jform_shipping_zipcode","jform_shipping_city"];
			
			for(var i=0; i<fields.length; i++) {
				
				var field = $(fields[i]);
				var id = field.get("id");
				var label = $(id+"-lbl");
				
				if(this.get("checked")) {
					
					field.removeClass("required");
					
				}else{
					
					field.addClass("required");
					label.getParent().getElements("span.optional").addClass("star").removeClass("optional").set("html"," *").setStyle("color","red");
					
				} // end if
				
			} // end for
			
			var form = $("zibbra-register-account");
			
			document.formvalidator.isValid(form);
			
		}); // end click
		
		toggle();
		
	}; // end function
	
})(); // end class

zibbra.registerModule(new Zibbra.Register());