(function() {
	
	Zibbra.Confirm = function() {
		
	}; // end function
	
	Zibbra.Confirm.prototype.dispatch = function() {

		this._initForm();
		
	}; // end function
	
	Zibbra.Confirm.prototype._initForm = function() {
		
		var self = this;
		
		$("zibbra-confirm-form").addEvent("submit",function() {
			
			self._showLoading();
			
			return true;
			
		}); // end onsubmit
		
	}; // end function
	
	Zibbra.Confirm.prototype._showLoading = function() {

		var overlay = new Element("div",{class:"overlay loading"});
		var text = new Element("div",{html:zibbra._("loading")}).inject(overlay);
		
		$$("#zibbra-checkout").grab(overlay,"top");
		
		var ph = overlay.getSize().y;
		var dh = text.getSize().y;
		var mh = (ph - dh) / 2;
		
		text.set('styles',{ 'top':mh });
		
	}; // end function
	
})(); // end class

zibbra.registerModule(new Zibbra.Confirm());