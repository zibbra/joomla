(function() {
	
	Zibbra = function() {
		
		this._modules = [];
		this._t = {};
		
	}; // end function
	
	Zibbra.prototype._ = function(keyword) {
		
		return this._t[keyword];
		
	}; // end function
	
	Zibbra.prototype.__ = function(keyword,translation) {
		
		this._t[keyword] = translation;
		
	}; // end function
	
	Zibbra.prototype.registerModule = function(module) {
		
		this._modules.push(module);
		
	}; // end function
	
	Zibbra.prototype.dispatch = function() {
		
		this.initForms();
		
		for(var i=0; i<this._modules.length; i++) {
			
			this._modules[i].dispatch();
			
		} // end for
		
	}; // end function
	
	Zibbra.prototype.initForms = function() {		

        var forms = $$("form.form-validate");
        
        for(var i=0; i<forms.length; i++) {
        	
        	var form = forms[i];
        	
        	form.addEvent("submit",function(e) {
				
		        var isValid = document.formvalidator.isValid(this);
		        var formid = this.get("id");

		        if(isValid) {
					
		        	$(formid).submit();
					return true;
		                
		        }else{
					
					e.stop();
		        	
		        	alert(Joomla.JText._("COM_ZIBBRA_JS_ERROR_FORM_INVALID","Some values are unacceptable"));
					return false;
		                
		        } // end if
			
			}); // end onSubmit
                
        } // end for
		
	}; // end function
	
})(); // end class

var zibbra = new Zibbra();

window.addEvent("domready",function() {
	
	zibbra.dispatch();
	
}); // end ready