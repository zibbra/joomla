(function() {
	
	Zibbra.Password = function() {
		
	}; // end function
	
	Zibbra.Password.prototype.dispatch = function() {
		
		document.formvalidator.setHandler("passwordverify",function(value) {
			
	        return ($("jform_password_new_password").get("value") == value) && value!="";
	        
	    });
	
		document.formvalidator.setHandler("password",function(value) {
			
	        return ($("jform_password_old_password").get("value") !== "");
	        
	    });		
		
	}; // end function
	
})(); // end class

zibbra.registerModule(new Zibbra.Password());