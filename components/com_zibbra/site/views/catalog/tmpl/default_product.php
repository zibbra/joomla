<?php

// No direct access to this file

defined("_JEXEC") or die("Restricted access");

$allow_order = $this->item->allowBackorder() || (!$this->item->allowBackorder() && $this->item->isInStock());
$product_url = JRoute::_("index.php?option=com_zibbra&view=product&id=".$this->item->getSlug());

?>
<div class="zibbra-catalog-product product-<?php echo $this->item->getProductid(); ?>">
	<div class="zibbra-catalog-product-wrapper">
		<div class="zibbra-catalog-product-middle">
			<h2 class="zibbra-catalog-product-name"><a href="<?php echo $product_url; ?>"><?php echo $this->item->getName(); ?></a></h2>
			<h3 class="zibbra-catalog-product-code"><?php echo $this->item->getCode(); ?></h3>
			<p class="zibbra-catalog-product-description"><?php echo strip_tags($this->item->getShortDescription()); ?></p>
		</div>
	</div>
	<div class="zibbra-catalog-product-left">
		<?php if($this->item->hasImages()): ?>
			<?php
			
			$image = $this->item->getImageByIndex($this->image);
			
			?>
			<div class="zibbra-catalog-product-image"><a href="<?php echo $product_url; ?>"><img src="<?php echo $image; ?>" border="0" /></a></div>
		<?php endif; ?>
	</div>
	<div class="zibbra-catalog-product-right">
		<p class="zibbra-catalog-product-stock">
			<?php if($this->params->get("show_stock","N")==="Y"): ?>
				<?php echo JText::_("COM_ZIBBRA_STOCK"); ?>: <span class="<?php echo $this->item->isInStock() ? "in_stock" : ""; ?>"><?php echo $this->item->getStock(); ?></span>
			<?php else: ?>
				<?php echo JText::_("COM_ZIBBRA_AVAILABILITY"); ?>: <span class="<?php echo $this->item->isInStock() ? "in_stock" : ""; ?>"><?php echo $this->item->isInStock() ? JText::_("COM_ZIBBRA_IN_STOCK") : JText::_("COM_ZIBBRA_NOT_IN_STOCK"); ?></span>
			<?php endif; ?>
		</p>
		<div class="zibbra-catalog-product-price">
			<?php if($this->item->hasDiscount()): ?>
				<?php echo JText::_("COM_ZIBBRA_FROM"); ?>&nbsp;<s><?php echo $this->item->getValutaSymbol(); ?>&nbsp;<?php echo number_format($this->item->getBasePrice(),2,",",""); ?></s>&nbsp;<?php echo JText::_("COM_ZIBBRA_FOR"); ?>&nbsp;<span><?php echo $this->item->getValutaSymbol(); ?>&nbsp;<?php echo number_format($this->item->getPrice(),2,",",""); ?></span>
			<?php else: ?>
				<span><?php echo $this->item->getValutaSymbol(); ?>&nbsp;<?php echo number_format($this->item->getPrice(),2,",",""); ?></span>
			<?php endif; ?>
		</div>
		<form action="/index.php?option=com_zibbra" method="post" name="zibbra-product-form" id="zibbra-product-form">
			<input type="submit" class="zibbra-catalog-product-add-to-cart<?php if(!$allow_order): ?> disabled<?php endif; ?>" value="<?php echo JText::_("COM_ZIBBRA_ADD_TO_CART"); ?>"<?php if(!$allow_order): ?> disabled="disabled"<?php endif; ?> />
			<input type="hidden" name="task" value="cart.addProduct" />
			<input type="hidden" name="id" value="<?php echo $this->item->getProductid(); ?>" />
			<?php echo JHtml::_("form.token"); ?>
		</form>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>