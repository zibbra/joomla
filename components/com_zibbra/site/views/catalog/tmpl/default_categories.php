<?php

// No direct access to this file

defined("_JEXEC") or die("Restricted access");

// Get the child categories

if($this->category->hasChildren()) {

	$categories = $this->category->getChildren();

} // end if

?>
<?php if($this->category->hasChildren()): ?>
	<?php if($this->category->hasImages()): ?>
		<img src="<?php echo $this->category->getFirstImage(); ?>" border="0" align="right" />
	<?php endif; ?>
	<h2><?php echo $this->category->getName(); ?></h2>
	<?php if($this->category->hasDescription()): ?>
		<div class="zibbra-main-category-description"><?php echo nl2br($this->category->getDescription()); ?></div>
	<?php endif; ?>
	<div id="zibbra-categories">
		<div class="zibbra-categories-row">
			<?php foreach($categories as $index=>$category): ?>
				<?php if($index>0 && $index%4==0): ?>
						<div style="clear:both;"></div>
					</div>
					<div class="zibbra-categories-row">
				<?php endif; ?>
					<?php $link = JRoute::_("index.php?option=com_zibbra&view=catalog&id=".$category->getIdString(),false); ?>
					<div class="zibbra-category">
						<h3><a href="<?php echo $link; ?>"><?php echo $category->getName(); ?></a></h3>
						<?php if($category->hasImages()): ?>
							<table class="zibbra-category-image">
						    	<tr>
									<td align="center" valign="middle"><a href="<?php echo $link; ?>"><img src="<?php echo $category->getFirstImage(); ?>" border="0" /></a></td>
						   		</tr>
							</table>
						<?php endif; ?>
						<a class="button" href="<?php echo $link; ?>"><?php echo JText::_("COM_ZIBBRA_VIEW_PRODUCTS"); ?></a>
					</div>
			<?php endforeach; ?>
			<div style="clear:both;"></div>
		</div>
		<div style="clear:both;"></div>
	</div>
<?php endif; ?>