<?php

// No direct access to this file

defined("_JEXEC") or die("Restricted access");

?>
<?php if(count($this->items)==0 && !is_null($this->category) && $this->category->hasChildren()): ?>

	<?php echo $this->loadTemplate("categories");?>
	
<?php else: ?>

	<table cellpadding="0" cellspacing="0" border="0" width="100%" id="zibbra-catalog-table">
		<tr>
			<?php if(!is_null($this->category)): ?>
				<td valign="top" width="20%" id="zibbra-catalog-filters">
					<?php echo $this->loadTemplate("filters");?>
				</td>
				<td width="20">&nbsp;</td>
			<?php endif; ?>
			<td valign="top">
				<?php echo $this->loadTemplate("header"); ?>
				<div id="zibbra-catalog">
					<?php
					
						foreach($this->items as $item) {
							
							$this->item = &$item;
							echo $this->loadTemplate("product");
									
						} // end foreach
						
						echo $this->loadTemplate("pagination");
						
					?>
				</div>
			</td>
		</tr>
	</table>
	<script>
	Zibbra.Catalog.DEFAULT_LIMIT = <?php echo $this->params->get("default_limit",5); ?>;
	zibbra.__("loading","<?php echo JText::_("COM_ZIBBRA_LOADING_PLEASE_WAIT");?>");
	</script>
	
<?php endif;?>