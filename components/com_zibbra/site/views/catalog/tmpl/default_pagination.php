<?php

// No direct access to this file

defined("_JEXEC") or die("Restricted access");

$uri = "index.php?option=com_zibbra&view=catalog";

if(!is_null($this->category)) {
	
	$uri .= "&id=".$this->category->getIdString();
	
} // end if

if(!is_null($_GET['st'])) {
	
	$uri .= "&st=".$_GET['st']."&sd=".$_GET['sd'];
	
} // end if

if(isset($_GET['manufacturers'])) {
	
	$uri .= "&manufacturers[]=".$_GET['manufacturers'][0];
	
} // end if

?>
<div style="clear:both;"></div>
<?php if($this->pagination['pages']>1): ?>
	<div class="pagination">
		<a class="page-first<?php if($this->pagination['page']<=1): ?> disabled<?php endif; ?>" href="<?php echo $this->pagination['page']>1 ? JRoute::_($uri."&page=1") : "javascript:void(0);"; ?>">
			<span><?php echo JText::_("COM_ZIBBRA_PAGINATION_FIRST"); ?> &lsaquo;</span>
		</a>
		<a class="page-previous<?php if($this->pagination['page']<=1): ?> disabled<?php endif; ?>" href="<?php echo $this->pagination['page']>1 ? JRoute::_($uri."&page=".($this->pagination['page']-1)) : "javascript:void(0);"; ?>">
			<span><?php echo JText::_("COM_ZIBBRA_PAGINATION_PREVIOUS"); ?> &laquo;</span>
		</a>
		<div class="page-switcher">
			<span><?php echo JText::_("COM_ZIBBRA_PAGINATION_PAGE"); ?></span>
			<input id="pagination-page" type="text" value="<?php echo $this->pagination['page']; ?>" />
			<span><?php echo JText::_("COM_ZIBBRA_PAGINATION_OF"); ?> <?php echo $this->pagination['pages']; ?></span>
		</div>
		<a class="page-next<?php if($this->pagination['page']>=$this->pagination['pages']): ?> disabled<?php endif; ?>" href="<?php echo $this->pagination['page']<$this->pagination['pages'] ? JRoute::_($uri."&page=".($this->pagination['page']+1)) : "javascript:void(0);"; ?>">
			<span><?php echo JText::_("COM_ZIBBRA_PAGINATION_NEXT"); ?> &rsaquo;</span>
		</a>
	    <a class="page-last<?php if($this->pagination['page']>=$this->pagination['pages']): ?> disabled<?php endif; ?>" href="<?php echo $this->pagination['page']<$this->pagination['pages'] ? JRoute::_($uri."&page=".$this->pagination['pages']) : "javascript:void(0);"; ?>">
	    	<span><?php echo JText::_("COM_ZIBBRA_PAGINATION_LAST"); ?> &raquo;</span>
	    </a>
	</div>
	<script>
	Zibbra.Catalog.PAGES = <?php echo $this->pagination['pages']; ?>;
	</script>
<?php endif; ?>