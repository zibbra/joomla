<?php

// No direct access to this file

defined("_JEXEC") or die("Restricted access");

?>
<div id="zibbra-catalog-header">
	<?php if(!is_null($this->category) && $this->category->hasImages()): ?>
		<img src="<?php echo $this->category->getFirstImage(); ?>" border="0" />
	<?php endif; ?>
	<?php if(!is_null($this->category)): ?>
		<h1><?php echo $this->category->getName(); ?></h1>
	<?php endif; ?>
	<?php if(is_array($this->manufacturers) && count($this->manufacturers)==1): ?>
		<h1><?php echo $this->manufacturers[0]->getName(); ?></h1>
	<?php endif; ?>
	<?php if(!is_null($this->category) && $this->category->hasDescription()): ?>
		<div class="zibbra-main-category-description"><?php echo $this->category->getDescription(); ?></div>
	<?php endif; ?>
	<div class="clear"></div>
	<div id="zibbra-catalog-header-controls">
		<?php if($this->pagination['total_rows']>5): ?>
			<span><?php echo JText::_("COM_ZIBBRA_LIMIT"); ?>:</span>
			<select id="zibbra-catalog-limit">
				<option value="5"<?php if($this->params->get("default_limit",5)==5): ?> selected="selected"<?php endif; ?>>5</option>
				<?php if($this->pagination['total_rows']>10): ?>
					<option value="10"<?php if($this->params->get("default_limit",5)==10): ?> selected="selected"<?php endif; ?>>10</option>
					<?php if($this->pagination['total_rows']>20): ?>
						<option value="20"<?php if($this->params->get("default_limit",5)==20): ?> selected="selected"<?php endif; ?>>20</option>
						<?php if($this->pagination['total_rows']>50): ?>
							<option value="50"<?php if($this->params->get("default_limit",5)==50): ?> selected="selected"<?php endif; ?>>50</option>
						<?php endif; ?>
					<?php endif; ?>
				<?php endif; ?>
				<option value="<?php echo $this->pagination['total_rows']; ?>"><?php echo JText::_("COM_ZIBBRA_ALL_PRODUCTS"); ?></option>
			</select>
		<?php endif; ?>
		<div id="zibbra-catalog-sort">
			<span><?php echo JText::_("COM_ZIBBRA_SORT_BY"); ?>:</span>
			<?php
				$arrSort = array(
					"times_sold"=>JText::_("COM_ZIBBRA_SORT_BEST_BUY"),
					"name"=>JText::_("COM_ZIBBRA_SORT_NAME"),
					"price"=>JText::_("COM_ZIBBRA_SORT_PRICE"),
					"timestamp_insert"=>JText::_("COM_ZIBBRA_SORT_NEWEST")
				);
			?>
			<?php foreach($arrSort as $type=>$label): ?>
				<?php
					$class = "";
					if($this->sort->type==$type) {
						$class .= "active";
						if($this->sort->dir=="asc") {
							$class .= " asc";
						}else{
							$class .= " desc";
						}
					}
				?>
				<a id="zibbra-catalog-sort-<?php echo $type; ?>" href="javascript:void(0);" class="<?php echo $class; ?>"><?php echo $label; ?></a>
			<?php endforeach; ?>
			<script>
			Zibbra.Catalog.DEFAULT_SORT_TYPE = "<?php echo $this->sort->type; ?>";
			Zibbra.Catalog.DEFAULT_SORT_DIR = "<?php echo $this->sort->dir; ?>";
			</script>
		</div>
	</div>
</div>