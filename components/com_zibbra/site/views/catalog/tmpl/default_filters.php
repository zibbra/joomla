<?php

// No direct access to this file

defined("_JEXEC") or die("Restricted access");

?>
<?php foreach($this->filters as $filter): ?>
	<div id="<?php echo $filter->getField(); ?>" class="zibbra-filter <?php echo $filter->getType(); ?>">
		<?php if($filter->getType()==ZProductFilter::TYPE_RANGE && $filter->getMin()!=$filter->getMax()): ?>
			<div class="header"><span><?php echo $filter->getName(); ?></span></div>
			<div id="<?php echo $filter->getField(); ?>_range" class="slider">
				<div class="slider-controls"></div>
				<table cellpadding="0" cellspacing="0" border="0" class="slider-labels" width="100%">
					<tr>
						<td class="slider-left-spacer"><img src="/templates/zibbra/images/spacer.gif" border="0" /></td>
						<td class="slider-min">
							<div>
								<span class="unit">&euro;</span>
								<input type="text" id="<?php echo $filter->getField(); ?>_min" name="<?php echo $filter->getField(); ?>[min]" value="<?php echo $filter->getMin(); ?>" size="0" />
							</div>
						</td>
						<td width="100%" class="slider-middle-spacer"><img src="/templates/zibbra/images/spacer.gif" border="0" /></td>
						<td class="slider-max">
							<div>
								<span class="unit">&euro;</span>
								<input type="text" id="<?php echo $filter->getField(); ?>_max" name="<?php echo $filter->getField(); ?>[max]" value="<?php echo $filter->getMax(); ?>" size="0" />
							</div>
						</td>
						<td class="slider-right-spacer"><img src="/templates/zibbra/images/spacer.gif" border="0" /></td>
					</tr>
				</table>
			</div>
		<?php endif; ?>
		<?php if($filter->getType()==ZProductFilter::TYPE_LIST && count($filter->getOptions())>0): ?>
			<div class="header"><span><?php echo $filter->getName(); ?></span><div class="icon"></div></div>
			<div id="<?php echo $filter->getField(); ?>_list" class="list">
				<?php foreach($filter->getOptions() as $index=>$option): ?>
					<input id="<?php if($filter->getField()!="manufacturer"): ?>property-<?php endif; ?><?php echo $filter->getField(); ?>-<?php echo $index; ?>" type="checkbox" name="<?php echo $filter->getField(); ?>" value="<?php echo $option['value']; ?>" />
					<label for="<?php if($filter->getField()!="manufacturer"): ?>property-<?php endif; ?><?php echo $filter->getField(); ?>-<?php echo $index; ?>"><?php echo $option['label']; ?><?php if($filter->hasUnit()): ?><span class="unit">&nbsp;<?php echo $filter->getUnit(); ?></span><?php endif; ?><span class="suffix">(<span class="count"><?php echo $option['count']; ?></span>)</span></label>
				<?php endforeach; ?>
				<div style="clear: both;"></div>
			</div>
		<?php endif; ?>
	</div>
<?php endforeach; ?>
<?php if($this->params->get("filters_collapsed","Y")!="Y"): ?>
<script> Zibbra.Catalog.FILTERS_COLLAPSED = false; </script>
<?php endif; ?>