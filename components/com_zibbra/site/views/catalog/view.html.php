<?php

// No direct access to this file

defined("_JEXEC") or die("Restricted access");
 
// import Joomla view library

jimport("joomla.application.component.view");
 
/**
 * HTML View for Zibbra Catalog
 */
class ZibbraViewCatalog extends JView {
	
	function display($tpl=null) {
		
		// Get the application and document
		
		$app = JFactory::getApplication();
		$doc = JFactory::getDocument();
 
		// Set the document title
		
		$doc->setTitle(JText::_("COM_ZIBBRA_CATALOG"));
		
		// Assign data to the view
			
		$this->categoryid = JRequest::getVar("id");
		$this->category = $this->get("Category");
		$this->items = $this->get("Items");
		$this->pagination = $this->get("Pagination");
		$this->filters = $this->get("Filters");
		$this->state = $this->get("State");
		$this->manufacturers = $this->get("Manufacturers");
		$this->params = JComponentHelper::getParams("com_zibbra");
		$this->image = JRequest::getVar("image","0");
		
		// Sorting
		
		$cat_sort_type = JRequest::getVar("sort_type","global");
		$cat_sort_dir = JRequest::getVar("sort_dir","global");
		
		$this->sort = new StdClass();
		$this->sort->type = isset($_GET['st']) ? $_GET['st'] : ($cat_sort_type!=="global" ? $cat_sort_type : $this->params->get("default_sort_type","times_sold"));
		$this->sort->dir = isset($_GET['sd']) ? $_GET['sd'] : ($cat_sort_dir!=="global" ? $cat_sort_dir : $this->params->get("default_sort_dir","desc"));
 
		// Check for errors
		
		if(count($errors = $this->get("Errors"))) {
			
			JError::raiseError(500,implode("<br />",$errors));
			return false;
			
		} // end if
		
		// Set the breadcrumb
		
		$this->setBreadcrumbs($app);
		
		// Display the view
		
		parent::display($tpl);
		
		// Check of this is an ajax request
		
		$ajax = JRequest::getVar("ajax");
		if(!empty($ajax)) $app->close();
		
		// Assign stylesheet and javascript
		
		$doc->addStyleSheet(JURI::root(true).DS."media/com_zibbra/css/zibbra.catalog.css");
		$doc->addStyleSheet(JURI::root(true).DS."media/com_zibbra/css/zibbra.slider.css");
		$doc->addScript(JURI::root(true).DS."media/com_zibbra/jscripts/zibbra.js");
		$doc->addScript(JURI::root(true).DS."media/com_zibbra/jscripts/zibbra.slider.js");
		$doc->addScript(JURI::root(true).DS."media/com_zibbra/jscripts/zibbra.catalog.js");
	
	} // end function
	
	private function setBreadcrumbs(&$app) {
		
		// Check if we are routed from the Zibbra Menu or the original Joomla menu
		
		$menu = $app->getMenu();
		$menuItem =	$menu->getActive();
		
		// If from the Zibbra Menu, add the breadcrumb trail
		
		if($menuItem===null) {
			
			$pathway = &$app->getPathway();
		
			if(!is_null($this->category)) {
					
				$this->setBreadcrumbsRecursive($pathway,$this->category);
			
			} // end if
			
			if(is_array($this->manufacturers) && count($this->manufacturers)==1) {

				$pathway->addItem(JText::_("COM_ZIBBRA_MANUFACTURERS"),JRoute::_("index.php?option=com_zibbra&view=manufacturers"));
				$pathway->addItem($this->manufacturers[0]->getName());
			
			} // end if
			
		} // end if
		
		// If a Zibbra Category menu item, add the breadcrumb trail
		
		if($menuItem!==null && $menuItem->menutype==="products") {
						
			$only_categories = isset($menuItem->query['only_categories']) && $menuItem->query['only_categories']=="Y";
			
			$pathway = &$app->getPathway();
			$pathway->setPathway(null);
		
			if(!is_null($this->category)) {
					
				$this->setBreadcrumbsRecursive($pathway,$this->category,$only_categories);
			
			} // end if
			
		} // end if
	
	} // end function
	
	private function setBreadcrumbsRecursive(&$pathway,$category,$only_categories=false) {
		
		$append = "";
			
		if($category->hasParent()) {
			
			$parent = $category->getParent();
			$this->setBreadcrumbsRecursive($pathway,$parent,$only_categories);
			
		}elseif($only_categories) {
			
			$append = "&only_categories=Y";
			
		} // end if
		
		$pathway->addItem($category->getName(),JRoute::_("index.php?option=com_zibbra&view=catalog&id=".$category->getIdString().$append));
		
	} // end function
        
} // end class