<?php

// No direct access to this file

defined("_JEXEC") or die("Restricted access");
 
// import Joomla view library

jimport("joomla.application.component.view");
 
/**
 * HTML View for Zibbra Confirm Module
 */
class ZibbraViewConfirm extends JView {
	
	function display($tpl=null) {
		
		// Get the application and document
		
		$app = JFactory::getApplication();
		$doc = JFactory::getDocument();
		
		// Assign data to the view
		
		$this->user = JFactory::getUser();
		$this->session = JFactory::getSession();
		$this->customer = $this->get("Customer");
		$this->order = $this->get("Order");
		
		// Check for errors
		
		if(count($errors = $this->get("Errors"))) {
			
			JError::raiseError(500,implode("<br />",$errors));
			return false;
			
		} // end if
		
		// Display the view
		
		parent::display($tpl);
		
		// Set breadcrumb
		
		$pathway =& $app->getPathway();
		$pathway->addItem(JText::_("COM_ZIBBRA_CHECKOUT"),"");
		
		// Assign stylesheet
		
		$doc->addStyleSheet(JURI::root(true).DS."media/com_zibbra/css/zibbra.confirm.css");
		$doc->addScript(JURI::root(true).DS."media/com_zibbra/jscripts/zibbra.js");
		$doc->addScript(JURI::root(true).DS."media/com_zibbra/jscripts/zibbra.confirm.js");
	
	} // end function
        
} // end class