<?php

// No direct access to this file

defined("_JEXEC") or die("Restricted access");

// Load info

$company = $this->customer->getCompany();
$contact = $this->customer->getPrimaryContact();
$billing_address = $this->customer->getBillingAddress();
$shipping_address = $this->customer->getShippingAddress();

?>
<h3><span><?php echo JText::_("COM_ZIBBRA_YOUR_ACCOUNT"); ?></span></h3>
<dl class="zibbra-account">
	<dt><?php echo JText::_("COM_ZIBBRA_CONTACT"); ?>:</dt>
	<dd><?php echo $contact->getFirstname(); ?>&nbsp;<?php echo $contact->getLastname(); ?></dd>
	<?php if($company->getName()!==null): ?>
		<dt><?php echo JText::_("COM_ZIBBRA_COMPANY"); ?>:</dt>
		<dd><?php echo $company->getName(); ?></dd>
	<?php endif; ?>
	<dt><?php echo JText::_("COM_ZIBBRA_EMAIL"); ?>:</dt>
	<dd><a href="mailto:<?php echo $contact->getEmail(); ?>" target="_blank"><?php echo $contact->getEmail(); ?></a></dd>
	<?php if($contact->getPhone()!==null): ?>
		<dt><?php echo JText::_("COM_ZIBBRA_PHONE"); ?>:</dt>
		<dd><?php echo $contact->getPhone(); ?></dd>
	<?php endif; ?>
</dl>
<div class="clear"></div>

<h3><span><?php echo JText::_("COM_ZIBBRA_BILLING_ADDRESS"); ?></span></h3>
<p><?php echo $billing_address->getStreet(); ?>&nbsp;<?php echo $billing_address->getStreetnr(); ?>&nbsp;<?php echo $billing_address->getBox(); ?><br /><?php echo $billing_address->getCountrycode(); ?>-<?php echo $billing_address->getZipcode(); ?>&nbsp;<?php echo $billing_address->getCity(); ?></p>

<h3><span><?php echo JText::_("COM_ZIBBRA_SHIPPING_ADDRESS"); ?></span></h3>
<p><?php echo $shipping_address->getStreet(); ?>&nbsp;<?php echo $shipping_address->getStreetnr(); ?>&nbsp;<?php echo $shipping_address->getBox(); ?><br /><?php echo $shipping_address->getCountrycode(); ?>-<?php echo $shipping_address->getZipcode(); ?>&nbsp;<?php echo $shipping_address->getCity(); ?></p>