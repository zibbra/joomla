<?php

// No direct access to this file

defined("_JEXEC") or die("Restricted access");

?>
<h2><?php echo JText::_("COM_ZIBBRA_CHECKOUT"); ?></h2>

<div id="zibbra-checkout">
	
	<form id="zibbra-confirm-form" action="<?php echo JRoute::_("index.php?option=com_zibbra"); ?>" method="post">
		<input type="hidden" name="task" value="<?php echo $this->session->get("payment","paypal","zibbra"); ?>.confirm" />
		<?php echo JHtml::_("form.token"); ?>
	
		<table cellpadding="0" cellspacing="0" border="0" width="100%">
			<tbody>
				<tr>
					<td valign="top">
					
						<div class="header"><?php echo JText::_("COM_ZIBBRA_ACCOUNT_AND_ADDRESS"); ?></div>
						
						<div id="zibbra-checkout-customer">
							<?php echo $this->loadTemplate("customer"); ?>
						</div>
							
					</td>
					<td valign="top" width="20"><img src="/media/com_zibbra/images/spacer.gif" width="20" border="0" /></td>
					<td valign="top">
		
						<div class="header"><?php echo JText::_("COM_ZIBBRA_ORDER_REVIEW"); ?></div>
						
						<div id="zibbra-checkout-order">
							<?php echo $this->loadTemplate("order"); ?>
						</div>
						<br />
						<div id="zibbra-checkout-confirm">
							<div class="header"><?php echo JText::_("COM_ZIBBRA_CONFIRM_YOUR_PAYMENT"); ?></div>
							<p><?php echo JText::_("COM_ZIBBRA_FORGOT_AN_ITEM"); ?>&nbsp;<a href="<?php echo JRoute::_("index.php?option=com_zibbra&view=cart"); ?>"><?php echo JText::_("COM_ZIBBRA_EDIT_YOUR_CART"); ?></a></p>
							<input type="submit" name="submit" value="<?php echo JText::_("COM_ZIBBRA_PAY_NOW"); ?>" />
						</div>
						
					</td>
				</tr>
			</tbody>
		</table>
	</form>

</div>
<script>
zibbra.__("loading","<?php echo JText::_("COM_ZIBBRA_LOADING_PLEASE_WAIT");?>");
</script>