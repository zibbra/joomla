<?php

// No direct access to this file

defined("_JEXEC") or die("Restricted access");

?>
<table cellpadding="0" cellspacing="0" border="0" width="100%" class="zibbra-order">
	<thead>
		<tr>
			<th><?php echo JText::_("COM_ZIBBRA_PRODUCT"); ?></th>
			<th align="right"><?php echo JText::_("COM_ZIBBRA_TOTAL"); ?></th>
			<th align="right"><?php echo JText::_("COM_ZIBBRA_VAT"); ?></th>
		</tr>
	</thead>
	<tbody>			
		<?php foreach($this->order->getItems() as $item): ?>		
			<tr>				
				<td><?php echo $item->getQuantity(); ?>&nbsp;x&nbsp;<?php echo $item->getDescription(); ?></td>
				<td align="right"><?php echo $this->order->getValutaSymbol(); ?>&nbsp;<?php echo number_format($item->getTotal(),2,",",""); ?></td>
				<td align="right"><?php echo $item->getVat()*100; ?>%</td>
			</tr>
		<?php endforeach; ?>
	</tbody>
</table>

<table cellpadding="0" cellspacing="4" border="0" align="right">
	<tbody>	
		<tr>						
			<td><?php echo JText::_("COM_ZIBBRA_CART_TOTAL_VAT"); ?>:</td>
			<td align="right" class="amount_vat"><?php echo $this->order->getValutaSymbol(); ?>&nbsp;<?php echo number_format($this->order->getVat(),2,",",""); ?></td>
		</tr>
		<tr>						
			<td><?php echo JText::_("COM_ZIBBRA_CART_TOTAL_INCL"); ?>:</td>
			<td align="right" class="amount_incl"><?php echo $this->order->getValutaSymbol(); ?>&nbsp;<?php echo number_format($this->order->getAmountIncl(),2,",",""); ?></td>
		</tr>				
	</tbody>				
</table>

<div class="clear"></div>