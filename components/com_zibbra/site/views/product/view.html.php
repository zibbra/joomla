<?php

// No direct access to this file

defined("_JEXEC") or die("Restricted access");
 
// import Joomla view library

jimport("joomla.application.component.view");
 
/**
 * HTML View for Zibbra Product Pages
 */
class ZibbraViewProduct extends JView {
	
	function display($tpl=null) {
		
		// Get the application and document
		
		$app = JFactory::getApplication();
		$doc = JFactory::getDocument();
		
		// Get the product & component params
		
		$this->product = $this->get("Product");
		$this->params = JComponentHelper::getParams("com_zibbra");
		
		// Check for errors
		
		if(count($errors = $this->get("Errors"))) {
			
			JError::raiseError(500,implode("<br />",$errors));
			return false;
			
		} // end if
		
		// Calculate property columns
		
		$this->calcPropertyColumns();
 
		// Set the document title
		
		$doc->setTitle($this->product->getName());
		
		// Set the breadcrumb
		
		$this->setBreadcrumbs($app);
		
		// Display the view
		
		parent::display($tpl);

		// Activate mootools
		
		JHTML::_("behavior.mootools");
		
		// Assign stylesheet and javascript
		
		$doc->addStyleSheet(JURI::root(true).DS."media/com_zibbra/css/zibbra.product.css");
		$doc->addScript(JURI::root(true).DS."media/com_zibbra/jscripts/zibbra.js");
		$doc->addScript(JURI::root(true).DS."media/com_zibbra/jscripts/zibbra.product.js");
	
	} // end function
	
	private function setBreadcrumbs($app) {
		
		$pathway = $app->getPathway();
		
		// Check if we are routed from the Zibbra Menu or the original Joomla menu
		
		$menu = $app->getMenu();
		$menuItem =	$menu->getActive();
		
		// If from the Zibbra Menu, add the categories to the breadcrumb trail
		
		if($menuItem===null) {
			
			if($this->product->hasCategories()) {
				
				foreach($this->product->getCategories() as $category) {
			
					$pathway->addItem($category->getName(),JRoute::_("index.php?option=com_zibbra&view=catalog&id=".$category->getIdString()));
					
				} // end foreach
				
			} // end if
		
		} // end if
		
		// Add the product to the breadcrumb trail
			
		$pathway->addItem($this->product->getName(),JRoute::_("index.php?option=com_zibbra&view=product&id=".$this->product->getSlug()));
		
	} // end function
	
	private function calcPropertyColumns() {
		
		if($this->product->hasProperties()) {
			
			$properties = $this->product->getProperties();
			$middle = floor(count($properties) / 2);
			$groups = array();
			$middlegroup = null;
			$lastgroup = null;
			$groupindex = 0;
			
			foreach($properties as $index=>$property) {
				
				if($lastgroup!=null && $lastgroup!=$property->getGroup()) $groupindex++;
				
				if(!isset($groups[$groupindex])) {
					
					$groups[$groupindex] = 1;
					
				}else{
					
					$groups[$groupindex]++;
					
				} // end if
				
				$lastgroup = $property->getGroup();
				
				if($index==$middle) $middlegroup = $groupindex;
				
			} // end foreach
			
			$right = $groups;
			$left = array_splice($right,0,$middlegroup);
			
			$left_total = array_sum($left);
			$right_total = array_sum($right);
			
			$left = $middle - $left_total;
			$right = $right_total - $middle;
			
			$this->split_properties = $left_total + $groups[$middlegroup];
			
			if($left>$right) $this->split_properties = $left_total + $groups[$middlegroup+1];
			
			/*
			echo "<pre>";
			var_dump(array(
				"middleindex"=>$middle,
				"middlegroupindex"=>$middlegroup,
				"left"=>$left,
				"right"=>$right,
				"left_total"=>$left_total,
				"right_total"=>$right_total,
				"split"=>$split
			));
			var_dump($groups);
			exit;
			*/
			
		} // end if
		
	} // end function
        
} // end class