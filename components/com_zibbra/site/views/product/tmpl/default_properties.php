<?php defined("_JEXEC") or die("Restricted access"); ?>
<?php if($this->product->hasProperties()): ?>
	<div class="zibbra-product-properties">
		<h3><?php echo JText::_("COM_ZIBBRA_PRODUCT_SPECIFICATIONS"); ?></h3>
		<table cellspacing="0" cellspacing="0" border="0" width="100%">
			<tr>
				<td valign="top">
					<?php $last_group = null; ?>
					<table cellpadding="0" cellspacing="0" border="0" width="100%">
						<?php foreach($this->product->getProperties() as $index=>$property): ?>
							<?php $group = $property->getGroup(); ?>
							<?php if(isset($this->split_properties) && $index==$this->split_properties): ?>
									</table>
								</td>
								<td width="20"><img src="/media/com_zibbra/images/spacer.gif" width="20" /></td>
								<td valign="top">
									<table cellpadding="0" cellspacing="0" border="0" width="100%">
							<?php endif; ?>
							<?php if(($last_group==null || ($last_group!=null && $last_group!=$group)) && !empty($group)): ?>
								<tr>
									<td colspan="2"><h4><?php echo $group; ?></h4></td>
								</tr>
							<?php endif;?>
								<tr>
									<td valign="top" class="zibbra-product-property-name"><?php echo $property->getName(); ?>:</td>
									<td valign="top" class="zibbra-product-property-value"><?php echo $property->getValue(); ?><?php if($property->hasUnit()): ?>&nbsp;<?php echo $property->getUnit(); ?><?php endif; ?></td>
								</tr>
							<?php $last_group = $group; ?>
						<?php endforeach; ?>
					</table>
				</td>
			</tr>
		</table>
	</div>
<?php endif; ?>