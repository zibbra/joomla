<?php defined("_JEXEC") or die("Restricted access"); ?>
<div class="zibbra-product-album">
	<?php if($this->product->hasImages()): ?>
		<div class="zibbra-product-album-preview">
			<script> Zibbra.Product.URI="<?php echo preg_replace("/\?.*/","",$this->product->getFirstImage()->getPath()); ?>"; </script>
			<img id="zibbra-product-image-preview" src="<?php echo $this->product->getFirstImage()->getPath(); ?>" class="zibbra-product-album-preview selected" border="0" />
		</div>
		<div class="zibbra-product-album-thumbnails">
			<?php foreach($this->product->getImages() as $index=>$image): ?>
				<div id="zibbra-product-image-thumbnail-<?php echo $image->getID(); ?>" class="zibbra-product-album-thumbnail<?php if($index==0): ?> selected<?php endif; ?>">
					<div class="zibbra-product-album-thumbnail-wrapper">
						<img src="<?php echo $image->getPath(42); ?>" border="0" />
					</div>
				</div>
			<?php endforeach; ?>
		</div>
	<?php else: ?>
		<p>NO IMAGE</p>
	<?php endif; ?>
</div>