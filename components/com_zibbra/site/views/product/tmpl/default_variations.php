<?php defined("_JEXEC") or die("Restricted access"); ?>
<?php if($this->product->hasVariations()): ?>
	<?php	
		$arrVariations = $this->product->getVariations();	
	?>
	<div id="zibbra-product-variations">
		<?php foreach($arrVariations as $oVariation): ?>
			<div class="zibbra-product-variation">
				<label for="variations_<?php echo $oVariation->getID(); ?>"><?php echo $oVariation->getName(); ?>:</label>
				<select id="variations_<?php echo $oVariation->getID(); ?>" name="variations[<?php echo $oVariation->getID(); ?>]">
					<option value=""><?php echo JText::_("COM_ZIBBRA_SELECT_OPTION"); ?></option>
					<?php foreach($oVariation->getOptions() as $option): ?>
						<option value="<?php echo $option->id; ?>"<?php if(/*count($arrVariations)==1 && */!$option->in_stock): ?> disabled="disabled"<?php endif; ?>><?php echo $option->name; ?></option>
					<?php endforeach; ?>
				</select>
			</div>
		<?php endforeach; ?>
	</div>
<?php endif; ?>