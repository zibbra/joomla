<?php

// No direct access to this file

defined("_JEXEC") or die("Restricted access");

?>
<?php if($this->product->hasSuggestions()): ?>
<div class="zibbra-product-suggestions">
	<h2>
		<span>Suggested products</span>
		<div class="icon"></div>
	</h2>
	<ul>
		<?php foreach($this->product->getSuggestions() as $product): ?>
			<li>
				<?php if($product->hasImages()): ?>
					<div class="image">
						<a href="<?php echo $product->getURL(); ?>"><img src="<?php echo $product->getFirstImage()->getPath(); ?>" border="0" /></a>
					</div>
				<?php endif; ?>
				<div class="info">
					<h3><?php echo $product->getName(); ?></h3>
					<p class="description"><?php echo substr($product->getShortDescription(),0,50); ?></p>
					<p class="price"><?php echo $product->getValutaSymbol(); ?>&nbsp;<?php echo number_format($product->getPrice(),2,",",""); ?></p>
					<a href="<?php echo $product->getURL(); ?>"><div class="icon"></div></a>
				</div>
				<div class="clear"></div>
			</li>
		<?php endforeach; ?>
	</ul>
	<div class="clear"></div>
</div>
<?php endif; ?>