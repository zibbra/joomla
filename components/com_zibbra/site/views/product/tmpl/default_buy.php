<?php

defined("_JEXEC") or die("Restricted access");

$allow_order = $this->product->allowBackorder() || (!$this->product->allowBackorder() && $this->product->isInStock());

?>
<div class="zibbra-product-buy">			
	<div class="zibbra-product-price">
		<?php if($this->product->hasDiscount()): ?>
			<small><?php echo JText::_("COM_ZIBBRA_FROM"); ?>&nbsp;<s><?php echo $this->product->getValutaSymbol(); ?>&nbsp;<?php echo number_format($this->product->getBasePrice(),2,",",""); ?></s>&nbsp;<?php echo JText::_("COM_ZIBBRA_FOR"); ?>&nbsp;</small><span><?php echo $this->product->getValutaSymbol(); ?>&nbsp;<?php echo number_format($this->product->getPrice(),2,",",""); ?></span>
		<?php else: ?>
			<?php echo $this->product->getValutaSymbol(); ?>&nbsp;<span><?php echo number_format($this->product->getPrice(),2,",",""); ?></span>
		<?php endif; ?>
	</div>
	<input type="submit" class="button zibbra-product-add-to-cart<?php if(!$allow_order): ?> disabled<?php endif; ?>" value="<?php echo JText::_("COM_ZIBBRA_ADD_TO_CART"); ?>"<?php if(!$allow_order): ?> disabled="disabled"<?php endif; ?> />
</div>