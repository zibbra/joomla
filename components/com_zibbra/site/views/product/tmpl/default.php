<?php

// No direct access to this file

defined("_JEXEC") or die("Restricted access");
?>					
<form action="/index.php?option=com_zibbra" method="post" name="zibbra-product-form" id="zibbra-product-form">

	<div id="zibbra-product">
		<div class="zibbra-product-info">
			<?php echo $this->loadTemplate("album"); ?>
			<div class="zibbra-product-description">
				<h1><?php echo $this->product->getName(); ?></h1>
				<p class="zibbra-product-stock">
					<?php if($this->params->get("show_stock","N")==="Y"): ?>
						<?php echo JText::_("COM_ZIBBRA_STOCK"); ?>: <span class="<?php echo $this->product->isInStock() ? "in_stock" : ""; ?>"><?php echo $this->product->getStock(); ?></span>
					<?php else: ?>
						<?php echo JText::_("COM_ZIBBRA_AVAILABILITY"); ?>: <span class="<?php echo $this->product->isInStock() ? "in_stock" : ""; ?>"><?php echo $this->product->isInStock() ? JText::_("COM_ZIBBRA_IN_STOCK") : JText::_("COM_ZIBBRA_NOT_IN_STOCK"); ?></span>
					<?php endif; ?>
				</p>
				<p><?php echo $this->product->getLongDescription(); ?></p>
				<?php echo $this->loadTemplate("addons"); ?>
				<?php echo $this->loadTemplate("variations"); ?>
				<div class="zibbra-product-actions">
					<?php echo $this->loadTemplate("buy"); ?>
					<?php echo $this->loadTemplate("social"); ?>
				</div>
			</div>
		</div>
		<div class="clear"></div>
		<?php echo $this->loadTemplate("suggestions"); ?>
		<?php echo $this->loadTemplate("properties"); ?>
	</div>
		
	<div>
		<input type="hidden" name="task" value="cart.addProduct" />
		<input id="productid" type="hidden" name="id" value="<?php echo $this->product->getProductid(); ?>" />
		<?php echo JHtml::_("form.token"); ?>
	</div>

</form>