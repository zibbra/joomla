<?php defined("_JEXEC") or die("Restricted access"); ?>
<?php

$product_url = JRoute::_("index.php?option=com_zibbra&view=product&id=".$this->product->getSlug());

?>
<div class="zibbra-product-social">
	<div class="zibbra-product-social-icon zibbra-product-social-facebook" title="Share on Facebook">
		<iframe src="//www.facebook.com/plugins/like.php?href=<?php echo urlencode($product_url); ?>&amp;send=false&amp;layout=button_count&amp;width=100&amp;show_faces=false&amp;action=like&amp;colorscheme=light&amp;font&amp;height=28" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:100px; height:20px;"allowTransparency="true"></iframe>
	</div>
	<div class="zibbra-product-social-icon zibbra-product-social-twitter" title="Share on Twitter">
		<a href="https://twitter.com/share" class="twitter-share-button">Tweet</a>
		<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
	</div>
	<div class="zibbra-product-social-icon zibbra-product-social-google-plus" title="Share on Google+">
		<div class="g-plus" data-action="share" data-height="20"></div>				
		<script type="text/javascript">
		  (function() {
		    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
		    po.src = 'https://apis.google.com/js/plusone.js';
		    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
		  })();
		</script>
	</div>
</div>