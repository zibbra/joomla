<?php

// No direct access to this file

defined("_JEXEC") or die("Restricted access");

?>
<h2><?php echo JText::_("COM_ZIBBRA_CART"); ?></h2>
			
<?php if(!$this->cart->isEmpty()): ?>
					
	<form action="<?php echo JRoute::_("index.php?option=com_zibbra"); ?>" method="post" name="zibbra-cart-form" id="zibbra-cart-form">
	
		<table cellpadding="0" cellspacing="0" border="0" width="100%" class="zibbra-cart">
			<thead>	
				<tr>		
					<th><?php echo JText::_("COM_ZIBBRA_PRODUCT"); ?></th>
					<th align="center"><?php echo JText::_("COM_ZIBBRA_QUANTITY"); ?></th>
					<th align="right"><?php echo JText::_("COM_ZIBBRA_AMOUNT"); ?></th>
					<th align="right"><?php echo JText::_("COM_ZIBBRA_TOTAL"); ?></th>
					<th align="right"><?php echo JText::_("COM_ZIBBRA_VAT"); ?></th>
					<th width="16">&nbsp;</th>		
				</tr>	
			</thead>
			<tbody>						
				<?php foreach($this->cart->getItems() as $item): ?>		
					<tr>				
						<td><a href="<?php echo JRoute::_("index.php?option=com_zibbra&view=product&id=".$item->getProductid()); ?>"><?php echo $item->getDescription(); ?></a></td>
						<td align="center"><input type="text" name="quantity[<?php echo $item->getCartitemid(); ?>]" size="3" maxlength="3" value="<?php echo $item->getQuantity(); ?>" size="2" /></td>
						<td align="right"><?php echo $this->cart->getValutaSymbol(); ?>&nbsp;<?php echo number_format($item->getAmount(),2,",",""); ?></td>
						<td align="right"><?php echo $this->cart->getValutaSymbol(); ?>&nbsp;<?php echo number_format($item->getTotal(),2,",",""); ?></td>
						<td align="right"><?php echo $item->getVat()*100; ?>%</td>
						<td width="16" class="actions">
							<a class="zibbra-button" href="<?php echo JRoute::_("index.php?option=com_zibbra&task=cart.deleteItem"); ?>&id=<?php echo $item->getCartitemid(); ?>" title="<?php echo JText::_("COM_ZIBBRA_CART_DELETE_ITEM"); ?>" onclick="return confirm('<?php echo JText::_("COM_ZIBBRA_CART_DELETE_ITEM_CONFIRM"); ?>');">
								<div class="zibbra-icon zibbra-icon-delete"></div>
							</a>
						</td>				
					</tr>			
				<?php endforeach; ?>
			</tbody>
		</table>
		
		<div id="zibbra-cart-bottom">
			
			<div class="zibbra-cart-totals">
			
				<table cellpadding="0" cellspacing="4" border="0" align="right">			
					<thead>					
						<tr>						
							<th colspan="2"><?php echo JText::_("COM_ZIBBRA_CART_TOTALS"); ?></th>						
						</tr>					
					</thead>			
					<tbody>					
						<tr>						
							<td><?php echo JText::_("COM_ZIBBRA_CART_TOTAL_EXCL"); ?>:</td>
							<td align="right" class="amount_excl"><?php echo $this->cart->getValutaSymbol(); ?>&nbsp;<?php echo number_format($this->cart->getAmount(),2,",",""); ?></td>
						</tr>
						<tr>						
							<td><?php echo JText::_("COM_ZIBBRA_CART_TOTAL_VAT"); ?>:</td>
							<td align="right" class="amount_vat"><?php echo $this->cart->getValutaSymbol(); ?>&nbsp;<?php echo number_format($this->cart->getVat(),2,",",""); ?></td>
						</tr>
						<?php if($this->shipping && $this->shipping->getPrice()>0): ?>		
							<tr>
								<td><?php echo JText::_("COM_ZIBBRA_SHIPPING_COST"); ?>:</td>
								<td align="right" class="amount_shipping"><?php echo $this->cart->getValutaSymbol(); ?>&nbsp;<?php echo number_format($this->shipping->getPriceVatIncl(),2,",",""); ?></td>
							</tr>
						<?php endif; ?>
						<?php if($this->payment && $this->payment->getPrice()>0): ?>		
							<tr>
								<td><?php echo JText::_("COM_ZIBBRA_PAYMENT_COST"); ?>:</td>
								<td align="right" class="amount_shipping"><?php echo $this->cart->getValutaSymbol(); ?>&nbsp;<?php echo number_format($this->payment->getPrice(),2,",",""); ?></td>
							</tr>
						<?php endif; ?>
						<tr>						
							<td><?php echo JText::_("COM_ZIBBRA_CART_TOTAL_INCL"); ?>:</td>
							<td align="right" class="amount_incl"><?php echo $this->cart->getValutaSymbol(); ?>&nbsp;<?php echo number_format($this->cart->getTotal(),2,",",""); ?></td>
						</tr>				
					</tbody>				
				</table>
						
			</div>
			
			<div class="zibbra-cart-buttons">
						
				<table cellpadding="0" cellspacing="10" width="100%">
					<tr>	
						<td width="50%" align="right">
							<a class="zibbra-cart-button zibbra-cart-update" href="javascript:document.getElementById('zibbra-cart-form').submit();">
								<span><?php echo JText::_("COM_ZIBBRA_CART_UPDATE"); ?></span>
							</a>
						</td>					
						<td width="50%" align="left">
							<a class="zibbra-cart-button zibbra-cart-empty" href="<?php echo JRoute::_("index.php?option=com_zibbra&task=cart.clear"); ?>" onclick="return confirm('<?php echo JText::_("COM_ZIBBRA_CART_EMPTY_CONFIRM"); ?>');">
								<span><?php echo JText::_("COM_ZIBBRA_CART_EMPTY"); ?></span>
							</a>
						</td>		
					</tr>		
					<tr>
						<td width="50%" align="right">
							<a class="zibbra-cart-link zibbra-cart-cancel" href="/">
								<span>&laquo;&nbsp;<?php echo JText::_("COM_ZIBBRA_CART_CANCEL"); ?></span>
							</a>
						</td>
						<td width="50%" align="left">
							<a class="zibbra-cart-link zibbra-cart-checkout" href="<?php echo JRoute::_("index.php?option=com_zibbra&view=checkout"); ?>">
								<span><?php echo JText::_("COM_ZIBBRA_CART_CHECKOUT"); ?>&nbsp;&raquo;</span>
							</a>
						</td>			
					</tr>
				</table>
						
			</div>
			
		</div>
			
		<div>
			<input type="hidden" name="task" value="cart.update" />
			<?php echo JHtml::_("form.token"); ?>
		</div>
		
		<div class="clear"></div>
	
	</form>

<?php else: ?>

	<p><?php echo JText::_("COM_ZIBBRA_CART_IS_EMPTY"); ?></p>
	
<?php endif; ?>