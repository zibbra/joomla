<?php

// No direct access to this file

defined("_JEXEC") or die("Restricted access");
 
// import Joomla view library

jimport("joomla.application.component.view");
 
/**
 * HTML View for Zibbra Shopping Cart
 */
class ZibbraViewCart extends JView {
	
	function display($tpl=null) {
		
		// Get the application and document
		
		$app = JFactory::getApplication();
		$doc = JFactory::getDocument();
		
		// Assign data to the view
		
		$this->cart = $this->get("Cart");
		$this->shipping = $this->get("Shipping");
		$this->payment = $this->get("Payment");
		$this->form = $this->get("Form");
		
		// Check for errors
		
		if(count($errors = $this->get("Errors"))) {
			
			JError::raiseError(500,implode("<br />",$errors));
			return false;
			
		} // end if
		
		// Display the view
		
		parent::display($tpl);
		
		// Set breadcrumb
		
		$pathway =& $app->getPathway();
		$pathway->addItem(JText::_("COM_ZIBBRA_CART"),"");
		
		// Assign stylesheet and javascript
		
		$doc->addStyleSheet(JURI::root(true).DS."media/com_zibbra/css/zibbra.cart.css");
		$doc->addScript(JURI::root(true).DS."media/com_zibbra/jscripts/zibbra.js");
		$doc->addScript(JURI::root(true).DS."media/com_zibbra/jscripts/zibbra.cart.js");
		
	} // end function
        
} // end class