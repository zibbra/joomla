<?php

// No direct access to this file

defined("_JEXEC") or die("Restricted access");
 
// import Joomla view library

jimport("joomla.application.component.view");
 
/**
 * HTML View for Zibbra Manufacturers
 */
class ZibbraViewManufacturers extends JView {
	
	function display($tpl=null) {
		
		// Get the application and document
		
		$app = JFactory::getApplication();
		$doc = JFactory::getDocument();
 
		// Set the document title
		
		$doc->setTitle(JText::_("COM_ZIBBRA_MANUFACTURERS"));
		
		// Assign data to the view
			
		$this->items = $this->get("Items");
		$this->params = JComponentHelper::getParams("com_zibbra");
 
		// Check for errors
		
		if(count($errors = $this->get("Errors"))) {
			
			JError::raiseError(500,implode("<br />",$errors));
			return false;
			
		} // end if
		
		// Set the breadcrumb
		
		$this->setBreadcrumbs($app);
		
		// Display the view
		
		parent::display($tpl);
		
		// Assign stylesheet and javascript
		
		$doc->addStyleSheet(JURI::root(true).DS."media/com_zibbra/css/zibbra.manufacturers.css");
		$doc->addScript(JURI::root(true).DS."media/com_zibbra/jscripts/zibbra.js");
		$doc->addScript(JURI::root(true).DS."media/com_zibbra/jscripts/zibbra.manufacturers.js");
	
	} // end function
	
	private function setBreadcrumbs($app) {
		
		// Check if we are routed from the Zibbra Menu or the original Joomla menu
		
		$menu = $app->getMenu();
		$menuItem =	$menu->getActive();
		
		// If from the Zibbra Menu, add the breadcrumb trail
		
		if($menuItem===null) {
		
			$pathway = $app->getPathway();			
			$pathway->addItem(JText::_("COM_ZIBBRA_MANUFACTURERS"));
		
		} // end if	
	
	} // end function
        
} // end class