<?php

// No direct access to this file

defined("_JEXEC") or die("Restricted access");

?>
<h2><?php echo JText::_("COM_ZIBBRA_MANUFACTURERS"); ?></h2>
<div id="zibbra-manufacturers">
	<?php if($this->params->get("manufacturers_tabs","Y")=="Y"): ?>
		<ul class="tabs">
			<?php $count = 0; ?>
			<?php foreach($this->items as $key=>$manufacturers): ?>
				<li<?php if($count==0): ?> class="selected"<?php endif; ?>><?php echo $key; ?></li>
				<?php $count++; ?>
			<?php endforeach; ?>
		</ul>
		<div style="clear:both;"></div>
	<?php endif; ?>
	<?php $count = 0; ?>
	<?php foreach($this->items as $key=>$manufacturers): ?>
		<div id="zibbra-manufacturers-<?php echo $key; ?>" class="list"<?php if($this->params->get("manufacturers_tabs","Y")=="Y" && $count>0): ?> style="display:none;"<?php endif; ?>>
			<ul class="zibbra-manufacturers">
				<?php foreach($manufacturers as $manufacturer): ?>
					<li><a href="<?php echo JRoute::_("index.php?option=com_zibbra&view=catalog")."?manufacturers[]=".$manufacturer->getManufacturerid(); ?>"><?php echo $manufacturer->getName(); ?></a></li>
				<?php endforeach; ?>
			</ul>
		</div>
		<?php $count++; ?>
	<?php endforeach; ?>
</div>
<?php if($this->params->get("manufacturers_tabs","Y")!="Y"): ?>
<style>
#zibbra-manufacturers {
	-moz-column-count: 4;
	-webkit-column-count: 4;
	column-count: 4;
}

#zibbra-manufacturers > div.list {
	margin-top: 0px;
	margin-bottom: 10px;
}
</style>
<?php endif; ?>