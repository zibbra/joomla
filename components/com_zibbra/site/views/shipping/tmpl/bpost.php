<?php

// No direct access to this file

defined("_JEXEC") or die("Restricted access");

?>

<iframe id="bpost_frontend_frame" name="bpost_frontend_frame" style="float:left; width:800px; height:520px; margin-right:20px;"></iframe>

<script type="text/javascript">

var bpost = null;

window.addEvent('domready', function() {

	bpost = new Bpost();
	bpost.uri("<?php echo $this->settings['frontend_uri']; ?>");

	<?php foreach($this->config as $key=>$value): ?>

		<?php if (is_array($value)): ?>
		<?php foreach($value as $subkey=>$subvalue): ?>

	bpost.set("<?php echo $key; ?>","<?php echo $subvalue; ?>");
			
		<?php endforeach; ?>
		<?php else: ?>

	bpost.set("<?php echo $key; ?>","<?php echo $value; ?>");
		
		<?php endif; ?>
		
	<?php endforeach; ?>

	bpost.submit();	
	
}); // dom ready

</script>