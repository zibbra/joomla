<?php

// No direct access to this file

defined("_JEXEC") or die("Restricted access");
 
// import Joomla view library

jimport("joomla.application.component.view");
 
/**
 * HTML View for Zibbra Shipping
 */
class ZibbraViewShipping extends JView {
	
	function display($tpl=null) {
		
		// Get the application and document
		
		$app = JFactory::getApplication();
		$doc = JFactory::getDocument();

		// Assign data to the view
		
		$this->config = $this->get("Config");
		$this->settings = $this->get("Settings");
		
		// Check for errors
		
		if(count($errors = $this->get("Errors"))) {
			
			JError::raiseError(500,implode("<br />",$errors));
			return false;
			
		} // end if
		
		// Display the view
		
		parent::display($tpl);
		
		// Assign javascript
		
		$doc->addStyleSheet(JURI::root(true).DS."media/com_zibbra/css/zibbra.shipping.css");
		$doc->addScript(JURI::root(true).DS."media/com_zibbra/jscripts/zibbra.js");
		$doc->addScript(JURI::root(true).DS."media/com_zibbra/jscripts/zibbra.shipping.js");
		
	} // end function
        
} // end class