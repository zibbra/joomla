<?php

// No direct access to this file

defined("_JEXEC") or die("Restricted access");
 
// import Joomla view library

jimport("joomla.application.component.view");
 
/**
 * HTML View for Zibbra Customer Registration
 */
class ZibbraViewRegister extends JView {
	
	function display($tpl=null) {
		
		// Get the application and document
		
		$app = JFactory::getApplication();
		$doc = JFactory::getDocument();
		
		// Assign data to the view
		
		$this->form = $this->get("Form");
		$this->return = base64_encode(JRoute::_(JRequest::getVar("return",false) ? JRequest::getVar("return") : "index.php?option=com_zibbra&view=account"));
		
		// Check for errors
		
		if(count($errors = $this->get("Errors"))) {
			
			JError::raiseError(500,implode("<br />",$errors));
			return false;
			
		} // end if
		
		// Display the view
		
		parent::display($tpl);
		
		// Assign javascript
		
		JText::script("COM_ZIBBRA_JS_ERROR_FORM_INVALID");
		$doc->addStyleSheet(JURI::root(true).DS."media/com_zibbra/css/zibbra.register.css");
		$doc->addScript(JURI::root(true).DS."media/com_zibbra/jscripts/zibbra.js");
		$doc->addScript(JURI::root(true).DS."media/com_zibbra/jscripts/zibbra.register.js");
		
	} // end function
        
} // end class