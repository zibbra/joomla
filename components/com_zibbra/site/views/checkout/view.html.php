<?php

// No direct access to this file

defined("_JEXEC") or die("Restricted access");
 
// import Joomla view library

jimport("joomla.application.component.view");
 
/**
 * HTML View for Zibbra Checkout Module
 */
class ZibbraViewCheckout extends JView {
	
	function display($tpl=null) {
		
		// Get the application and document
		
		$app = JFactory::getApplication();
		$doc = JFactory::getDocument();
		
		// Get the library adapter
		
		$adapter = Zibbra::getInstance()->getLibrary()->getAdapter();
		
		// Assign data to the view
		
		$this->user = JFactory::getUser();
		$this->cart = $this->get("Cart");
		
		if($this->cart->isEmpty()) {
			
			$app->redirect(JRoute::_("index.php?option=com_zibbra&view=cart",false));
		
		} // end if
		
		$this->customer = $this->get("Customer");
		$this->shipping = $this->get("Shippingmethod");
		$this->payment = $this->get("Payment");
		$this->form = $this->get("Form");
		$this->params = $app->getParams();
		$this->allow_comments = JComponentHelper::getParams("com_zibbra")->get("allow_comments","N")=="Y";
		$this->comments = $adapter->getSessionValue("order.comments",null);
		
		// Check for errors
		
		if(count($errors = $this->get("Errors"))) {
			
			JError::raiseError(500,implode("<br />",$errors));
			return false;
			
		} // end if
		
		// Display the view
		
		parent::display($tpl);
		
		// Check of this is an ajax request
		
		$ajax = JRequest::getVar("ajax");
		if(!empty($ajax)) $app->close();
		
		// Set breadcrumb
		
		$pathway =& $app->getPathway();
		$pathway->addItem(JText::_("COM_ZIBBRA_CHECKOUT"),"");
		
		// Assign stylesheet and javascript
		
		$doc->addStyleSheet(JURI::root(true).DS."media/com_zibbra/css/zibbra.checkout.css");
		$doc->addScript(JURI::root(true).DS."media/com_zibbra/jscripts/zibbra.js");
		$doc->addScript(JURI::root(true).DS."media/com_zibbra/jscripts/zibbra.checkout.js");
	
	} // end function
        
} // end class