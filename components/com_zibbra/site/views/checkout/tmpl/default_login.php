<?php

// No direct access to this file

defined("_JEXEC") or die("Restricted access");

?>
<form action="<?php echo JRoute::_("index.php?option=com_users&task=user.login",true,$this->params->get("usesecure")); ?>" method="post" name="com-login" id="redirect" name="redir">
    <input type="hidden" name="remember" value="yes" />
    <input type="hidden" name="option" value="com_users" />
    <input type="hidden" name="task" value="user.login" />
    <input type="hidden" name="return" value="<?php echo base64_encode("index.php?option=com_zibbra&view=checkout"); ?>" />
    <?php echo JHtml::_("form.token"); ?>
	<div class="username-container">
	    <label id="username-label" for="username"><?php echo JText::_("COM_ZIBBRA_USERNAME"); ?></label>
	    <input type="text" id="username" name="username" value="" />
    </div>
    <div class="password-container">
	    <label id="password-label" for="password"><?php echo JText::_("COM_ZIBBRA_PASSWORD"); ?></label>
    	<input type="password" id="password" name="password" value="" />
    </div>
    <input type="submit" name="submit" value="<?php echo JText::_("COM_ZIBBRA_LOGIN"); ?>" />
</form>