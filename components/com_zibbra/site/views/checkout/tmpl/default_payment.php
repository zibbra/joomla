<?php

// No direct access to this file

defined("_JEXEC") or die("Restricted access");

// Get the field

$field = $this->form->getField("payment_method","checkout");

?>
<div id="zibbra-checkout-payment">
	<?php echo $field->input; ?>
</div>