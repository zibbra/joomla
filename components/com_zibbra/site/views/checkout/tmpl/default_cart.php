<?php

// No direct access to this file

defined("_JEXEC") or die("Restricted access");

?>
<table cellpadding="0" cellspacing="0" border="0" width="100%" class="zibbra-cart">
	<thead>
		<tr>
			<th><?php echo JText::_("COM_ZIBBRA_PRODUCT"); ?></th>
			<th align="right"><?php echo JText::_("COM_ZIBBRA_TOTAL"); ?></th>
			<th align="right"><?php echo JText::_("COM_ZIBBRA_VAT"); ?></th>
		</tr>
	</thead>
	<tbody>			
		<?php foreach($this->cart->getItems() as $item): ?>		
			<tr>				
				<td><?php echo $item->getQuantity(); ?>&nbsp;x&nbsp;<a href="<?php echo JRoute::_("index.php?option=com_zibbra&view=product&id=".$item->getProductid()); ?>"><?php echo $item->getDescription(); ?></a></td>
				<td align="right"><?php echo $this->cart->getValutaSymbol(); ?>&nbsp;<?php echo number_format($item->getTotal(),2,",",""); ?></td>
				<td align="right"><?php echo $item->getVat()*100; ?>%</td>
			</tr>
		<?php endforeach; ?>
	</tbody>
</table>

<table cellpadding="0" cellspacing="4" border="0" align="right">
	<tbody>	
		<tr>						
			<td><?php echo JText::_("COM_ZIBBRA_CART_TOTAL_EXCL"); ?>:</td>
			<td align="right" class="amount_excl"><?php echo $this->cart->getValutaSymbol(); ?>&nbsp;<?php echo number_format($this->cart->getAmount(),2,",",""); ?></td>
		</tr>
		<?php if($this->shipping && $this->shipping->getPrice()>0): ?>		
			<tr>
				<td><?php echo JText::_("COM_ZIBBRA_SHIPPING_COST"); ?>:</td>
				<td align="right" class="amount_shipping"><?php echo $this->cart->getValutaSymbol(); ?>&nbsp;<?php echo number_format($this->shipping->getPriceVatIncl(),2,",",""); ?></td>
			</tr>
		<?php endif; ?>
		<tr>						
			<td><?php echo JText::_("COM_ZIBBRA_CART_TOTAL_VAT"); ?>:</td>
			<td align="right" class="amount_vat"><?php echo $this->cart->getValutaSymbol(); ?>&nbsp;<?php echo number_format($this->cart->getVat(),2,",",""); ?></td>
		</tr>
		<?php if($this->payment && $this->payment->getPrice()>0): ?>		
			<tr>
				<td><?php echo JText::_("COM_ZIBBRA_PAYMENT_COST"); ?>:</td>
				<td align="right" class="amount_shipping"><?php echo $this->cart->getValutaSymbol(); ?>&nbsp;<?php echo number_format($this->payment->getPrice(),2,",",""); ?></td>
			</tr>
		<?php endif; ?>
		<tr>						
			<td><?php echo JText::_("COM_ZIBBRA_CART_TOTAL_INCL"); ?>:</td>
			<td align="right" class="amount_incl"><?php echo $this->cart->getValutaSymbol(); ?>&nbsp;<?php echo number_format($this->cart->getTotal(),2,",",""); ?></td>
		</tr>				
	</tbody>				
</table>

<div class="clear"></div>