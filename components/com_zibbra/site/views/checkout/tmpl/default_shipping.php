<?php

// No direct access to this file

defined("_JEXEC") or die("Restricted access");

// Get the field

$field = $this->form->getField("shipping_method","checkout");

?>
<div id="zibbra-checkout-shipping">
	<?php echo $field->input; ?>
</div>