<?php

// No direct access to this file

defined("_JEXEC") or die("Restricted access");

?>

<div id="zibbra-checkout">

	<h2><?php echo JText::_("COM_ZIBBRA_CHECKOUT"); ?></h2>
	
	<table cellpadding="0" cellspacing="0" border="0" width="100%">
		<tbody>
			<tr>
				<td valign="top">
					<div class="header"><span class="step">1</span><?php echo JText::_("COM_ZIBBRA_ACCOUNT_AND_ADDRESS"); ?></div>
					<?php if($this->user->get("guest")): ?>
						<p><?php echo JText::_("COM_ZIBBRA_CUSTOMER_LOGIN"); ?></p>
						<div id="zibbra-checkout-login">
							<?php echo $this->loadTemplate("login"); ?>
						</div>
						<p><?php echo JText::_("COM_ZIBBRA_OR"); ?></p>
						<div id="zibbra-checkout-register">
							<a href="<?php echo JRoute::_("index.php?option=com_zibbra&view=register")."?return=".urlencode("index.php?option=com_zibbra&view=checkout"); ?>"><?php echo JText::_("COM_ZIBBRA_CUSTOMER_REGISTER"); ?></a>
						</div>
					<?php else: ?>
						<div id="zibbra-checkout-account">
							<?php echo $this->loadTemplate("account"); ?>
						</div>
					<?php endif; ?>
				</td>
				<td valign="top" width="20"><img src="/media/com_zibbra/images/spacer.gif" width="20" border="0" /></td>
				<td valign="top">
					<form id="zibbra-checkout-form" action="<?php echo JRoute::_("index.php?option=com_zibbra"); ?>" method="post">
						<input type="hidden" name="task" value="checkout.confirm" />
						<?php echo JHtml::_("form.token"); ?>	
						<table cellpadding="0" cellspacing="0" border="0" width="100%">
							<tbody>
								<tr>
									<td valign="top">
										<div class="header"><span class="step">2</span><?php echo JText::_("COM_ZIBBRA_SHIPPING_METHOD"); ?></div>
										<?php echo $this->loadTemplate("shipping"); ?>
										<br />
										<div class="header"><span class="step">3</span><?php echo JText::_("COM_ZIBBRA_PAYMENT_METHOD"); ?></div>
										<div id="zibbra-checkout-payment">
											<?php echo $this->loadTemplate("payment"); ?>
										</div>
									</td>
									<td valign="top" width="20"><img src="/media/com_zibbra/images/spacer.gif" width="20" border="0" /></td>
									<td valign="top">
										<div class="header"><span class="step">4</span><?php echo JText::_("COM_ZIBBRA_ORDER_REVIEW"); ?></div>
										<div id="zibbra-checkout-cart">
											<?php echo $this->loadTemplate("cart"); ?>
										</div>
										<br />
										<?php if($this->allow_comments): ?>
											<div class="header"><span class="step">5</span><?php echo JText::_("COM_ZIBBRA_ORDER_COMMENTS"); ?></div>
											<div id="zibbra-checkout-comments">
												<textarea name="comments" id="comments"><?php echo $this->comments; ?></textarea>
											</div>
										<?php endif; ?>
										<div class="header"><span class="step"><?php if($this->allow_comments): ?>6<?php else: ?>5<?php endif; ?></span><?php echo JText::_("COM_ZIBBRA_CONFIRM_YOUR_ORDER"); ?></div>
										<div id="zibbra-checkout-confirm">
											<p><?php echo JText::_("COM_ZIBBRA_FORGOT_AN_ITEM"); ?>&nbsp;<a href="<?php echo JRoute::_("index.php?option=com_zibbra&view=cart"); ?>"><?php echo JText::_("COM_ZIBBRA_EDIT_YOUR_CART"); ?></a></p>
											<input type="submit" name="submit" value="<?php echo JText::_("COM_ZIBBRA_PLACE_ORDER"); ?>" disabled="disabled" class="disabled" />
										</div>
									</td>
								</tr>
							</tbody>
						</table>						
					</form>
				</td>
			</tr>
		</tbody>
	</table>

</div>
<script>
zibbra.__("loading","<?php echo JText::_("COM_ZIBBRA_LOADING_PLEASE_WAIT");?>");
</script>