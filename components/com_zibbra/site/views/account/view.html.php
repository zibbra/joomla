<?php

// No direct access to this file

defined("_JEXEC") or die("Restricted access");
 
// import Joomla view library

jimport("joomla.application.component.view");
 
/**
 * HTML View for Zibbra Customer Account
 */
class ZibbraViewAccount extends JView {
	
	function display($tpl=null) {
		
		// Get the application and document
		
		$app = JFactory::getApplication();
		$doc = JFactory::getDocument();
		
		// Assign data to the view
		
		$this->customer = $this->get("Customer");
		$this->contact = $this->get("Contact");
		$this->company = $this->get("Company");
		$this->billing_address = $this->get("BillingAddress");
		$this->shipping_address = $this->get("ShippingAddress");
		
		if($this->getLayout()=="edit") {
			
			$this->form = $this->get("Form");
			
		}elseif($this->getLayout()=="orders") {
		
			$this->orders = $this->get("Orders");
			
		}elseif($this->getLayout()=="invoices") {
		
			$this->invoices = $this->get("Invoices");
			
		}else {

			$this->orders = $this->get("RecentOrders");
			$this->invoices = $this->get("RecentInvoices");	
			
		} // end if
		
		// Check for errors
		
		if(count($errors = $this->get("Errors"))) {
			
			JError::raiseError(500,implode("<br />",$errors));
			return false;
			
		} // end if
		
		// Display the view
		
		parent::display($tpl);
		
		// Set breadcrumb
		
		$pathway = $app->getPathway();
		
		if($this->getLayout()=="orders") {
		
			$pathway->addItem(JText::_("COM_ZIBBRA_YOUR_ACCOUNT"),JRoute::_("index.php?option=com_zibbra&view=account"));
			$pathway->addItem(JText::_("COM_ZIBBRA_YOUR_ORDERS"),"");
		
		}elseif($this->getLayout()=="invoices") {
		
			$pathway->addItem(JText::_("COM_ZIBBRA_YOUR_ACCOUNT"),JRoute::_("index.php?option=com_zibbra&view=account"));
			$pathway->addItem(JText::_("COM_ZIBBRA_YOUR_INVOICES"),"");
			
		}elseif($this->getLayout()=="edit") {
		
			$pathway->addItem(JText::_("COM_ZIBBRA_YOUR_ACCOUNT"),JRoute::_("index.php?option=com_zibbra&view=account"));
			$pathway->addItem(JText::_("COM_ZIBBRA_EDIT"),"");
			
		}else{
		
			$pathway->addItem(JText::_("COM_ZIBBRA_YOUR_ACCOUNT"),"");
			
		} // end if
		
		// Assign stylesheet and javascript
		
		$doc->addStyleSheet(JURI::root(true).DS."media/com_zibbra/css/zibbra.account.css");
		//$doc->addScript(JURI::root(true).DS."media/com_zibbra/jscripts/zibbra.js");
		//$doc->addScript(JURI::root(true).DS."media/com_zibbra/jscripts/zibbra.account.js");
		
	} // end function
        
} // end class