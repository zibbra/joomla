<?php

// No direct access to this file

defined("_JEXEC") or die("Restricted access");
?>
<h3>
	<span><?php echo JText::_("COM_ZIBBRA_RECENT_INVOICES"); ?></span>
	<?php if(count($this->invoices)>0): ?>
		<a href="<?php echo JRoute::_("index.php?option=com_zibbra&view=account&layout=invoices"); ?>"><?php echo JText::_("COM_ZIBBRA_VIEW_ALL"); ?></a>
	<?php endif; ?>
</h3>
<?php if(count($this->invoices)>0): ?>
	<table cellpadding="0" cellspacing="0" border="0" width="100%" class="zibbra-account">
		<thead>
			<tr>
				<th><?php echo JText::_("COM_ZIBBRA_NUMBER"); ?></th>
				<th style="text-align:center;"><?php echo JText::_("COM_ZIBBRA_DATE"); ?></th>
				<th style="text-align:center;"><?php echo JText::_("COM_ZIBBRA_EXPIRY_DATE"); ?></th>
				<th style="text-align:right;"><?php echo JText::_("COM_ZIBBRA_TOTAL"); ?></th>
				<th style="text-align:right;"><?php echo JText::_("COM_ZIBBRA_PAID"); ?></th>
				<th style="text-align:center;"><?php echo JText::_("COM_ZIBBRA_ACTIONS"); ?></th>
			</tr>
		</thead>
		<tbody>
			<?php foreach($this->invoices as $obj): ?>
				<tr>
					<td><?php echo $obj->getNumber(); ?></td>
					<td style="text-align:center;"><?php echo $obj->getInvoiceDate(); ?></td>
					<td style="text-align:center;"><?php echo $obj->getExpiryDate(); ?></td>
					<td style="text-align:right;">&euro;&nbsp;<?php echo number_format($obj->getAmount(),2,",","."); ?></td>
					<td style="text-align:center;"><?php echo $obj->isPaid() ? JText::_("COM_ZIBBRA_YES") : JText::_("COM_ZIBBRA_NO"); ?></td>
					<td style="text-align:center;"><a href="<?php echo JRoute::_("index.php?option=com_zibbra&task=account.downloadPdf&type=invoice&nr=".$obj->getNumber()); ?>" target="_blank" title="<?php echo JText::_("COM_ZIBBRA_DOWNLOAD_PDF"); ?>"><?php echo JText::_("COM_ZIBBRA_VIEW"); ?></a></td>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
<?php else: ?>
	<p><?php echo JText::_("COM_ZIBBRA_NO_INVOICES"); ?></p>
<?php endif; ?>