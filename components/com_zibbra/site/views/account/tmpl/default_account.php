<?php

// No direct access to this file

defined("_JEXEC") or die("Restricted access");
?>
<h3>
	<span><?php echo JText::_("COM_ZIBBRA_ACCOUNT_INFORMATION"); ?></span>
	<a href="<?php echo JRoute::_("index.php?option=com_zibbra&view=account&layout=edit&section=account"); ?>"><?php echo JText::_("COM_ZIBBRA_EDIT"); ?></a>
</h3>
<dl class="zibbra-account">
	<dt><?php echo JText::_("COM_ZIBBRA_CONTACT"); ?>:</dt>
	<dd><?php echo $this->contact->getFirstname(); ?>&nbsp;<?php echo $this->contact->getLastname(); ?></dd>
	<?php if($this->company->getName()!==null): ?>
		<dt><?php echo JText::_("COM_ZIBBRA_COMPANY"); ?>:</dt>
		<dd><?php echo $this->company->getName(); ?></dd>
	<?php endif; ?>
	<dt><?php echo JText::_("COM_ZIBBRA_EMAIL"); ?>:</dt>
	<dd><a href="mailto:<?php echo $this->contact->getEmail(); ?>" target="_blank"><?php echo $this->contact->getEmail(); ?></a></dd>
	<?php if($this->contact->getPhone()!==null): ?>
		<dt><?php echo JText::_("COM_ZIBBRA_PHONE"); ?>:</dt>
		<dd><?php echo $this->contact->getPhone(); ?></dd>
	<?php endif; ?>
</dl>
<div class="clear"></div>
<h3>
	<span><?php echo JText::_("COM_ZIBBRA_BILLING_ADDRESS"); ?></span>
	<a href="<?php echo JRoute::_("index.php?option=com_zibbra&view=account&layout=edit&section=billing"); ?>"><?php echo JText::_("COM_ZIBBRA_EDIT"); ?></a>
</h3>
<?php echo $this->billing_address->getStreet(); ?> <?php echo $this->billing_address->getStreetnr(); ?> <?php echo $this->billing_address->getBox(); ?>
<br />
<?php echo $this->billing_address->getCountrycode(); ?>-<?php echo $this->billing_address->getZipcode(); ?> <?php echo $this->billing_address->getCity(); ?>
<h3>
	<span><?php echo JText::_("COM_ZIBBRA_SHIPPING_ADDRESS"); ?></span>
	<a href="<?php echo JRoute::_("index.php?option=com_zibbra&view=account&layout=edit&section=shipping"); ?>"><?php echo JText::_("COM_ZIBBRA_EDIT"); ?></a>
</h3>
<?php echo $this->shipping_address->getStreet(); ?> <?php echo $this->shipping_address->getStreetnr(); ?> <?php echo $this->shipping_address->getBox(); ?>
<br />
<?php echo $this->shipping_address->getCountrycode(); ?>-<?php echo $this->shipping_address->getZipcode(); ?> <?php echo $this->shipping_address->getCity(); ?>