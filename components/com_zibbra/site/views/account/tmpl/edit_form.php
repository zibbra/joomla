<?php

// No direct access to this file

defined("_JEXEC") or die("Restricted access");

JHtml::_("behavior.keepalive");
JHtml::_("behavior.tooltip");
JHtml::_("behavior.formvalidation");
JHtml::_("behavior.noframes");
JHtml::_("behavior.modal");

$return = JRequest::getVar("return",false);

?>
<form id="zibbra-account-edit" action="<?php echo JRoute::_("index.php?option=com_zibbra&task=account.submit"); ?>" method="post" class="form-validate">

	<?php foreach($this->form->getFieldsets() as $fieldset): ?>
		<?php $fields = $this->form->getFieldset($fieldset->name); ?>
		<?php if(count($fields)): ?>
			<fieldset>
				<?php if(isset($fieldset->label)): ?>
					<legend><?php echo JText::_($fieldset->label);?></legend>
				<?php endif;?>
				<dl>
					<?php foreach($fields as $field): ?>
						<?php if($field->hidden): ?>
							<?php echo $field->input;?>
						<?php else:?>
							<?php if($field->type=="Checkbox"): ?>
								<dd class="<?php echo strtolower($field->type); ?>"><?php echo ($field->type!="Spacer") ? $field->input : "&#160;"; ?></dd>
							<?php endif; ?>
							<dt class="<?php echo strtolower($field->type); ?>">
								<?php echo $field->label; ?>
								<?php if(!$field->required && $field->type!="Spacer" && $field->type!="Checkbox"): ?>
									<span class="optional"><?php echo JText::_("COM_ZIBBRA_OPTIONAL"); ?></span>
								<?php endif; ?>
							</dt>
							<?php if($field->type!="Checkbox"): ?>
								<dd class="<?php echo strtolower($field->type); ?>"><?php echo ($field->type!="Spacer") ? $field->input : "&#160;"; ?></dd>
							<?php endif; ?>
						<?php endif;?>
					<?php endforeach;?>
				</dl>
			</fieldset>
		<?php endif;?>
	<?php endforeach;?>
	
	<div>
		<input class="button" type="submit" name="submit" value="<?php echo JText::_("COM_ZIBBRA_SUBMIT"); ?>" class="validate" />
		<?php echo JText::_("COM_ZIBBRA_OR");?>
		<a href="<?php echo $return ? $return : JRoute::_("index.php?option=com_zibbra&view=account"); ?>" title="<?php echo JText::_("JCANCEL");?>"><?php echo JText::_("JCANCEL");?></a>
		<input type="hidden" name="task" value="account.submit" />
		<input type="hidden" name="section" value="<?php echo $this->section; ?>" />
		<input type="hidden" name="return" value="<?php echo base64_encode($return ? $return : JRoute::_("index.php?option=com_zibbra&view=account")); ?>" />
		<?php echo JHtml::_("form.token"); ?>
	</div>

</form>