<?php

// No direct access to this file

defined("_JEXEC") or die("Restricted access");

$this->section = JRequest::getVar("section");

switch($this->section) {
	
	case "account": echo $this->loadTemplate("account");break;
	case "billing": echo $this->loadTemplate("billing");break;
	case "shipping": echo $this->loadTemplate("shipping");break;
	
} // end switch

?>