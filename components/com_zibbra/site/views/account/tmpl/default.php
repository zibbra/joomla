<?php

// No direct access to this file

defined("_JEXEC") or die("Restricted access");
?>
<h2><?php echo JText::_("COM_ZIBBRA_YOUR_ACCOUNT"); ?></h2>

<table cellpadding="0" cellspacing="0" border="0" width="100%" id="zibbra-account">
	<tbody>
		<tr>
			<td valign="top">
				<?php echo $this->loadTemplate("account"); ?>
			</td>
			<td valign="top" width="20"><img src="/media/com_zibbra/images/spacer.gif" border="0" width="20" /></td>
			<td valign="top">
				<?php echo $this->loadTemplate("orders"); ?>
				<?php echo $this->loadTemplate("invoices"); ?>
			</td>
		</tr>
	</tbody>
</table>