<?php

function ZibbraBuildRoute(&$query) {
	
	$segments = array();
	
	if(isset($query['view'])) {
		
		$segments[] = $query['view'];
		unset($query['view']);
		
	} // end if
	
	if(isset($query['id']))	{
		
		$segments[] = $query['id'];
		unset($query['id']);
		
	} // end if
	
	return $segments;
	
} // end function

function ZibbraParseRoute($segments) {
	
	$vars = array();
	
	switch($segments[0]) {
		
		case "catalog": {
			
			$vars['view'] = "catalog";
			
			if(isset($segments[1])) {
			
				$id = explode(":",$segments[1]);
				$vars['id'] = (int) $id[0];
			
			} // end if
			
		};break;
		
		case "product": {
			
			$vars['view'] = "product";
			$id = explode(":",$segments[1]);
			$vars['id'] = (int) $id[0];
			
		};break;
		
		default: {
			
			$vars['view'] = $segments[0];
			
		};break;
		
	} // end switch
	
	return $vars;
	
} // end function

?>