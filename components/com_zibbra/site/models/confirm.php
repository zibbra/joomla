<?php

// No direct access to this file

defined("_JEXEC") or die("Restricted access");
 
// Import Joomla modelitem library

jimport("joomla.application.component.modelform");

class ZibbraModelConfirm extends JModelForm {
	
	private $customer;
	private $order;
	
	public function getCustomer() {
	
		if(!isset($this->customer)) {
			
			$this->customer = ZCustomer::load();
			
		} // end if
	
		return $this->customer;
		
	} // end function
	
	public function getOrder() {
	
		if(!isset($this->order)) {
		
			// Get the library adapter
			
			$adapter = Zibbra::getInstance()->getLibrary()->getAdapter();
			
			// Load the order
			
			$this->order = ZOrder::load($adapter->getSessionValue("order.id",0));
			
		} // end if
	
		return $this->order;
		
	} // end function
	
	public function getForm($data=array(),$loadData=true) {
		
		$form = $this->loadForm("com_zibbra","confirm",array("control"=>"jform","load_data"=>$loadData));
		
		if(empty($form)) return false;
		
		return $form;
		
	} // end function
	
} // end class

?>