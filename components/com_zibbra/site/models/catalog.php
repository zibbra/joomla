<?php

// No direct access to this file

defined("_JEXEC") or die("Restricted access");
 
// Import Joomla modelitem library

jimport("joomla.application.component.modelform");

class ZibbraModelCatalog extends JModelForm {
	
	protected $category;
	protected $items;
	protected $pagination;
	protected $page = 1;
	protected $manufacturers;
	
	public function getFilters() {
		
		return ZProduct::getFilters();
		
	} // end function
	
	public function getCategory() {
			
		$categoryid = JRequest::getVar("id",null);
		
		if(is_null($categoryid)) return null;
		
		$cache = JFactory::getCache();
		$cache->setCaching(true);
		$cache->setLifeTime(JComponentHelper::getParams("com_zibbra")->get("default_cache",60));
		return $cache->call(array("ZCategory","getById"),$categoryid,true);
		
	} // end function
	
	public function getItems() {
	
		if(!isset($this->items)) {
		
			// Get the library adapter
			
			$adapter = Zibbra::getInstance()->getLibrary()->getAdapter();
			
			// Get component configuration

			$params = JComponentHelper::getParams("com_zibbra");
			
			// Check if we need to show only categories, in this case we will return an empty products array
			
			$only_categories = JRequest::getVar("only_categories","N");			
			if($only_categories==="Y") return array();
			
			// Check if the limit has changed, if so we need to change the page to 1

			$page = JRequest::getVar("page");
			$limit = (int) JRequest::getVar("limit",$params->get("default_limit",5));
			$old_limit = (int) $adapter->getSessionValue("limit",$params->get("default_limit",5));
			
			if($old_limit!=$limit) {
				
				$page = 1;
				$adapter->setSessionValue("limit",$limit);
				
			} // end if
			
			// Check if we have a specific sort type and direction for this category
			
			$cat_sort_type = JRequest::getVar("sort_type","global");
			$cat_sort_dir = JRequest::getVar("sort_dir","global");
			
			// Prepare the arguments for the query
			
			$categoryid = JRequest::getVar("id",null);	
			$price = JRequest::getVar("price");
			$properties = JRequest::getVar("properties");
			$manufacturers = JRequest::getVar("manufacturers");
			$in_stock = JRequest::getVar("in_stock");
			$sort_type = JRequest::getVar("st",$cat_sort_type!=="global" ? $cat_sort_type : $params->get("default_sort_type","times_sold"));
			$sort_dir = JRequest::getVar("sd",$cat_sort_dir!=="global" ? $cat_sort_dir : $params->get("default_sort_dir","desc"));
			
			if(!empty($page)) $this->page = (int) $page;
			
			if(!empty($price)) {
				
				$this->price = explode("-",$price);
				
				ZProduct::setPriceFilter((int) $this->price[0],(int) $this->price[1]);
			
			} // end if
			
			if(!empty($manufacturers)) {
				
				ZProduct::setManufacturerFilter($manufacturers);
			
			} // end if
			
			if(!empty($in_stock)) {
				
				ZProduct::setStockFilter($in_stock);
			
			} // end if
			
			if(!empty($properties)) {
				
				foreach($properties as $id=>$options) {
				
					ZProduct::setPropertyFilter((int) $id,$options);
					
				} // end foreach
			
			} // end if
			
			$pagination = array(
				"page"=>$this->page,
				"limit"=>$limit
			);
			
			$this->items = ZProduct::getProducts($pagination,$categoryid,true,$sort_type,$sort_dir);
			$this->pagination = $pagination;
			
		} // end if
	
		return $this->items;
	
	} // end function
	
	public function getPagination() {
	
		return $this->pagination;
	
	} // end function

	public function getForm($data=array(),$loadData=true) {
		
		$form = $this->loadForm("com_zibbra","catalog",array("control"=>"jform","load_data"=>$loadData));
		
		$form->setValue("test1",null,"val1");
		$form->setValue("test2",null,"val2");
		
		if(empty($form)) return false;
		
		return $form;
		
	} // end function
	
	public function getManufacturers() {
	
		if(!isset($this->manufacturers)) {
			
			$ids = JRequest::getVar("manufacturers");
		
			$cache = JFactory::getCache();
			$cache->setCaching(true);
			$cache->setLifeTime(JComponentHelper::getParams("com_zibbra")->get("default_cache",60));
			$manufacturers = $cache->call(array("ZManufacturer","getManufacturers"));
			
			if($ids!=null) {
			
				foreach($manufacturers as $manufacturer) {
					
					if(in_array($manufacturer->getManufacturerid(),$ids)) $this->manufacturers[] = $manufacturer;
					
				} // end foreach
			
			} // end if
			
		} // end if
	
		return $this->manufacturers;
	
	} // end function
	
} // end class

?>