<?php

// No direct access to this file

defined("_JEXEC") or die("Restricted access");
 
// Import Joomla modelitem library

jimport("joomla.application.component.modelform");

class ZibbraModelAccount extends JModelForm {
	
	private $customer;
	private $company;
	private $contact;
	private $billing_address;
	
	public function getCustomer() {
	
		if(!isset($this->customer)) {
			
			$this->customer = ZCustomer::load();
			
		} // end if
	
		return $this->customer;
		
	} // end function
	
	public function getCompany() {
	
		if(!isset($this->customer)) $this->getCustomer();
	
		if(!isset($this->company)) {
			
			$this->company = $this->customer->getCompany();
			
		} // end if
	
		return $this->company;
		
	} // end function
	
	public function getContact() {
	
		if(!isset($this->customer)) $this->getCustomer();
	
		if(!isset($this->contact)) {
			
			$this->contact = $this->customer->getPrimaryContact();
			
		} // end if
	
		return $this->contact;
		
	} // end function
	
	public function getBillingAddress() {
	
		if(!isset($this->customer)) $this->getCustomer();
	
		if(!isset($this->billing_address)) {
			
			$this->billing_address = $this->customer->getBillingAddress();
			
		} // end if
	
		return $this->billing_address;
		
	} // end function
	
	public function getShippingAddress() {
	
		if(!isset($this->customer)) $this->getCustomer();
	
		if(!isset($this->shipping_address)) {
			
			$this->shipping_address = $this->customer->getShippingAddress();
			
		} // end if
	
		return $this->shipping_address;
		
	} // end function
	
	public function getOrders() {
	
		if(!isset($this->customer)) $this->getCustomer();
		
		return $this->customer->getOrders();
		
	} // end function
	
	public function getRecentOrders() {
	
		if(!isset($this->customer)) $this->getCustomer();
		
		return $this->customer->getOrders(5);
		
	} // end function
	
	public function getInvoices() {
	
		if(!isset($this->customer)) $this->getCustomer();
		
		return $this->customer->getInvoices();
		
	} // end function
	
	public function getRecentInvoices() {
	
		if(!isset($this->customer)) $this->getCustomer();
		
		return $this->customer->getInvoices(5);
		
	} // end function
	
	public function getForm($data=array(),$loadData=true) {
		
		$form = null;
		$section = JRequest::getVar("section",false);
		
		if($section) {
			
			$form = $this->loadForm("com_zibbra","register",array("control"=>"jform","load_data"=>$loadData));
			
		} // end if
		
		switch($section) {
			
			case "account": {
				
				$form->removeGroup("account");
				$form->removeGroup("billing");
				$form->removeGroup("shipping");

				$contact = $this->getContact();

				$form->setValue("title","contact",$contact->getTitle());
				$form->setValue("firstname","contact",$contact->getFirstname());
				$form->setValue("lastname","contact",$contact->getLastname());
				$form->setValue("gender","contact",$contact->getGender());
				$form->setValue("mobile","contact",$contact->getMobile());
				$form->setValue("phone","contact",$contact->getPhone());
				$form->setValue("email","contact",$contact->getEmail());
				
				$company = $this->getCompany();
				
				$form->setValue("name","company",$company->getName());
				$form->setValue("phone","company",$company->getPhone());
				$form->setValue("fax","company",$company->getFax());
				$form->setValue("email","company",$company->getEmail());
				$form->setValue("website","company",$company->getWebsite());
				$form->setValue("vat_nr","company",$company->getVatNr());
				
			};break;
			
			case "billing": {
				
				$form->removeGroup("account");
				$form->removeGroup("contact");
				$form->removeGroup("company");
				$form->removeGroup("shipping");

				$address = $this->getBillingAddress();
				
				$form->setValue("street","billing",$address->getStreet());
				$form->setValue("streetnr","billing",$address->getStreetnr());
				$form->setValue("box","billing",$address->getBox());
				$form->setValue("zipcode","billing",$address->getZipcode());
				$form->setValue("city","billing",$address->getCity());
				$form->setValue("countrycode","billing",$address->getCountrycode());
				
			};break;
			
			case "shipping": {
				
				$form->removeGroup("account");
				$form->removeGroup("contact");
				$form->removeGroup("company");
				$form->removeGroup("billing");
				$form->removeField("toggle","shipping");

				$address = $this->getShippingAddress();
				
				$form->setValue("street","shipping",$address->getStreet());
				$form->setValue("streetnr","shipping",$address->getStreetnr());
				$form->setValue("box","shipping",$address->getBox());
				$form->setValue("zipcode","shipping",$address->getZipcode());
				$form->setValue("city","shipping",$address->getCity());
				$form->setValue("countrycode","shipping",$address->getCountrycode());
				
			};break;
			
		} // end switch
		
		if(empty($form)) return false;
		
		return $form;
		
	} // end function
	
	public function updateCustomer($section,$data) {
		
		if(in_array($section,array("account","billing","shipping"))) {			
		
			$customer = $this->getCustomer();
			
			switch($section) {
				
				case "account": {
					
					$company = $customer->getCompany();
					
					$company->setName($data['company']['name']);
					$company->setPhone($data['company']['phone']);
					$company->setFax($data['company']['fax']);
					$company->setEmail($data['company']['email']);
					$company->setWebsite($data['company']['website']);
					$company->setVatNr($data['company']['vat_nr']);
					
					$contact = $customer->getPrimaryContact();
					
					$contact->setTitle($data['contact']['title']);
					$contact->setFirstname($data['contact']['firstname']);
					$contact->setLastname($data['contact']['lastname']);
					$contact->setGender($data['contact']['gender']);
					$contact->setMobile($data['contact']['mobile']);
					$contact->setPhone($data['contact']['phone']);
					$contact->setEmail($data['contact']['email']);
					
				};break;
				
				case "billing":
				case "shipping": {
					
					$func = "get".ucfirst($section)."Address";
					$address = $customer->$func();
					$address->setStreet($data[$section]['street']);
					$address->setStreetnr($data[$section]['streetnr']);
					$address->setBox($data[$section]['box']);
					$address->setZipcode($data[$section]['zipcode']);
					$address->setCity($data[$section]['city']);
					$address->setCountrycode($data[$section]['countrycode']);
					
				};break;
				
			} // end switch
			
			$customer->save();
			
		} // end if
		
	} // end function
	
} // end class

?>