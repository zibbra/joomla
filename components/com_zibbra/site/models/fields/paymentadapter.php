<?php

// No direct access to this file

defined("_JEXEC") or die("Restricted access");
 
// Import the list field type

jimport("joomla.form.helper");
JFormHelper::loadFieldClass("radio");

class JFormFieldPaymentadapter extends JFormFieldRadio {
	
	protected $type = "Paymentadapter";
	private static $adapters = array();
	
	public function getInput() {
		
		$html = array();

		// Initialize some field attributes
		
		$class = $this->element['class'] ? " class=\"radio ".(string) $this->element['class']."\"" : " class=\"radio\"";
		
		// Start the radio field output
		
		$html[] = "<fieldset id=\"".$this->id."\"".$class.">";
		
		// Get the field options
		
		$options = $this->getOptions();
		
		// Build the radio field output
		
		foreach($options as $i=>$option) {
			
			// Initialize some option attributes
			
			$checked = ((string) $option->value == (string) $this->value) ? " checked=\"checked\"" : "";
			$class = !empty($option->class) ? " class=\"".$option->class."\"" : "";
			$disabled = !empty($option->disable) ? " disabled=\"disabled\"" : "";
		
			// Initialize some JavaScript option attributes
			
			$onclick = !empty($option->onclick) ? " onclick=\"".$option->onclick."\"" : "";
			
			// Generate the input field
			
			$input = "<input type=\"radio\"";
			$input .= " id=\"".$this->id.$i."\"";
			$input .= " name=\"".$this->name."\"";
			$input .= " value=\"".htmlspecialchars($option->value, ENT_COMPAT, 'UTF-8')."\"";
			$input .= $checked.$class.$onclick.$disabled;
			$input .= " />";
			
			// Generate the label
			
			switch($option->value) {
				
				case "icepay_basic": $label = $this->getIcepayBasicLabel($this->id.$i,$class,$option);break;
				default: $label = $this->getDefaultLabel($this->id.$i,$class,$option);break;
				
			} // end switch
			
			// Append to HTML
			
			$html[] = $input;
			$html[] = $label;
			 
		} // end foreach
		
		// End the radio field output
		
		$html[] = "</fieldset>";
		
		return implode($html);
		
	} // end function
	
	protected function getOptions() {
		
		$options = array();
			
		if(($response = ZPayment::load())===false) {
			
			$this->setError("ZIBBRA: Unable to load payment adapters");
		
		} // end if
		
		foreach($response as $oPayment) {
			
			self::$adapters[$oPayment->getType()] = $oPayment;
			
			$options[] = JHtml::_("select.option",$oPayment->getType(),$oPayment->getName()." (&euro;&nbsp;".number_format($oPayment->getPrice(),2,",","").")");
			
		} // end foreach
		
		$options = array_merge(parent::getOptions(),$options);
		
		return $options;
		
	} // end function
	
	private function getIcepayBasicLabel($fieldid, $class, $option) {
		
		$icepay = self::$adapters['icepay_basic'];
		$text = $icepay->getCheckoutTitle()." (&euro; ".number_format($icepay->getPrice(),2,",","").")";
		
		$label = $this->getDefaultLabel($fieldid,$class,$option,$text);
		$label .= "<div class=\"clear\" style=\"margin-left:29px;\">";
		$label .= "<img src=\"/media/com_zibbra/images/payment-icepay-basic.png\" style=\"margin: 6px 0 6px 0;\" border=\"0\" />";
		$label .= "<p style=\"font-size:8pt;\">".$icepay->getDescription()."</p>";
		$label .= "</div>";
				
		return $label;
		
	} // end function
	
	private function getDefaultLabel($fieldid, $class, $option, $text = false) {
		
		$label = "<label";
		$label .= " for=\"".$fieldid."\"";
		$label .= $class;
		$label .= ">";
		$label .= ($text!==false ? $text : $option->text);
		$label .= "</label>";
		
		return $label;
		
	} // end function
	
} // end class