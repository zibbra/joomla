<?php

// No direct access to this file

defined("_JEXEC") or die("Restricted access");
 
// Import the list field type

jimport("joomla.form.helper");
JFormHelper::loadFieldClass("list");
 
class JFormFieldCountry extends JFormFieldList {
	
	/**
	 * The field type
	 *
	 * @var string
	 */
	protected $type = "Country";
 
	/**
	 * Method to get a list of options for a list input
	 *
	 * @return	array An array of JHtml options
	 */
	protected function getOptions() {
		
		$options = array();
		
		$cache = JFactory::getCache();
		$cache->setCaching(true);
		$cache->setLifeTime(24*60);
		$response = $cache->call(array("ZCountry","load"));
		
		if(!$response instanceof ZApiError) {
		
			foreach($response as $oCountry) {
				
				$options[] = JHtml::_("select.option",$oCountry->getCountrycode(),$oCountry->getName());
				
			} // end foreach
		
		} // end if
		
		$options = array_merge(parent::getOptions(), $options);
		
		return $options;
		
	} // end function
	
} // end class