<?php

// No direct access to this file

defined("_JEXEC") or die("Restricted access");
 
// Import the list field type

jimport("joomla.form.helper");
JFormHelper::loadFieldClass("radio");
 
class JFormFieldShippingadapter extends JFormFieldRadio {
	
	protected $type = "Shippingadapter";
	
	protected function getInput() {
		
		$options = $this->getOptions();
		$html = "<fieldset id=\"jform_checkout_shipping_method\" class=\"radio\">";		
		
		foreach($options as $index=>$option) {
			
			$html .= "<input type=\"radio\" id=\"jform_checkout_shipping_method".$index."\" name=\"jform[checkout][shipping_method]\" value=\"".$option->value."\"".($this->value==$option->value ? " checked=\"checked\"" : "")." /><label for=\"jform_checkout_shipping_method".$index."\">".$option->text."</label>";	
			
		} // end foreach
		
		$html .= "</fieldset>";
		
		return $html;
		
	} // end function
 
	protected function getOptions() {
		
		$options = array();
			
		if(($response = ZShippingMethod::load())===false) {
			
			$this->setError("ZIBBRA: Unable to load shipping adapters");
		
		} // end if
		
		foreach($response as $index=>$oShipping) {
			
			$price = $oShipping->getPriceVatIncl();
			$name = $oShipping->getName()." (&euro;&nbsp;".number_format($price,2,",","").")";
			
			//exit("###".$name."###");
			
			$options[] = JHtml::_("select.option",$oShipping->getEnterpriseshippingmethodid(),$name);
			
		} // end foreach
		
		$options = array_merge(parent::getOptions(),$options);
		
		return $options;
		
	} // end function
	
} // end class