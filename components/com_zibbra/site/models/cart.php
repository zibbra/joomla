<?php

// No direct access to this file

defined("_JEXEC") or die("Restricted access");
 
// Import Joomla modelitem library

jimport("joomla.application.component.modelform");

class ZibbraModelCart extends JModelForm {
	
	private $cart;
	private $shipping;
	private $payment;
	
	public function getCart() {
	
		if(!isset($this->cart)) {
			
			$this->cart = ZCart::getInstance();
			
		} // end if
	
		return $this->cart;
	
	} // end function
	
	public function getShipping() {
	
		if(!isset($this->shipping)) {
			
			$cart = $this->getCart();
			$this->shipping = $cart->getShippingMethod();
			
		} // end if
	
		return $this->shipping;
		
	} // end function
	
	public function getPayment() {
	
		if(!isset($this->payment)) {
			
			$cart = $this->getCart();
			$this->payment = $cart->getPayment();
			
		} // end if
	
		return $this->payment;
		
	} // end function
	
	public function getForm($data=array(),$loadData=true) {
		
		$form = $this->loadForm("com_zibbra","cart",array("control"=>"jform","load_data"=>$loadData));
		
		if(empty($form)) return false;
		
		return $form;
		
	} // end function
	
} // end class

?>