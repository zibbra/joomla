<?php

// No direct access to this file

defined("_JEXEC") or die("Restricted access");
 
// Import Joomla modelitem library

jimport("joomla.application.component.modellist");

class ZibbraModelManufacturers extends JModelList {
	
	protected $items;
	
	public function getItems() {
	
		if(!isset($this->items)) {
		
			$cache = JFactory::getCache();
			$cache->setCaching(true);
			$cache->setLifeTime(JComponentHelper::getParams("com_zibbra")->get("default_cache",60));
			$manufacturers = $cache->call(array("ZManufacturer","getManufacturers"));
			
			foreach($manufacturers as $manufacturer) {
				
				$firstLetter = strtoupper(substr($manufacturer->getName(),0,1));
				
				if(!isset($this->items[$firstLetter])) $this->items[$firstLetter] = array();
				
				$this->items[$firstLetter][] = $manufacturer;
				
			} // end foreach
			
		} // end if
	
		return $this->items;
	
	} // end function
	
} // end class

?>