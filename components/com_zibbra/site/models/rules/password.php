<?php

// No direct access to this file

defined("_JEXEC") or die("Restricted access");
 
// Import Joomla formrule library

jimport("joomla.form.formrule");
 
/**
 * Password validation
 */
class JFormRulePassword extends JFormRule {
	
	protected $regex = "^[^0-9]+$";
	
} // end class