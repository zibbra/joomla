<?php

// No direct access to this file

defined("_JEXEC") or die("Restricted access");
 
// Import Joomla modelitem library

jimport("joomla.application.component.modelform");

class ZibbraModelShipping extends JModelForm {
	
	private $config = array();
	private $settings = array();
	
	public function getConfig() {
		
		if(!count($this->config)) {
			
			$adapter = Zibbra::getInstance()->getLibrary()->getAdapter();
			
			if(!is_null($type = $adapter->getSessionValue("shipping.type",null))) {
				
				$this->config = $adapter->getSessionValue("shipping.".$type,array());
				
			} // end if
			
		} // end if
	
		return $this->config;
	
	} // end function
	
	public function getSettings() {
		
		if(!count($this->settings)) {
			
			$adapter = Zibbra::getInstance()->getLibrary()->getAdapter();
				
			if(!is_null($type = $adapter->getSessionValue("shipping.settings",null))) {
			
				$this->settings = $adapter->getSessionValue("shipping.settings",array());
			
			} // end if			
			
		} // end if
		
		return $this->settings;
		
	} // end function
	
	public function getForm($data = array(), $loadData = true) {
		
		return false;
		
	} // end function

} // end class

?>