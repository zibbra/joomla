<?php

// No direct access to this file

defined("_JEXEC") or die("Restricted access");
 
// Import Joomla modelitem library

jimport("joomla.application.component.modelform");

class ZibbraModelPassword extends JModelForm {
	
	private $customer;
	
	public function getCustomer() {
	
		if(!isset($this->customer)) {
			
			$this->customer = ZCustomer::load();
			
		} // end if
	
		return $this->customer;
		
	} // end function
	
	public function getForm($data=array(),$loadData=true) {
		
		if(isset($_GET['layout'])) {
		
			$form = $this->loadForm("com_zibbra","reset",array("control"=>"jform","load_data"=>$loadData));
			
		}else{
		
			$form = $this->loadForm("com_zibbra","password",array("control"=>"jform","load_data"=>$loadData));
		
		} // end if
		
		if(empty($form)) return false;
		
		return $form;
		
	} // end function
	
	public function updatePassword($section,$data) {
			
		$adapter = Zibbra::getInstance()->getLibrary()->getAdapter();
		
		if($data['password']['new_password']==$data['password']['verify_password']) {
		
			if($adapter->hasSessionValue("user")) {
				
				if($user = unserialize($adapter->getSessionValue("user"))) {
					
					if($user instanceof ZCustomerUser) {
						
						return $user->changePassword($data['password']['old_password'],$data['password']['new_password']);
						
					} // end if
					
				} // end if
			
			} // end if
		
		} // end if
		
		return false;
		
	} // end function
	
	public function resetPassword($section,$data) {
		
		ZCustomerUser::resetPassword($data['reset']['email']);
		
	} // end function
	
} // end class

?>