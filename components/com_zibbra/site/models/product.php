<?php

// No direct access to this file

defined("_JEXEC") or die("Restricted access");
 
// Import Joomla modelitem library

jimport("joomla.application.component.modelform");

class ZibbraModelProduct extends JModelForm {
	
	private $product;
	
	public function getVariations($variations) {
		
		$oProduct = $this->getProduct();
		return $oProduct->getVariationCombinations($variations);
		
	} // end function
	
	public function getProduct() {
		
		$productid = JRequest::getVar("id");
	
		if(!isset($this->product)) {
		
			$cache = JFactory::getCache();
			$cache->setCaching(true);
			$cache->setLifeTime(JComponentHelper::getParams("com_zibbra")->get("default_cache",60));
			$this->product = $cache->call(array("ZProduct","getProduct"),$productid);			
			//$this->product = ZProduct::getProduct($productid);
			
		} // end if
	
		return $this->product;
	
	} // end function

	public function getForm($data=array(),$loadData=true) {
		
		$form = $this->loadForm("com_zibbra","product",array("control"=>"jform","load_data"=>$loadData));
		
		$form->setValue("productid",null,JRequest::getVar("id"));
		
		if(empty($form)) return false;
		
		return $form;
		
	} // end function
	
} // end class

?>