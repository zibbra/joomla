<?php

// No direct access to this file

defined("_JEXEC") or die("Restricted access");
 
// Import Joomla modelitem library

jimport("joomla.application.component.modelform");

class ZibbraModelRegister extends JModelForm {
	
	public function getForm($data=array(),$loadData=true) {
		
		$form = $this->loadForm("com_zibbra","register",array("control"=>"jform","load_data"=>$loadData));		
		
		$oCountry = ZCountry::getDefaultCountry();
			
		if(!empty($oCountry)) {
	
			$form->setValue("countrycode","billing",$oCountry->getCountrycode());
			$form->setValue("countrycode","shipping",$oCountry->getCountrycode());
				
		} // end if
		
		if(empty($form)) return false;
		
		return $form;
		
	} // end function
	
	public function saveCustomer($data) {
		
		// Create objects
		
		$oCustomer = new ZCustomer();
		$oUser = new ZCustomerUser();
		$oCompany = ZCustomerCompany::parse($data['company'],null);
		$oContact = ZCustomerContact::parse($data['contact']);
		$oBillingAddress = ZCustomerAddress::parse(ZCustomerAddress::TYPE_BILLING,$data['billing'],false);
		
		if(isset($data['shipping']['toggle']) && $data['shipping']['toggle']=="1") {
		
			$oShippingAddress = ZCustomerAddress::parse(ZCustomerAddress::TYPE_SHIPPING,$data['billing'],false);
		
		}else{
		
			$oShippingAddress = ZCustomerAddress::parse(ZCustomerAddress::TYPE_SHIPPING,$data['shipping'],false);
			
		} // end if
		
		// Set user info
		
		$oUser->setFirstname($data['contact']['firstname']);
		$oUser->setLastname($data['contact']['lastname']);
		$oUser->setEmail($data['contact']['email']);
		$oUser->setUsername($data['contact']['email']);
		$oUser->setPassword($data['account']['new_password']);
		
		// Get the adapter
		
		$adapter = Zibbra::getInstance()->getLibrary()->getAdapter();
		
		// Assign to customer
		
		$oCustomer->setLanguagecode($adapter->getLanguageCode());
		$oCustomer->setCompany($oCompany);
		$oCustomer->addContact($oContact);
		$oCustomer->setBillingAddress($oBillingAddress);
		$oCustomer->setShippingAddress($oShippingAddress);
		$oCustomer->addUser($oUser);
		
		// Save
		
		return $oCustomer->save();
		
	} // end function
	
} // end class

?>