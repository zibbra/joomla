<?php

// No direct access to this file

defined("_JEXEC") or die("Restricted access");
 
// Import Joomla modelitem library

jimport("joomla.application.component.modelform");

class ZibbraModelCheckout extends JModelForm {
	
	private $cart;
	private $customer;
	private $shippingmethod;
	private $payment;
	
	public function updateCart($shipping,$payment) {
		
		$oCart = $this->getCart();
		$oCart->setShippingMethod($shipping);
		$oCart->setPayment($payment);
		
	} // end function
	
	public function placeOrder($comments=null) {
		
		$oCart = $this->getCart();
		
		return $oCart->cartToOrder($comments);
		
	} // end function
	
	public function getCart() {
	
		if(!isset($this->cart)) {
			
			if(($this->cart = ZCart::getInstance())===false) {
				
				$this->setError("ZIBBRA: Unable to load shopping cart");
			
			} // end if
			
		} // end if
	
		return $this->cart;
	
	} // end function
	
	public function getCustomer() {
	
		if(!isset($this->customer)) {
			
			$this->customer = ZCustomer::load();
			
		} // end if
	
		return $this->customer;
		
	} // end function
		
	public function getShippingMethod() {
		
		if(!isset($this->shippingmethod)) {
			
			$oCart = $this->getCart();
			$this->shippingmethod = $oCart->getShippingMethod();
			
		} // end if
		
		return $this->shippingmethod;
	
	} // end function
	
	public function getShippingAdapter() {
		
		if(!$oShippingMethod = $this->getShippingMethod()) {
			
			return false;
			
		} // end if
		
		return $oShippingMethod->getShippingAdapter();
		
	} // end function
	
	public function getPayment() {
	
		if(!isset($this->payment)) {
			
			$cart = $this->getCart();
			$this->payment = $cart->getPayment();
			
		} // end if
	
		return $this->payment;
		
	} // end function
	
	public function getForm($data=array(),$loadData=true) {
		
		// Load stored shipping and payment
		
		$shipping = $this->getShippingMethod();
		$payment = $this->getPayment();
		
		// Get form
		
		$form = $this->loadForm("com_zibbra","checkout",array("control"=>"jform","load_data"=>$loadData));
		
		// Set values
		
		$shippingid = false;
		
		if($shipping) {
			
			$shippingid = $shipping->getEnterpriseshippingmethodid();
			
		}else{
			
			$options = ZShippingMethod::load();
			
			if($options!==false && count($options)>=1) {
			
				$shippingid = $options[0]->getEnterpriseshippingmethodid();
			
			} // end if
			
		} // end if
		
		if($payment) {
			
			$paymentid = $payment->getType();
			
		}else{
			
			$options = ZPayment::load();
			$paymentid = $options[0]->getType();
			
		} // end if
		
		if(!$payment || !$shipping) {
		
			$this->updateCart($shippingid,$paymentid);
			header("Location: ".$_SERVER['PHP_SELF']);
			exit;
		
		} // end if
		
		$form->setValue("shipping_method","checkout",$shippingid);
		$form->setValue("payment_method","checkout",$paymentid);
		
		if(empty($form)) return false;
		
		return $form;
		
	} // end function
	
} // end class

?>