<?php

// No direct access to this file

defined("_JEXEC") or die("Restricted access");
 
// Import Joomla controlleradmin library

jimport("joomla.application.component.controller");

class ZibbraControllerBpost extends JController {
	
	private function init() {
		
		// Load and store the application and session
		
		$this->app = JFactory::getApplication();
		$this->session = JFactory::getSession();
		
		if(!$this->order = ZOrder::getInstance()) {
			
			throw new ZException("Cannot set the shipping method because order failed to load");
			
		} // end if
	
		// Populate the data array
				
		$this->data = JRequest::get();
		
	} // end function
	
	public function onConfirm() {
		
		// Initialize controller
		
		$this->init();
		
		// Log incoming request
		
		$enterpriseshippingmethodid = $this->session->get("shipping",null,"zibbra");
		$price = $this->session->get("shipping.price",0,"zibbra");				
		ZShippingAdapterBpost::confirm($this->order,$enterpriseshippingmethodid,$price,$this->data);
		$this->session->set("shipping.complete",true,"zibbra");
		
		// Redirect to the checkout cancel action
		
		$this->closeFrame();	
		
	} // end function
	
	public function onCancel() {
		
		// Initialize controller
		
		$this->init();
		
		// Log incoming request
		
		$this->log("cancel",print_r($this->data,true));
		$this->session->set("shipping.complete",true,"zibbra");
		
		// Redirect to the checkout cancel action

		$this->app->redirect(JRoute::_("index.php?option=com_zibbra&task=checkout.cancel",false));
		
	} // end function
	
	public function onError() {
		
		// Initialize controller
		
		$this->init();
		
		// Log incoming request
		
		$this->log("exception",print_r($this->data,true));
		
		// Redirect

		$this->app->enqueueMessage(JText::_("COM_ZIBBRA_SHIPPING_FAILED"));
		$this->app->redirect(JRoute::_("index.php?option=com_zibbra&view=checkout",false));
		
	} // end function
	
	private function log($type,$msg) {
		
		$logfile = realpath($this->app->getCfg("log_path"))."/".date("YmdHis")."_ogone_".$type.".log";
		file_put_contents($logfile,$msg);
		
	} // end function
	
	private function closeFrame() {
		
		header("Content-Type: text/html");
		echo "<html><head></head><body><script> window.parent.bpost.dismiss(); </script></body></html>";
		exit;		
		
	} // end function
	
} // end class