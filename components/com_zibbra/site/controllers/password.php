<?php

// No direct access to this file

defined("_JEXEC") or die("Restricted access");
 
// Import Joomla controlleradmin library

jimport("joomla.application.component.controller");

class ZibbraControllerPassword extends JController {
	
	public function reset() {
		
		// Get the application
		
		$app = JFactory::getApplication();

		// Populate the data array
		
		$data = array();
		$data['return'] = base64_decode(JRequest::getVar("return","","POST","BASE64"));
		$data['form'] = JRequest::getVar("jform",array(),"POST","array");
		$data['section'] = JRequest::getVar("section",false,"POST");

		// Set the return URL if empty
		
		if(empty($data['return'])) {
			
			$data['return'] = "/index.php?option=com_zibbra&view=password&layout=reset";
			
		} // end if
		
		$model = $this->getModel("password");
			
		$model->resetPassword($data['section'],$data['form']);
		$app->enqueueMessage(JText::_("COM_ZIBBRA_CUSTOMER_RESET_PASSWORD_DONE"));
		
		// Redirect to the return url

		$app->redirect(JRoute::_($data['return'],false));
		
	} // end function
	
	public function submit() {
		
		// Get the application
		
		$app = JFactory::getApplication();

		// Populate the data array
		
		$data = array();
		$data['return'] = base64_decode(JRequest::getVar("return","","POST","BASE64"));
		$data['form'] = JRequest::getVar("jform",array(),"POST","array");
		$data['section'] = JRequest::getVar("section",false,"POST");

		// Set the return URL if empty
		
		if(empty($data['return'])) {
			
			$data['return'] = "index.php?option=com_zibbra&view=account";
			
		} // end if
		
		$model = $this->getModel("password");
			
		if(!$model->updatePassword($data['section'],$data['form'])) {
			
			$app->enqueueMessage(JText::_("COM_ZIBBRA_CHANGE_PASSWORD_FAILED"));
			
		}else{
			
			$app->enqueueMessage(JText::_("COM_ZIBBRA_CHANGE_PASSWORD_SUCCESS"));
			
		} // end if
		
		// Redirect to the return url

		$app->redirect(JRoute::_($data['return'],false));
		
	} // end function
	
} // end class