<?php

// No direct access to this file

defined("_JEXEC") or die("Restricted access");
 
// Import Joomla controlleradmin library

jimport("joomla.application.component.controller");

class ZibbraControllerIcepay extends JController {
	
	private $app;
	private $adapter;
	private $data;
	private $initialized = false;
	
	private function init() {
		
		if($this->initialized) {
			
			return;
			
		} // end if
		
		// Load and store the application
		
		$this->app = JFactory::getApplication();
		
		// Load and store the library adapter
		
		$this->adapter = Zibbra::getInstance()->getLibrary()->getAdapter();

		// Populate the data array
				
		$this->data = JRequest::get();
		$this->initialized = true;
		
	} // end function
	
	public function onReturn() {
		
		// Initialize controller
		
		$this->init();
		
		switch($this->data['Status']) {
			
			case "OK": {
				
				return $this->onSuccess();
			
			};break;
			
			case "ERR": {
				
				return $this->onError();
			
			};break;
			
			default: {

				$this->adapter->log(LOG_DEBUG, "Icepay::onReturn [".print_r($this->data,true)."]");
				 
			};break;
			
		} // end switch
		
		$this->app->redirect(JRoute::_("index.php?option=com_zibbra&view=checkout",false));
		exit;
		
	} // end function
	
	public function onError() {
		
		// Initialize controller
		
		$this->init();
		
		// Log incoming request
		
		$this->adapter->log(LOG_DEBUG, "Icepay::onError [".print_r($this->data,true)."]");
		
		// Check if it is a "cancel" or "error" action
		
		if(stripos($_GET['StatusCode'],"cancel")===false) {
			
			// Failed payment

			$this->app->enqueueMessage(JText::_("COM_ZIBBRA_PAYMENT_FAILED")." (".$_GET['StatusCode'].")");
			
		} // end if
		
		$this->app->redirect(JRoute::_("index.php?option=com_zibbra&task=checkout.cancel",false));
		
	} // end function
	
	public function onSuccess() {
		
		// Initialize controller
		
		$this->init();
		
		// Log incoming request
		
		$this->adapter->log(LOG_DEBUG, "Icepay::onSuccess [".print_r($this->data,true)."]");
		
		// Get payment adapter from session
			
		$orderid = $this->adapter->getSessionValue("order.id",false);
		$order_amount = $this->adapter->getSessionValue("order.amount",false);
				
		// Add the payment to the order
		
		ZOrder::addCreditCardPayment($orderid,$order_amount,"icepay_basic");
		
		// Reset the cart
		
		ZCart::reset();

		// Redirect

		$params = JComponentHelper::getParams("com_zibbra");
		$return = $params->get("finish_uri",false);
		
		if($return) {
			
			$menu = $this->app->getMenu();
			$item = $menu->getItem($return);
			$return = $item->link;
		
		}else{
			
			$this->app->enqueueMessage(JText::_("COM_ZIBBRA_THANK_YOU_FOR_YOUR_ORDER"));
			$return = "index.php?option=com_zibbra&view=account";
			
		} // end if
		
		$this->app->redirect(JRoute::_($return,false));
		
	} // end function
	
} // end class