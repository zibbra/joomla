<?php

// No direct access to this file

defined("_JEXEC") or die("Restricted access");
 
// Import Joomla controlleradmin library

jimport("joomla.application.component.controller");

class ZibbraControllerAccount extends JController {
	
	public function submit() {
		
		// Get the application
		
		$app = JFactory::getApplication();

		// Populate the data array
		
		$data = array();
		$data['return'] = base64_decode(JRequest::getVar("return","","POST","BASE64"));
		$data['form'] = JRequest::getVar("jform",array(),"POST","array");
		$data['section'] = JRequest::getVar("section",false,"POST");

		// Set the return URL if empty
		
		if(empty($data['return'])) {
			
			$data['return'] = "index.php?option=com_zibbra&view=account";
			
		} // end if
		
		$model = $this->getModel("account");
		$model->updateCustomer($data['section'],$data['form']);
		
		// Redirect to the return url

		$app->redirect(JRoute::_($data['return'],false));
		
	} // end function
	
	public function downloadPdf() {	
		
		// Get the application
		
		$app = JFactory::getApplication();	

		// Populate the data array
		
		$data = JRequest::get();
		
		// Check the type
		
		if(!in_array($data['type'],array("quotation","order","invoice","creditnote"))) {			
			
			$app->enqueueMessage(JText::_("COM_ZIBBRA_CUSTOMER_INVALID_DOCUMENT_TYPE"),"error");
			echo "<script> window.close(); </script>";
			exit;
			
		} // end if
		
		$class = "ZCustomer".ucfirst($data['type']);
		
		$pdf_data = $class::download($data['nr']);
		
		header("Pragma: public");
		header("Expires: 0");
		header("Pragma: no-cache");
		header("Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
		header("Content-Type: application/force-download");
		header("Content-Type: application/octet-stream");
		header("Content-Type: application/download");
		header('Content-disposition: attachment; filename='.basename($data['nr'].".pdf"));
		header("Content-Type: application/pdf");
		header("Content-Transfer-Encoding: binary");
		header("Content-Length: ".strlen($pdf_data));
		echo $pdf_data;
		exit;
		
	} // end function
	
} // end class