<?php

// No direct access to this file

defined("_JEXEC") or die("Restricted access");
 
// Import Joomla controlleradmin library

jimport("joomla.application.component.controller");

class ZibbraControllerProduct extends JController {
	
	public function getVariations() {
		
		// Get the application
		
		$app = JFactory::getApplication();
		
		// Get the model
		
		$model = $this->getModel("product");
		
		// Get the selected variations
		
		$variations = JRequest::getVar("variations",false,"POST");
		
		if(!$variations) $variations = array();
		
		// Output variations

		header("Content-Type: application/json");
		echo json_encode($model->getVariations($variations));
		exit;
		
	} // end function
	
} // end class