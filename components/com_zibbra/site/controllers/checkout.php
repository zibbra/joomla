<?php

// No direct access to this file

defined("_JEXEC") or die("Restricted access");
 
// Import Joomla controlleradmin library

jimport("joomla.application.component.controller");

class ZibbraControllerCheckout extends JController {
	
	public function confirm() {
		
		// Get the application, params and library adapter
		
		$app = JFactory::getApplication();
		$params = JComponentHelper::getParams("com_zibbra");
		$adapter = Zibbra::getInstance()->getLibrary()->getAdapter();
		
		// Get the model
		
		$model = $this->getModel("checkout");
		
		// Get the cart and amount
		
		$cart = $model->getCart();
		$amount = $cart->getTotal();
		
		// Create the order

		$shippingadapter = $model->getShippingAdapter();
		$payment = $model->getPayment();

		// Place the order
		
		$oOrder = $model->placeOrder($adapter->getSessionValue("order.comments",null));
		
		// Store the orderid and amount
		
		$adapter->setSessionValue("order.id",$oOrder->getOrderid());
		$adapter->setSessionValue("order.amount",$amount);
		
		// Dispatch Shipping
		
		if($shippingadapter && !$adapter->hasSessionValue("shipping.complete")) {
				
			$this->confirmShipping($shippingadapter,$model->getCustomer(),$oOrder);
				
		} // end if
		
		$adapter->clearSessionValue("shipping");
		
		// Dispatch Payment
		
		if($payment && !$adapter->hasSessionValue("payment.complete")) {
				
			$this->confirmPayment($payment,$amount,$oOrder->getOrderid());
				
		} // end if

		$shipping_type = $adapter->getSessionValue("shipping.type",null);
		
		$adapter->clearSessionValue("payment");
		$adapter->clearSessionValue("shipping.complete");
		$adapter->clearSessionValue("shipping.type");
		$adapter->clearSessionValue("shipping.".$shipping_type);
		$adapter->clearSessionValue("shipping.settings");
		$adapter->clearSessionValue("shipping.price");
		$adapter->clearSessionValue("payment.complete");
		$adapter->clearSessionValue("order");
		$adapter->clearSessionValue("order.comments");
				
		// Redirect
		
		$return = $params->get("finish_uri",false);
		
		if($return) {
			
			$menu = $app->getMenu();
			$item = $menu->getItem($return);
			$return = $item->link;
		
		}else{
			
			$app->enqueueMessage(JText::_("COM_ZIBBRA_THANK_YOU_FOR_YOUR_ORDER"));
			$return = "index.php?option=com_zibbra&view=account";
			
		} // end if
		
		$app->redirect(JRoute::_($return,false));
		
	} // end function
	
	private function confirmShipping(ZShippingAdapter $shippingadapter, ZCustomer $oCustomer, ZOrder $oOrder) {
		
		$shippingadapter->dispatch($oCustomer,$oOrder);
		
	} // end function
	
	private function confirmPayment($payment,$amount,$orderid) {
		
		$payment->dispatch($amount,$orderid);
		
	} // end function
	
	public function update() {
		
		// Get the application and library adapter
		
		$app = JFactory::getApplication();
		$adapter = Zibbra::getInstance()->getLibrary()->getAdapter();
		
		// Get the model
		
		$model = $this->getModel("checkout");
		
		// Update the cart
		
		$shipping = JRequest::getVar("shipping",false,"POST");
		$payment = JRequest::getVar("payment",false,"POST");
		
		$model->updateCart($shipping,$payment);
		
		// Store comments in the session

		$comments = JRequest::getVar("comments",false,"POST");
		
		$adapter->setSessionValue("order.comments",$comments);
		
		// Redirect
		
		$app->redirect(JRoute::_("index.php?option=com_zibbra&view=checkout&ajax=true",false));		
		
	} // end function
	
	public function cancel() {
		
		// Get the application and library adapter
		
		$app = JFactory::getApplication();
		$adapter = Zibbra::getInstance()->getLibrary()->getAdapter();
		
		// Get the orderid from session
		
		$orderid = $adapter->getSessionValue("order.id",0);
		
		// Clear the orderid from session
		
		$adapter->clearSessionValue("order.id");
		$adapter->clearSessionValue("order.amount");
		
		// Cancel the order
		
		ZOrder::cancel($orderid);
		
		// Notify the user		

		$app->enqueueMessage(JText::_("COM_ZIBBRA_ORDER_CANCELLED"));
		
		// Redirect to the homepage
		
		$app->redirect("/");
		
	} // end function
	
} // end class