<?php

// No direct access to this file

defined("_JEXEC") or die("Restricted access");
 
// Import Joomla controlleradmin library

jimport("joomla.application.component.controllerform");

// Import users component

require_once(JPATH_SITE.DS."components".DS."com_users".DS."controller.php");
require_once(JPATH_SITE.DS."components".DS."com_users".DS."controllers".DS."user.php");

class ZibbraControllerRegister extends JControllerForm {
	
	public function submit() {
		
		// Populate the data array
		
		$data = array();
		$data['return'] = base64_decode(JRequest::getVar("return","","POST","BASE64"));
		$data['form'] = JRequest::getVar("jform",array(),"POST","array");

		// Set the return URL if empty
		
		if(empty($data['return'])) {
			
			$data['return'] = "index.php?option=com_zibbra&view=account";
			
		} // end if
		
		// Save the customer
		
		$model = $this->getModel();
		$model->saveCustomer($data['form']);
		
		// Execute login
		
		$login = new UsersControllerUser();
		
		JRequest::setVar("username",$data['form']['contact']['email']);
		JRequest::setVar("password",$data['form']['account']['new_password']);
		JRequest::setVar("return",base64_encode(JRoute::_($data['return'],false)));
		
		$login->execute("login");
		
	} // end function
	
} // end class