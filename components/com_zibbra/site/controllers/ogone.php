<?php

// No direct access to this file

defined("_JEXEC") or die("Restricted access");
 
// Import Joomla controlleradmin library

jimport("joomla.application.component.controller");

class ZibbraControllerOgone extends JController {
	
	private $app;
	private $adapter;
	private $data;
	
	private function init() {
		
		// Load and store the application
		
		$this->app = JFactory::getApplication();
		
		// Load and store the library adapter
		
		$this->adapter = Zibbra::getInstance()->getLibrary()->getAdapter();

		// Populate the data array
				
		$this->data = JRequest::get();
		
	} // end function
	
	public function onAccept() {
		
		// Initialize controller
		
		$this->init();
		
		// Log incoming request
		
		$this->adapter->log(LOG_DEBUG, "Ogone::onAccept [".print_r($this->data,true)."]");
		
		// Get payment adapter from session
			
		$payment = $this->adapter->getSessionValue("payment",false);
		$orderid = $this->adapter->getSessionValue("order.id",false);
		$order_amount = $this->adapter->getSessionValue("order.amount",false);
		
		$adapter = $payment ? ZPayment::get($payment) : false;
		
		if($adapter) {
				
			// Add the payment to the order
			
			ZOrder::addCreditCardPayment($orderid,$order_amount,"ogone");
			
			// Reset the cart
			
			ZCart::reset();
	
			// Redirect
	
			$params = JComponentHelper::getParams("com_zibbra");
			$return = $params->get("finish_uri",false);
			
			if($return) {
				
				$menu = $this->app->getMenu();
				$item = $menu->getItem($return);
				$return = $item->link;
			
			}else{
				
				$this->app->enqueueMessage(JText::_("COM_ZIBBRA_THANK_YOU_FOR_YOUR_ORDER"));
				$return = "index.php?option=com_zibbra&view=account";
				
			} // end if
			
			$this->app->redirect(JRoute::_($return,false));
			
		} // end if
		
		// Redirect to the checkout cancel action

		$this->app->enqueueMessage(JText::_("COM_ZIBBRA_PAYMENT_FAILED"));
		$this->app->redirect(JRoute::_("index.php?option=com_zibbra&view=checkout",false));
		
	} // end function
	
	public function onCancel() {
		
		// Initialize controller
		
		$this->init();
		
		// Log incoming request
		
		$this->adapter->log(LOG_DEBUG, "Ogone::onCancel [".print_r($this->data,true)."]");
		
		// Redirect to the checkout cancel action

		$this->app->redirect(JRoute::_("index.php?option=com_zibbra&task=checkout.cancel",false));
		
	} // end function
	
	public function onDecline() {
		
		// Initialize controller
		
		$this->init();
		
		// Log incoming request
		
		$this->adapter->log(LOG_DEBUG, "Ogone::onDecline [".print_r($this->data,true)."]");
		
		// Redirect

		$this->app->enqueueMessage(JText::_("COM_ZIBBRA_PAYMENT_FAILED"));
		$this->app->redirect(JRoute::_("index.php?option=com_zibbra&view=checkout",false));
		
	} // end function
	
	public function onException() {
		
		// Initialize controller
		
		$this->init();
		
		// Log incoming request
		
		$this->adapter->log(LOG_DEBUG, "Ogone::onException [".print_r($this->data,true)."]");
		
		// Redirect

		$this->app->enqueueMessage(JText::_("COM_ZIBBRA_PAYMENT_FAILED"));
		$this->app->redirect(JRoute::_("index.php?option=com_zibbra&view=checkout",false));
		
	} // end function
	
} // end class