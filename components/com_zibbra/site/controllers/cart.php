<?php

// No direct access to this file

defined("_JEXEC") or die("Restricted access");
 
// Import Joomla controlleradmin library

jimport("joomla.application.component.controller");

class ZibbraControllerCart extends JController {
	
	public function addProduct() {
		
		// Get the application
		
		$app = JFactory::getApplication();

		// Populate the data array
		
		$data = array();
		$data['return'] = base64_decode(JRequest::getVar("return","","POST","BASE64"));
		$data['productid'] = JRequest::getInt("id");

		// Set the return URL if empty
		
		if(empty($data['return'])) {
			
			$data['return'] = "index.php?option=com_zibbra&view=cart";
			
		} // end if

		// Set the return URL in the user state
		
		$app->setUserState("product.form.return",$data['return']);
		
		// Load the product
		
		$product = ZProduct::getProduct($data['productid']);
		
		// Check for required variations, if missing, redirect to detail page
		
		if($product->hasVariations() && !isset($_POST['variations'])) {

			$app->redirect(JRoute::_("index.php?option=com_zibbra&view=product&id=".$data['productid'],false));
			
		} // end if
		
		// Load the cart
		
		$cart = ZCart::getInstance();
		
		// Create an item
		
		$item = new ZCartItem();
		$item->setProductid($data['productid']);
		$item->setQuantity(1);
		
		if(isset($_POST['variations'])) {

			$item->setVariations($_POST['variations']);
			
		} // end if
		
		// Add the item to the cart
		
		$cart->addItem($item);
		
		// Save the cart
		
		$cart->save();
		
		// Redirect to the return url

		$app->redirect(JRoute::_($app->getUserState("product.form.return"),false));
		
	} // end function
	
	public function deleteItem() {
		
		// Get the application
		
		$app = JFactory::getApplication();

		// Populate the data array
		
		$data = array();
		$data['return'] = base64_decode(JRequest::getVar("return","","POST","BASE64"));
		$data['id'] = JRequest::getInt("id");

		// Set the return URL if empty
		
		if(empty($data['return'])) {
			
			$data['return'] = "index.php?option=com_zibbra&view=cart";
			
		} // end if
		
		// Load the cart
		
		$cart = ZCart::getInstance();
		
		// Load the item
		
		$item = $cart->getItem($data['id']);
		
		// Delete the item
		
		$item->delete();
		
		// Redirect to the return url

		$app->redirect(JRoute::_($data['return'],false));
		
	} // end function
	
	public function clear() {
		
		// Get the application
		
		$app = JFactory::getApplication();

		// Populate the data array
		
		$data = array();
		$data['return'] = base64_decode(JRequest::getVar("return","","POST","BASE64"));

		// Set the return URL if empty
		
		if(empty($data['return'])) {
			
			$data['return'] = "index.php?option=com_zibbra&view=cart";
			
		} // end if
		
		// Load the cart
		
		$cart = ZCart::getInstance();
		
		// Clear the cart
		
		$cart->clear();
		
		// Redirect to the return url

		$app->redirect(JRoute::_($data['return'],false));
		
	} // end function
	
	public function update() {
		
		// Get the application
		
		$app = JFactory::getApplication();

		// Populate the data array
		
		$data = array();
		$data['return'] = base64_decode(JRequest::getVar("return","","POST","BASE64"));
		$data['items'] = JRequest::getVar("quantity",array());

		// Set the return URL if empty
		
		if(empty($data['return'])) {
			
			$data['return'] = "index.php?option=com_zibbra&view=cart";
			
		} // end if
		
		// Load the cart
		
		$cart = ZCart::getInstance();
		
		// Update the items
		
		foreach($data['items'] as $id=>$quantity) {
			
			$item = $cart->getItem($id);
			
			if($item) {
				
				if((int) $quantity>0) {
				
					$item->setQuantity($quantity);
					$item->save();
					
				}else{
					
					$item->delete();
					
				} // end if
				
			} // end if
			
		} // end foreach
		
		// Redirect to the return url

		$app->redirect(JRoute::_($data['return'],false));
		
	} // end function
	
} // end class