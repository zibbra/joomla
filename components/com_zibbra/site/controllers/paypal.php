<?php

// No direct access to this file

defined("_JEXEC") or die("Restricted access");
 
// Import Joomla controlleradmin library

jimport("joomla.application.component.controller");

class ZibbraControllerPaypal extends JController {
	
	private $app;
	private $adapter;
	private $data;
	
	private function init() {
		
		// Load and store the application
		
		$this->app = JFactory::getApplication();
		
		// Load and store the library adapter
		
		$this->adapter = Zibbra::getInstance()->getLibrary()->getAdapter();

		// Populate the data array
				
		$this->data = JRequest::get();
		
	} // end function
	
	public function onReturn() {
		
		// Initialize controller
		
		$this->init();
		
		// Log incoming request
		
		$this->adapter->log(LOG_DEBUG, "Paypal::onReturn [".print_r($this->data,true)."]");
		
		// Check request params
		
		if(isset($this->data['token']) && isset($this->data['PayerID'])) {
			
			$this->adapter->setSessionValue("paypal.token", $this->data['token']);
			$this->adapter->setSessionValue("paypal.payerid", $this->data['PayerID']);
			
		} // end if
		
		// Redirect to the confirm page

		$this->app->redirect(JRoute::_("index.php?option=com_zibbra&view=confirm",false));
		
	} // end function
	
	public function onCancel() {
		
		// Initialize controller
		
		$this->init();
		
		// Log incoming request
		
		$this->adapter->log(LOG_DEBUG, "Paypal::onCancel [".print_r($this->data,true)."]");
		
		// Redirect to the checkout cancel action

		$this->app->redirect(JRoute::_("index.php?option=com_zibbra&task=checkout.cancel",false));
		
	} // end function
	
	public function confirm() {
		
		// Initialize controller
		
		$this->init();
		
		// Log incoming request
		
		$this->adapter->log(LOG_DEBUG, "Paypal::confirm [".print_r($_SESSION,true)."]");
		
		// Get payment adapter from session
		
		$payment = $this->adapter->getSessionValue("payment",false);
		$orderid = $this->adapter->getSessionValue("order.id",false);
		$order_amount = $this->adapter->getSessionValue("order.amount",false);
		
		$adapter = $payment ? ZPayment::get($payment) : false;
		
		if($adapter) {
			
			$adapter->setToken($this->adapter->getSessionValue("paypal.token",null));
			$result = $adapter->requestPayment();
			
			if($result && (!$result instanceof ZApiError)) {
				
				// Add the payment to the order
				
				ZOrder::addCreditCardPayment($orderid,$order_amount,"paypal");
				
				// Reset the cart
				
				ZCart::reset();
		
				// Redirect

				$params = JComponentHelper::getParams("com_zibbra");
				$return = $params->get("finish_uri",false);
				
				if($return) {
					
					$menu = $this->app->getMenu();
					$item = $menu->getItem($return);
					$return = $item->link;
				
				}else{
					
					$this->app->enqueueMessage(JText::_("COM_ZIBBRA_THANK_YOU_FOR_YOUR_ORDER"));
					$return = "index.php?option=com_zibbra&view=account";
					
				} // end if
				
				$this->app->redirect(JRoute::_($return,false));
				
			} // end if
			
		} // end if
		
		// Redirect to the checkout cancel action

		$this->app->enqueueMessage(JText::_("COM_ZIBBRA_PAYMENT_FAILED"));
		$this->app->redirect(JRoute::_("index.php?option=com_zibbra&view=checkout",false));
		
	} // end function
	
} // end class