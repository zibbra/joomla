<?php

// No direct access to this file

defined("_JEXEC") or die("Restricted access");
 
// Import Joomla controller library

jimport("joomla.application.component.controller");

class ZibbraControllerNewsletter extends JController {
	
	public function subscribe() {
		
		// Get the application
		
		$app = JFactory::getApplication();
		
		// subscribe
		
		if(ZEmarketing::subscribe($_POST['email'])===true) {

			// Register message
			
			$app->enqueueMessage(JText::_("COM_ZIBBRA_NEWSLETTER_SUBSCRIBE_SUCCESS"));
		
		}else{

			// Register message
			
			$app->enqueueMessage(JText::_("COM_ZIBBRA_NEWSLETTER_SUBSCRIBE_FAILED"));
		
		} // end if
	
		// Redirect to the same page
			
		$app->redirect(JRoute::_($_POST['return'],false));
		
	} // end function
	
} // end class