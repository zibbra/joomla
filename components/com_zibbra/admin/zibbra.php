<?php

// No direct access to this file

defined("_JEXEC") or die("Restricted access");

// Access check: is this user allowed to access the backend of this component?

if(!JFactory::getUser()->authorise("core.manage","com_zibbra")) {
	
	return JError::raiseWarning(404, JText::_("JERROR_ALERTNOAUTHOR"));
	
} // end if

// Set some global property

$document = JFactory::getDocument();
$document->addStyleDeclaration(".icon-48-zibbra {background-image: url(../media/com_zibbra/images/zibbra_48x48.png);}");
 
// import joomla controller library

jimport("joomla.application.component.controller");
 
// Get an instance of the controller prefixed by Zibbra

$controller = JController::getInstance("Zibbra");
 
// Perform the Request task

$controller->execute(JRequest::getCmd("task"));
 
// Redirect if set by the controller

$controller->redirect();