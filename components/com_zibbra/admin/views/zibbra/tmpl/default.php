<?php

// No direct access to this file

defined("_JEXEC") or die("Restricted access");

?>

<form action="<?php echo JRoute::_("index.php?option=com_zibbra&layout=default"); ?>" method="post" name="adminForm" id="adminForm">

	<table cellpadding="0" cellspacing="0" border="0" width="100%">
		<tr>
			<td valign="top">
			
				<fieldset class="adminform">
					<legend><?php echo JText::_("COM_ZIBBRA"); ?></legend>
					<p>&nbsp;<br />Frankrijklei 33 bus 3<br />B-2000 Antwerpen</p>
					<p><?php echo JText::_("Phone"); ?>: +32 (3) 5355555<br /><?php echo JText::_("Website"); ?>: <a href="https://www.zibbra.com/" target="_blank">www.zibbra.com</a><br /><?php echo JText::_("E-mail"); ?>: <a href="mailto:sales@zibbra.com">sales@zibbra.com</a></p>
				</fieldset>
			
				<fieldset class="adminform">
					<legend><?php echo JText::_("COM_ZIBBRA_DOCUMENTATION_HELPDESK"); ?></legend>
					<ul>
						<li><a href="https://wiki.zibbra.com/" target="_blank">Zibbra <?php echo JText::_("Introduction"); ?></a></li>
						<li><a href="https://wiki.zibbra.com/index.php/Joomla/Introduction" target="_blank">Zibbra Joomla <?php echo JText::_("Documentation"); ?></a></li>
						<li><a href="https://wiki.zibbra.com/index.php/API/Introduction" target="_blank">Zibbra API <?php echo JText::_("Documentation"); ?></a></li>
						<li><a href="https://support.zibbra.com/" target="_blank"><?php echo JText::_("Helpdesk"); ?></a></li>
					</ul>
				</fieldset>
			
			</td>
			<td valign="top">
					
				<?php foreach($this->form->getFieldsets() as $fieldset): ?>
					<fieldset class="adminform">
						<legend><?php echo JText::_($fieldset->label); ?></legend>
						<?php foreach($this->form->getFieldset($fieldset->name) as $field): ?>
							<?php if (!$field->hidden): ?>
								<?php echo $field->label; ?>
							<?php endif; ?>
							<?php echo $field->input; ?>
						<?php endforeach; ?>
					</fieldset>
				<?php endforeach; ?>
			
			</td>
		</tr>
	</table>
	
	<div>
		<input type="hidden" name="task" value="save" />
		<?php echo JHtml::_("form.token"); ?>
	</div>

</form>