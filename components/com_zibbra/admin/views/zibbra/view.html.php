<?php

// No direct access to this file

defined("_JEXEC") or die("Restricted access");
 
// import Joomla view library

jimport("joomla.application.component.view");
 
/**
 * HTML View for Zibbra Configuration Screen
 */
class ZibbraViewZibbra extends JView {
	
	public function display($tpl=null) {
		
		// Get the application and document
		
		$app = JFactory::getApplication();
		$doc = JFactory::getDocument();
 
		// Check for errors
		
		if(count($errors = $this->get("Errors"))) {
			
			JError::raiseError(500,implode("<br />",$errors));
			return false;
			
		} // end if
 
		// Set the document title
		
		$doc->setTitle(JText::_("COM_ZIBBRA"));
		
		// Assign stylesheet and javascript
		
		$doc->addStyleSheet(JURI::root(true).DS."media/com_zibbra/css/zibbra.admin.css");
		
		// Load the form
		
		$this->form = $this->get("Form");
		
		// Display the view
		
		parent::display($tpl);
		
		// Add the toolbar
		
		$this->addToolbar();
	
	} // end function
	
	protected function addToolbar() {
		
		JToolBarHelper::title(JText::_("COM_ZIBBRA_CONFIGURATION"),"zibbra");
		JToolBarHelper::save("zibbra.save","COM_ZIBBRA_TOOLBAR_SAVE");
		
		if(JFactory::getUser()->authorise("core.admin","com_zibbra")) {
			
			JToolBarHelper::divider();
			JToolBarHelper::preferences("com_zibbra");
			
		} // end if
		
	} // end function
        
} // end class