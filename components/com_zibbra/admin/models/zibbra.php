<?php

// No direct access to this file

defined("_JEXEC") or die("Restricted access");
 
// Import Joomla controller library

jimport("joomla.application.component.modeladmin");

class ZibbraModelZibbra extends JModelAdmin {

	public function getForm($data=array(),$loadData=true) {
		
		$form = $this->loadForm("com_zibbra.zibbra","configuration",array("control"=>"jform","load_data"=>$loadData));
		
		$db = &JFactory::getDBO();
		$db->setQuery("SELECT `params` FROM #__extensions WHERE `type`='component' AND `element`='com_zibbra' LIMIT 0,1");
		$result = $db->query();
		$row = $result->fetch_assoc();		
		$params = json_decode($row['params']);
	
		$form->setValue("api_client_id",null,$params->api_client_id);
		$form->setValue("api_client_secret",null,$params->api_client_secret);
		$form->setValue("api_uri",null,$params->api_uri);
		$form->setValue("default_cache",null,$params->default_cache);
		$form->setValue("finish_uri",null,$params->finish_uri);
		$form->setValue("allow_comments",null,$params->allow_comments);
		$form->setValue("default_limit",null,$params->default_limit);
		$form->setValue("default_sort_type",null,$params->default_sort_type);
		$form->setValue("default_sort_dir",null,$params->default_sort_dir);
		$form->setValue("show_stock",null,$params->show_stock);
		$form->setValue("filters_collapsed",null,$params->filters_collapsed);
		$form->setValue("manufacturers_tabs",null,$params->manufacturers_tabs);
		
		if(empty($form)) return false;
		
		return $form;
		
	} // end function

} // end class