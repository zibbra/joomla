<?php

// No direct access to this file

defined("_JEXEC") or die("Restricted access");
 
// Import Joomla list field type

jimport("joomla.form.helper");
JFormHelper::loadFieldClass("list");

class JFormFieldCategory extends JFormFieldList {
	
	protected $type = "category";
	
	protected function getOptions() {
		
		$options = array();	
			
		$cache = JFactory::getCache();
		$cache->setCaching(false);
		$cache->setLifeTime(JComponentHelper::getParams("com_zibbra")->get("default_cache",60));
		$categories = $cache->call(array("ZCategory","getCategories"),true);
		
		foreach($categories as $category) {
			
			$this->processCategory($options,$category);
			
		} // end foreach
		
		$options = array_merge(parent::getOptions(),$options);
		
		return $options;
		
	} // end function
	
	private function processCategory(&$options,$category,$level=0) {
		
		if(($level==0 && !$category->hasParent()) || $level>0) {
		
			$options[] = JHtml::_("select.option",$category->getCategoryid(),str_repeat("-&nbsp;",$level).$category->getName());
		
		} // end if
		
		if($category->hasChildren()) {
		
			foreach($category->getChildren() as $child) {
				
				$this->processCategory($options,$child,$level+1);
				
			} // end foreach
			
		} // end if
		
	} // end function
	
} // end class

?>