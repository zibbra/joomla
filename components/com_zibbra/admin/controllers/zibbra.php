<?php

// No direct access to this file

defined("_JEXEC") or die("Restricted access");

// Import Joomla controlleradmin library

jimport("joomla.application.component.controlleradmin");

class ZibbraControllerZibbra extends JControllerAdmin {
	
	public function save() {
		
		$params = JComponentHelper::getParams("com_zibbra");
		
		$params->set("api_client_id",$_POST['jform']['api_client_id']);
		$params->set("api_client_secret",$_POST['jform']['api_client_secret']);
		$params->set("api_uri",$_POST['jform']['api_uri']);
		$params->set("default_cache",$_POST['jform']['default_cache']);
		$params->set("finish_uri",$_POST['jform']['finish_uri']);
		$params->set("allow_comments",$_POST['jform']['allow_comments']);
		$params->set("filters_collapsed",$_POST['jform']['filters_collapsed']);
		$params->set("default_limit",$_POST['jform']['default_limit']);
		$params->set("default_sort_type",$_POST['jform']['default_sort_type']);
		$params->set("default_sort_dir",$_POST['jform']['default_sort_dir']);
		$params->set("show_stock",$_POST['jform']['show_stock']);
		$params->set("manufacturers_tabs",$_POST['jform']['manufacturers_tabs']);
		
		$db = &JFactory::getDBO();
		$db->setQuery("UPDATE #__extensions SET params=".$db->quote((string) $params)." WHERE `type`='component' AND `element`='com_zibbra'");
		$db->query();
		
		$app = JFactory::getApplication();
		$app->redirect(
	        "index.php?option=com_zibbra",
	        JText::_("COM_ZIBBRA_CONFIGURATION_SAVED"),
	        "message",
	        false
		);
		
	} // end function
	
} // end class

?>