#!/bin/sh
DIR="../../joomla-2.5.27";
PLACES='
administrator/components
administrator/modules
administrator/templates
cache
components
images
language
media
modules
templates
tmp
logs
';

echo -n "Enter directory of your joomla installation [$DIR]: ";
read ANSWER;
if [ ! "${ANSWER}" = "" ]; then
	if [ -d $ANSWER ]; then
		DIR=$ANSWER;
	else
		echo "Invalid directory ${ANSWER}";
		exit 1;
	fi
fi

echo -n "Resetting permissions ... ";
	find "${DIR}" -type f -exec chmod 644 {} \;
	find "${DIR}" -type d -exec chmod 755 {} \;
echo "done";

for i in $PLACES; do
	echo -n "Set write permissions for folder "${DIR}/${i}" ... ";
		chmod 777 "${DIR}/${i}";
		find "${DIR}/${i}" -type f -exec chmod 666 {} \;
		find "${DIR}/${i}" -type d -exec chmod 777 {} \;
	echo "done";
done