#!/bin/sh
cd "../";
ROOT="$PWD";

version() {

	VERSION=`cat $1 | grep "<version>" | sed -e "s/^.*<version>//" | sed -e "s/<\/version>.*$//"`;
	echo $VERSION;
	
}

cleanup () {

	echo -n "Cleanup... ";
		if [ "$1" = "before" ]; then	
			rm -f ${ROOT}/repository/packages/*;
			rm -f ${ROOT}/repository/pkg_zibbra-*.zip;		
		fi	
		rm -f ${ROOT}/repository/pkg_zibbra.xml;	
	echo "done";

}

#
# $1: Path to extension
# $2: XML file name
# $3: Zip file name
#
extension() {

	VERSION=`version "${ROOT}/${1}/${2}"`;
	echo -n "Creating zip ${3}-${VERSION} ... ";
		cd "${ROOT}/${1}";
		zip -qr ${3} *;
		mv "${3}.zip" "${ROOT}/repository/packages/${3}-${VERSION}.zip";
	echo "done";

}

package() {

	echo -n "Version of the package? ";
	read VERSION;
	cd "${ROOT}/repository";
	cp "${ROOT}/pkg_zibbra.xml" "${ROOT}/repository/";
	zip --exclude "zibbra-update.xml" -r pkg_zibbra *;
	mv pkg_zibbra.zip "${ROOT}/repository/pkg_zibbra-${VERSION}.zip"; 

}

cleanup before;
extension "components/com_zibbra" "zibbra.xml" "com_zibbra";
extension "libraries/zibbra" "zibbra.xml" "lib_zibbra";
#extension "templates/zibbra" "zibbra.xml" "tpl_zibbra";
#extension "modules/mod_zibbra_categories" "mod_zibbra_categories.xml" "mod_zibbra_categories";
#extension "modules/mod_zibbra_filters" "mod_zibbra_filters.xml" "mod_zibbra_filters";
#extension "modules/mod_zibbra_login" "mod_zibbra_login.xml" "mod_zibbra_login";
#extension "modules/mod_zibbra_minicart" "mod_zibbra_minicart.xml" "mod_zibbra_minicart";
#extension "plugins/authentication/zibbra" "zibbra.xml" "plg_authentication_zibbra";
#extension "plugins/finder/zibbra_catalog" "zibbra_catalog.xml" "plg_finder_zibbra_catalog";
#extension "plugins/system/zibbra" "zibbra.xml" "plg_system_zibbra";
package;
cleanup after;