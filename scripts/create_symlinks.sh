#!/bin/sh
DIR="$(readlink -f ../../joomla-2.5.27)";
BRANCH=`pwd | sed "s/\/storage\/$USER\/projects\/joomla.zibbra.com\///g" | sed 's/\/.*$//g'`;

if [ "$BRANCH" = `pwd` ]; then
	BRANCH="trunk";
fi

echo -n "Enter directory of your joomla installation [$DIR]: ";
read ANSWER;
if [ ! "${ANSWER}" = "" ]; then
	if [ -d $ANSWER ]; then
		DIR="$(readlink -f ${ANSWER})";
	else
		echo "Invalid directory ${ANSWER}";
		exit 1;
	fi
fi

SOURCEDIR="$(readlink -f ../../${BRANCH})";

echo -n "Enter the source directory [$SOURCEDIR]: ";
read ANSWER;
if [ ! "${ANSWER}" = "" ]; then
	if [ -d $ANSWER ]; then
		SOURCEDIR="${ANSWER}";
	else
		echo "Invalid directory ${ANSWER}";
		exit 1;
	fi
fi

SYMLINKS="
libraries/								libraries/zibbra/																		zibbra
administrator/manifests/libraries/		libraries/zibbra/zibbra.xml																zibbra.xml
components/								components/com_zibbra/site/																com_zibbra
components/com_zibbra					components/com_zibbra/zibbra.xml														zibbra.xml
administrator/components/				components/com_zibbra/admin/															com_zibbra
administrator/components/com_zibbra		components/com_zibbra/zibbra.xml														zibbra.xml
media/									components/com_zibbra/media/															com_zibbra
modules/								modules/mod_zibbra_bestsellers/															mod_zibbra_bestsellers
modules/								modules/mod_zibbra_demo/																mod_zibbra_demo
modules/								modules/mod_zibbra_login/																mod_zibbra_login
modules/								modules/mod_zibbra_manufacturers/														mod_zibbra_manufacturers
modules/								modules/mod_zibbra_menu/																mod_zibbra_menu
modules/								modules/mod_zibbra_minicart/															mod_zibbra_minicart
modules/								modules/mod_zibbra_newsletter/															mod_zibbra_newsletter
plugins/authentication/					plugins/authentication/zibbra/															zibbra
plugins/search/							plugins/search/zibbra/																	zibbra
plugins/system/							plugins/system/zibbra/																	zibbra
templates/								templates/zibbra/																		zibbra
language/en-GB/							modules/mod_zibbra_login/language/en-GB/en-GB.mod_zibbra_login.ini						en-GB.mod_zibbra_login.ini
language/en-GB/							modules/mod_zibbra_manufacturers/language/en-GB/en-GB.mod_zibbra_manufacturers.ini		en-GB.mod_zibbra_manufacturers.ini
language/en-GB/							modules/mod_zibbra_minicart/language/en-GB/en-GB.mod_zibbra_minicart.ini				en-GB.mod_zibbra_minicart.ini
language/en-GB/							modules/mod_zibbra_newsletter/language/en-GB/en-GB.mod_zibbra_newsletter.ini			en-GB.mod_zibbra_newsletter.ini
language/en-GB/							plugins/search/zibbra/en-GB.plg_search_zibbra.ini										en-GB.plg_search_zibbra.ini
administrator/language/en-GB/			modules/mod_zibbra_bestsellers/language/en-GB/en-GB.mod_zibbra_bestsellers.ini			en-GB.mod_zibbra_bestsellers.ini
administrator/language/en-GB/			modules/mod_zibbra_manufacturers/language/en-GB/en-GB.mod_zibbra_manufacturers.sys.ini	en-GB.mod_zibbra_manufacturers.sys.ini
administrator/language/en-GB/			modules/mod_zibbra_menu/language/en-GB/en-GB.mod_zibbra_menu.sys.ini					en-GB.mod_zibbra_menu.sys.ini
administrator/language/en-GB/			plugins/search/zibbra/en-GB.plg_search_zibbra.sys.ini									en-GB.plg_search_zibbra.sys.ini
";

echo "${SYMLINKS}" | while read LINE; do
	if [ ! "${LINE}" = "" ]; then
		LINE=`echo "${LINE}" | tr -s '\t' ':'`;
		SRC=`echo "${LINE}" | sed 's/:.*:.*$//g'`;
		DST=`echo "${LINE}" | sed 's/^[^:]*://g' | sed 's/:.*$//g'`;
		NAME=`echo "${LINE}" | sed 's/^.*://g`;
		if [ -L "${DIR}/${SRC}${NAME}" ]; then
			rm "${DIR}/${SRC}${NAME}";
		fi;
		if [ -d "${DIR}/${SRC}" ]; then
			echo "Link: ${DST} => ${NAME}";
			cd "${DIR}/${SRC}";
			ln -s "${SOURCEDIR}/${DST}" "${NAME}";
		fi;
	fi
done

exit 0;