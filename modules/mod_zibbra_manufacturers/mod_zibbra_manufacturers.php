<?php

// No direct access to this file

defined("_JEXEC") or die("Restricted access");

// Include helper

require_once(dirname(__FILE__).DS."helper.php");

// Instantiate helper

$manufacturers = modZibbraManufacturersHelper::getManufacturers($params);
$limit = $params->get("limit",5);
require(JModuleHelper::getLayoutPath("mod_zibbra_manufacturers"));

?>