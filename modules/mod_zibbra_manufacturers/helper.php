<?php

class modZibbraManufacturersHelper {
	
	function getManufacturers($params) {
		
		$cache = JFactory::getCache();
		$cache->setCaching(true);
		$cache->setLifeTime(JComponentHelper::getParams("com_zibbra")->get("default_cache",60));
		return $cache->call(array("ZManufacturer","getManufacturers"),$params->get("limit",5));
	
	} // end function

} // end class

?>