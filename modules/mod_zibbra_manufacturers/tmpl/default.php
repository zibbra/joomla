<?php

// No direct access

defined("_JEXEC") or die("Restricted access");

?>
<div id="zibbra-manufacturers">
	<h2>
		<span><?php echo JText::_("MOD_ZIBBRA_MANUFACTURERS"); ?></span>
		<div class="icon"></div>
	</h2>
	<ul class="menu">
		<?php foreach($manufacturers as $index=>$manufacturer): ?>
			<?php if($index < $limit): ?>
				<li>
					<a href="<?php echo JRoute::_("index.php?option=com_zibbra&view=catalog"); ?>?manufacturers[]=<?php echo $manufacturer->getManufacturerid(); ?>"><?php echo $manufacturer->getName(); ?></a>
				</li>
			<?php endif; ?>
		<?php endforeach; ?>
	</ul>
	<?php if(count($manufacturers) > $limit): ?>
		<a href="<?php echo JRoute::_("index.php?option=com_zibbra&view=manufacturers"); ?>"><span><?php echo JText::_("MOD_ZIBBRA_MANUFACTURERS_SHOW_ALL"); ?></span><div class="icon"></div></a>
	<?php endif; ?>
</div>