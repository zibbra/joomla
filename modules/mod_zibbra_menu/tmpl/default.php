<?php

// No direct access

defined("_JEXEC") or die("Restricted access");

?>
<?php function item($item,$limit,$level=0) { ?>
	<?php if(($level<$limit) && (($level==0 && !$item->hasParent()) || $level>0)): ?>
		<li class="item-<?php echo $item->getCategoryid(); ?> <?php if($item->hasChildren()): ?>deeper parent<?php endif; ?> level-<?php echo $level; ?>">
			<a href="<?php echo JRoute::_("index.php?option=com_zibbra&view=catalog&id=".$item->getIdString()."&page=1"); ?>"><?php echo $item->getName(); ?></a>
			<?php if($item->hasChildren()): ?>
				<?php if($level==1): ?>
					<div class="showmore"></div>
				<?php endif; ?>
				<ul>
					<?php foreach($item->getChildren() as $subitem): ?>
						<?php item($subitem,$limit,$level+1); ?>
					<?php endforeach; ?>
				</ul>
			<?php endif; ?>
		</li>
	<?php endif; ?>
<?php } ?>
<ul class="menu">
	<?php if($params->get("showHome",1)): ?>
		<li class="item-home">
			<a href="/"><?php echo JText::_("HOME"); ?></a>
		</li>
	<?php endif; ?>
	<?php foreach($menu as $item): ?>
		<?php item($item,$params->get("limitChildren",1)); ?>
	<?php endforeach; ?>
</ul>