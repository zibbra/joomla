<?php

// No direct access to this file

defined("_JEXEC") or die("Restricted access");

// Include helper

require_once(dirname(__FILE__).DS."helper.php");

// Instantiate helper

$menu = modZibbraMenuHelper::getMenu($params);
require(JModuleHelper::getLayoutPath("mod_zibbra_menu"));

?>