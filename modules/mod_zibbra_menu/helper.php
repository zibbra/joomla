<?php

class modZibbraMenuHelper {
	
	function getMenu($params) {		
			
		$cache = JFactory::getCache();
		$cache->setCaching(true);
		$cache->setLifeTime(JComponentHelper::getParams("com_zibbra")->get("default_cache",60));
		return $cache->call(array("ZCategory","getCategories"),(boolean) $params->get("showAllChildren"));
	
	} // end function

} // end class

?>