<?php

// No direct access

defined("_JEXEC") or die("Restricted access");

?>
<div id="zibbra-demo">
	<table cellpadding="0" cellspacing="20" border="0" width="100%">
		<tr>
			<td><span class="zibbra">Zibbra</span></td>
			<td>&nbsp;</td>
			<td><img src="/templates/zibbra/images/demo.png" border="0" /></td>
			<td>&nbsp;</td>
			<td>
				<h2>Important Demo Notice</h2>
				<p>We do <strong>NOT</strong> sell through this website. It is strictly intended for Zibbra demonstration purposes.</p>
			</td>
		</tr>
	</table>
</div>