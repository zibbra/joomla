(function() {
	
	Newsletter = function() {
		
		this.field = null;
		this.value = null;
		
	}; // end function
	
	Newsletter.prototype.init = function() {
		
		var self = this;
		
		this.field = $("zibbra-newsletter-email");
		this.value = this.field.get("value");
		
		this.field.addEvents({
			"focus": function() {
				
				if(this.get("value")==self.value) this.set("value","");
				
			},
			"blur": function() {
				
				if(this.get("value")=="") this.set("value",self.value);
				
			}
		});
		
	}; // end function
	
})(); // end class

var newsletter = new Newsletter();

window.addEvent("domready",function() {
	
	newsletter.init();
	
}); // end domready