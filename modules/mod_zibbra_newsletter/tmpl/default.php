<?php

// No direct access

defined("_JEXEC") or die("Restricted access");

?>
<div id="zibbra-newsletter">
	<h2><?php echo JText::_("MOD_ZIBBRA_NEWSLETTER_LIKE_WHAT_YOU_SEE"); ?></h2>
	<p><?php echo JText::_("MOD_ZIBBRA_NEWSLETTER_SUBSCRIBE_TO_OUR_NEWSLETTER"); ?></p>
	<form action="<?php echo JRoute::_("index.php",true,$params->get("usesecure")); ?>" method="post" name="com-zibbra" id="zibbra-newsletter" name="redir">
		<?php echo JHtml::_("form.token"); ?>	
		<input type="hidden" name="option" value="com_zibbra" />
		<input type="hidden" name="task" value="newsletter.subscribe" />
		<input type="hidden" name="return" value="<?php echo $_SERVER['PHP_SELF']; ?>" />
		<table cellpadding="0" cellspacing="0" border="0" width="100%">
			<tr>
				<td><input type="text" id="zibbra-newsletter-email" name="email" value="<?php echo JText::_("MOD_ZIBBRA_NEWSLETTER_YOUR_EMAIL"); ?>" /></td>
				<td><input type="submit" name="submit" id="zibbra-newsletter-subscribe" value="<?php echo JText::_("MOD_ZIBBRA_NEWSLETTER_SUBSCRIBE"); ?>" /></td>
			</tr>
		</table>
	</form>
</div>