<?php

// No direct access to this file

defined("_JEXEC") or die("Restricted access");

// Include helper

require_once(dirname(__FILE__).DS."helper.php");

// Assign JavaScript

$doc = JFactory::getDocument();
$doc->addScript(JURI::root()."modules/mod_zibbra_newsletter/jscripts/newsletter.js");

// Instantiate helper

require(JModuleHelper::getLayoutPath("mod_zibbra_newsletter"));

?>