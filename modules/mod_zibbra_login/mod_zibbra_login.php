<?php

// No direct access to this file

defined("_JEXEC") or die("Restricted access");

// Include helper

require_once(dirname(__FILE__).DS."helper.php");

// Assign JavaScript & CSS

$doc = JFactory::getDocument();
$doc->addScript(JURI::root()."modules/mod_zibbra_login/jscripts/mod_zibbra_login.js");
$doc->addStylesheet(JURI::root()."modules/mod_zibbra_login/css/mod_zibbra_login.css");

// Instantiate helper

$return_login = modZibbraLoginHelper::getReturnURL(true);
$return_logout = modZibbraLoginHelper::getReturnURL(false);
$user = JFactory::getUser();
require(JModuleHelper::getLayoutPath("mod_zibbra_login"));

?>