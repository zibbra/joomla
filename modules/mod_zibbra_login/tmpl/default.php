<?php

// No direct access

defined("_JEXEC") or die("Restricted access");

?>
<form action="<?php echo JRoute::_("index.php",true,$params->get("usesecure")); ?>" method="post" name="com-login" id="zibbra-login" name="redir">
	<?php echo JHtml::_("form.token"); ?>

	<?php if($user->get("guest")): ?>
	
	    <input type="hidden" name="remember" value="yes" />
	    <input type="hidden" name="option" value="com_users" />
	    <input type="hidden" name="task" value="user.login" />
		<div class="username-container">
		    <label id="username-label" for="username"><?php echo JText::_("MOD_ZIBBRA_LOGIN_USERNAME"); ?></label>
		    <input type="text" id="username" name="username" value="" />
	    </div>
	    <div class="password-container">
		    <label id="password-label" for="password"><?php echo JText::_("MOD_ZIBBRA_LOGIN_PASSWORD"); ?></label>
	    	<input type="password" id="password" name="password" value="" />
	    </div>
		<input type="hidden" name="return" value="<?php echo $return_login; ?>" />
	    <input type="submit" name="submit" value="" />
		
	<?php else: ?>
	
		<input type="hidden" name="option" value="com_users" />
		<input type="hidden" name="task" value="user.logout" />
		<input type="hidden" name="return" value="<?php echo $return_logout; ?>" />
		<a class="button" href="<?php echo JRoute::_("index.php?option=com_zibbra&view=account"); ?>"><?php echo JText::_("MOD_ZIBBRA_LOGIN_YOUR_ACCOUNT"); ?></a>
		<a id="logout" class="button" href="javascript:void(0);"><?php echo JText::_("JLOGOUT"); ?></a>
		
	<?php endif; ?>
	
</form>