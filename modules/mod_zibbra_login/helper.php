<?php

class modZibbraLoginHelper {
	
	public static function getReturnURL($login=true) {
		
		return base64_encode(JRoute::_($login ? "index.php?option=com_zibbra&view=account" : "/"));
		
	} // end function

} // end class

?>