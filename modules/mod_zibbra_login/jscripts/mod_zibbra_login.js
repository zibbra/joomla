(function() {
	
	Login = function() {

		this.login = {};
		this.logout = {};
		this.password = {};
		
	}; // end function
	
	Login.prototype.init = function() {
		
		var self = this;

		this.login.field = $("username");
		this.login.label = $("username-label");
		this.password.field = $("password");
		this.password.label = $("password-label");
		this.logout.field = $("logout");
		
		if(this.login.field!==null) {
			
			if(this.login.label!==null) {
			
				this.login.label.addEvents({
					"click": function() {
						
						this.hide();
						self.password.label.hide();
						
					}
				});
			
			} // end if
			
			this.login.field.addEvents({
				"blur": function() {
					
					if(this.get("value")=="") self.login.label.show();
					
				}
			});
			
			if(this.password.label!==null) {
			
				this.password.label.addEvents({
					"click": function() {
						
						this.hide();
						
					}
				});
			
			} // end if
			
			this.password.field.addEvents({
				"blur": function() {
					
					if(this.get("value")=="") self.password.label.show();
					
				}
			});
			
		} // end if
		
		if(this.logout.field!==null) {
		
			$("logout").addEvents({
				"click": function() {
					
					this.getParent("form").submit();
					
				}
			});
			
		} // end if
		
	}; // end function
	
})(); // end class

var login = new Login();

window.addEvent("domready",function() {
	
	login.init();
	
}); // end domready