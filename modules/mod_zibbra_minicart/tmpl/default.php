<?php

// No direct access

defined("_JEXEC") or die("Restricted access");

?>
<table cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td><div class="icon"></div></td>
		<td><div class="price"><?php echo $cart->getValutaSymbol(); ?>&nbsp;<?php echo number_format($cart->getTotal(),2,",",""); ?></div></td>
		<td><button onclick="location.href='<?php echo JRoute::_("index.php?option=com_zibbra&view=checkout"); ?>';"><?php echo JText::_("MOD_ZIBBRA_MINICART_CHECKOUT"); ?></button></td>
	</tr>
</table>	
<?php if(!$cart->isEmpty()): ?>
	<div id="minicart-details" style="visibility:hidden;">
		<div class="arrow"></div>
		<ul>					
			<?php foreach($cart->getItems() as $item): ?>		
				<li>				
					<?php echo $item->getQuantity(); ?>&nbsp;x&nbsp;<a href="<?php echo JRoute::_("index.php?option=com_zibbra&view=product&id=".$item->getProductid()); ?>"><?php echo $item->getDescription(); ?></a></td>	
				</li>			
			<?php endforeach; ?>
		</ul>
		<div class="button">
			<a href="<?php echo JRoute::_("index.php?option=com_zibbra&view=cart"); ?>"><?php echo JText::_("MOD_ZIBBRA_MINICART_VIEW_CART"); ?></a>
		</div>			
	</div>
<?php endif; ?>