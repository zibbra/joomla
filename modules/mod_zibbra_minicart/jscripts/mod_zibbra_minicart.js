(function() {
	
	Minicart = function() {
		
		this.container = null;
		this.content = null;
		this.fx = null;
		
	}; // end function
	
	Minicart.prototype.init = function() {
		
		var self = this;
		
		this.container = $("minicart");
		this.content = $("minicart-details");
		
		if(this.content!==null) {
			
			this.content.fade("hide");
			
			this.container.addEvents({
				"mouseenter": function() {				
					self.content.fade("toggle");
					self.container.addClass("active");
				},
				"mouseleave": function() {			
					self.content.fade("toggle");	
					self.container.removeClass("active");	
				}
			});		
		
		} // end if
		
	}; // end function
	
})(); // end class

var minicart = new Minicart();

window.addEvent("load",function() {
	
	minicart.init();
	
}); // end domready