<?php

// No direct access to this file

defined("_JEXEC") or die("Restricted access");

// Include helper

require_once(dirname(__FILE__).DS."helper.php");

// Assign JavaScript and CSS

$doc = JFactory::getDocument();
$doc->addScript(JURI::root()."modules/mod_zibbra_bestsellers/jscripts/bestsellers.js");
$doc->addStylesheet(JURI::root()."modules/mod_zibbra_bestsellers/css/bestsellers.css");

// Instantiate helper

$bestsellers = modZibbraBestsellersHelper::getBestsellers($params);
require(JModuleHelper::getLayoutPath("mod_zibbra_bestsellers"));

?>