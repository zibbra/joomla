<?php

// No direct access

defined("_JEXEC") or die("Restricted access");

?>
<div id="zibbra-bestsellers">
	<h2>
		<span>Bestsellers</span>
		<div class="icon"></div>
	</h2>
	<ul>
		<?php foreach($bestsellers as $product): ?>
			<?php 
				$product_url = JRoute::_("index.php?option=com_zibbra&view=product&id=".$product->getSlug());
			?>
			<li>
				<?php if($product->hasImages()): ?>
					<p class="image" align="center">
						<a href="<?php echo $product_url; ?>"><img src="<?php echo $product->getFirstImage()->getPath(); ?>" border="0" /></a>
					</p>
				<?php endif; ?>
				<div class="info">
					<h3><?php echo strip_tags($product->getName()); ?></h3>
					<p class="description"><?php echo substr(strip_tags($product->getShortDescription()),0,50); ?></p>
					<p class="price"><?php echo $product->getValutaSymbol(); ?>&nbsp;<?php echo number_format($product->getPrice(),2,",",""); ?></p>
					<a href="<?php echo $product_url; ?>"><div class="icon"></div></a>
				</div>
			</li>
		<?php endforeach; ?>
	</ul>
</div>