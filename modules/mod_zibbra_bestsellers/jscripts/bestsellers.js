(function() {
	
	Bestsellers = function() {
		
		this.container = null;
		this.header = null;
		this.content = null;
		
	}; // end function
	
	Bestsellers.prototype.init = function() {
		
		var self = this;
		
		this.container = $("zibbra-bestsellers");
		this.header = this.container.getElement("h2");
		this.content = this.container.getElement("ul");
		
		var size = this.content.getComputedSize();
		
		this.content.setStyle("height",size.height);
		
		this.header.addEvents({
			"click": function() {
				
				new Fx.Slide(self.content,{duration:1000,transition:Fx.Transitions.Pow.easeOut}).toggle();
				
				self.header.toggleClass("collapsed");
				
			}
		});
		
	}; // end function
	
})(); // end class

var bestsellers = new Bestsellers();

window.addEvent("load",function() {
	
	bestsellers.init();
	
}); // end domready