<?php

// No direct access to this file

defined("_JEXEC") or die("Restricted access");

// Import Joomla libraries

jimport("joomla.filesystem.file");

// Check modules

$showRightColumn = ($this->countModules("position-3") or $this->countModules("position-6") or $this->countModules("position-8"));
$showLeftColumn = ($this->countModules("position-4") or $this->countModules("position-7") or $this->countModules("position-5"));
$showBottom = ($this->countModules("position-9") or $this->countModules("position-10") or $this->countModules("position-11"));

// Include Mootools

JHtml::_("behavior.framework",true);

// Get template params

$color = $this->params->get("templatecolor");
$phone = $this->params->get("phone");
$logo = $this->params->get("logo");
$slogan = $this->params->get("siteslogan");
$app = JFactory::getApplication();
$doc = JFactory::getDocument();
$templateparams = $app->getTemplate(true)->params;
$user = JFactory::getUser();

// Register stylesheets

$doc->addStyleSheet($this->baseurl."/templates/system/css/system.css");
$doc->addStyleSheet($this->baseurl."/templates/".$this->template."/css/template.css",$type = "text/css",$media = "screen,projection");
$doc->addStyleSheet($this->baseurl."/templates/".$this->template."/css/print.css",$type = "text/css",$media = "print");
$doc->addStyleSheet($this->baseurl."/templates/".$this->template."/css/".htmlspecialchars($color).".css");

// Register Javascripts

if($this->countModules("position-16")) {
	
	$doc->addScript($this->baseurl."/templates/".$this->template."/jscripts/banners.js");

} // end if

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
<head>
<jdoc:include type="head" />
</head>
<body>

	<div id="all">
		
		<div id="header-wrapper">
			<div id="header">
				<div id="top-bar">
					<div id="languages">
						<jdoc:include type="modules" name="position-15" />
					</div>
					<div id="login">
						<jdoc:include type="modules" name="position-13" />
					</div>
					<?php if($user->get("guest")): ?>
						<div id="register">Don't have an account yet? register <a href="<?php echo JRoute::_("index.php?option=com_zibbra&view=register"); ?>">here</a></div>
					<?php endif; ?>
					<?php if($phone): ?>
						<div id="phone"><img src="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/images/phone.png" border="0" /><span><?php echo $phone; ?></span></div>
					<?php endif; ?>
				</div>
				<div class="logoheader">
					<h1 id="logo">	
						<?php if($logo): ?>
							<a href="/"><img src="<?php echo $this->baseurl; ?>/<?php echo htmlspecialchars($logo); ?>"  alt="<?php echo htmlspecialchars($templateparams->get("sitetitle"));?>" /></a>
						<?php else: ?>
							<span class="site-title"><?php echo htmlspecialchars($templateparams->get("sitetitle"));?></span>
							<span class="site-description"><?php echo htmlspecialchars($templateparams->get("sitedescription"));?></span>
						<?php endif; ?>
					</h1>
					<?php if($slogan): ?>
						<div id="slogan"><?php echo $slogan; ?></div>
					<?php endif; ?>			
					<?php if($this->countModules("position-18")): ?>
						<div id="minicart">
							<jdoc:include type="modules" name="position-18" />	
						</div>
					<?php endif; ?>	
				</div>
				<div id="menu">
					<jdoc:include type="modules" name="position-1" />
				</div>
				<div id="search">
					<jdoc:include type="modules" name="position-0" />
				</div>
			</div>
		</div>
	
		<div id="back">
			
			<div id="contentarea">
			
				<?php if($this->countModules("position-2")): ?>
					<div id="breadcrumbs">
						<jdoc:include type="modules" name="position-2" />	
					</div>
				<?php endif; ?>	
			
				<?php if($this->countModules("position-16")): ?>
					<div id="banners">
						<jdoc:include type="modules" name="position-16" />
					</div>
				<?php endif; ?>	
				
				<table cellpadding="0" cellspacing="0" border="0" width="100%">
					<tbody>
						<tr>	
							<?php if($showLeftColumn): ?>
								<td valign="top" width="220">			
									<div id="left">	
										<jdoc:include type="modules" name="position-7" />
										<jdoc:include type="modules" name="position-4" />
										<jdoc:include type="modules" name="position-5" />
									</div>
								</td>
							<?php endif; ?>
							<td valign="top">
								<div id="main">	
									<?php if($this->countModules("position-12")): ?>
										<div id="top">
											<jdoc:include type="modules" name="position-12" />
										</div>
									<?php endif; ?>		
									<jdoc:include type="message" />
									<jdoc:include type="component" />
									<?php if($this->countModules("position-17")): ?>
										<div id="bottom">
											<jdoc:include type="modules" name="position-17" />
										</div>
									<?php endif; ?>	
								</div>
							</td>		
							<?php if($showRightColumn): ?>
								<td valign="top" width="220">			
									<div id="right">
										<jdoc:include type="modules" name="position-6" />
										<jdoc:include type="modules" name="position-8" />
										<jdoc:include type="modules" name="position-3" />
									</div>
								</td>
							<?php endif; ?>
						</tr>
					</tbody>				
				</table>
	
			</div>
			
		</div>
		
	</div>
	
	<div id="footer-outer">
	
		<?php if($showBottom): ?>
			<div id="footer-inner">
				<div id="bottom">
					<div class="box box1"><jdoc:include type="modules" name="position-9" /></div>
					<div class="box box3"><jdoc:include type="modules" name="position-11" /></div>
					<div class="box box2"><jdoc:include type="modules" name="position-10" /></div>
				</div>
			</div>
		<?php endif ; ?>
	
		<div id="footer-sub">
			<div id="footer">
				<jdoc:include type="modules" name="position-14" />
				<p id="poweredby"><?php echo JText::_("TPL_ZIBBRA_POWERED_BY");?> <a href="https://www.zibbra.com">Zibbra Technologies</a></p>
			</div>	
		</div>
		
	</div>
	
	<jdoc:include type="modules" name="debug" />
	
</body>
</html>