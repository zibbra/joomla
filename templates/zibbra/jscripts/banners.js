(function() {
	
	Banners = function() {

		this.timer = null;
		this.container = null;
		this.banners = [];
		
	}; // end function
	
	Banners.prototype.init = function() {

		this.container = $("banners");
		this.banners = $$("#banners > div.bannergroup > div.banneritem");
		
		if(this.banners.length>0) {
			
			this.controls();
			
			for(var i=0; i<this.banners.length; i++) {
				
				this.banners[i].set("id","banner-"+i);
				
			} // end for

			$$("#banners > div.bannergroup > div.banneritem:nth-child(last)").addClass("active").getElement("div.info").setStyle("opacity","1.0");			
			$$("#banners > div.bannercontrols > div.buttons > div.button:nth-child(first)").addClass("active");

			var contentarea = $("contentarea").getComputedSize();
			var first = this.banners[0];
			var size = first.getComputedSize();
			var left = Math.ceil((contentarea.width - size.width) / 2);
			
			this.container.setStyle("width",size.width);
			this.container.setStyle("height",size.height);
			this.container.setStyle("margin-left",left);
			
			$$("#banners > div.bannergroup > div.banneritem").show();
			
			this.resetTimer();
			
		} // end if
		
	}; // end function
	
	Banners.prototype.resetTimer = function() {

		var self = this;
		
		clearTimeout(this.timer);
		
		this.timer = setInterval(function() {
			
			self.next();
			
		},5000);
		
	}; // end function
	
	Banners.prototype.controls = function() {
		
		var self = this;
		var container = $("banners");
		var controls = new Element("div",{"class":"bannercontrols"});
		var previous = new Element("div",{"class":"previous",html:"<"});
		var next = new Element("div",{"class":"next",html:">"});
		var buttons = new Element("div",{"class":"buttons"});
		
		for(var i=0; i<this.banners.length; i++) {
			
			var banner = this.banners[i];
			var button = new Element("div",{"id":"button-"+i,"class":"button","title":banner.getElement("img").get("alt")});
			
			button.inject(buttons);
			
			button.addEvent("click",function(event) {				
			    
			    event.stop();
			    self.go(parseInt(this.get("id").replace("button-","")));		    	
		    	self.resetTimer();
			    
			}); // end click
			
		} // end for
		
		previous.addEvent("click",function(event) {
			
		    event.stop();		    
		    self.previous();
	    	self.resetTimer();
		    
		}); // end click
		
		next.addEvent("click",function(event) {
			
		    event.stop();		    
		    self.next();
	    	self.resetTimer();
		    
		}); // end click

		previous.inject(controls);
		next.inject(controls);
		buttons.inject(controls);
		controls.inject(container);
		
	}; // end function
	
	Banners.prototype.go = function(index) {
		
		var active = $$("#banners > div.bannergroup > div.banneritem.active");
		var next = $("banner-"+index);
		
		this.transition(active[0],next);
		
		$$("#banners > div.bannercontrols > div.buttons > div.button").removeClass("active"); 
	    
		$("banner-"+index).addClass("active");
		$("button-"+index).addClass("active");
		
	}; // end function
	
	Banners.prototype.previous = function() {
		
		var active = $$("#banners > div.bannergroup > div.banneritem.active");		
		var previous =  active.getPrevious();
		
		if(previous[0]===null) previous = $$("#banners > div.bannergroup > div.banneritem:last-child");
	    
		active[0].addClass("last-active");
		
		var title = previous[0].getElement("img").get("alt");
		
		$$("#banners > div.bannercontrols > div.buttons > div.button").removeClass("active");    	
    	$$("#banners > div.bannercontrols > div.buttons > div.button[title='"+title+"']").addClass("active");
    	
    	this.transition(active[0],previous[0]);
		
	}; // end function
	
	Banners.prototype.next = function() {
		
		var active = $$("#banners > div.bannergroup > div.banneritem.active");		
		var next =  active.getNext();
	    
		if(next[0]===null) next = $$("#banners > div.bannergroup > div.banneritem:first-child");
	    
		active[0].addClass("last-active");
    	
    	var title = next[0].getElement("img").get("alt");
		
		$$("#banners > div.bannercontrols > div.buttons > div.button").removeClass("active");    	
    	$$("#banners > div.bannercontrols > div.buttons > div.button[title='"+title+"']").addClass("active");
    	
    	this.transition(active[0],next[0]);
		
	}; // end function
	
	Banners.prototype.transition = function(from,to) {

    	from.addClass("last-active");
		to.setStyle("opacity","0.0");
		to.getElement("div.info").setStyle("opacity","0.0");
		to.addClass("active");
	    
	    var fx1 = new Fx.Tween(to,{duration:"long",onComplete: function() {
			
	    	from.removeClass("active");
	    	from.removeClass("last-active");	    	
	    	
	    }}); // end tween
	    
	    var fx2 = new Fx.Tween(from.getElement("div.info"),{duration:"long"});
	    var fx3 = new Fx.Tween(to.getElement("div.info"),{duration:"long"});
	    
	    fx1.start("opacity","1.0");
	    fx2.start("opacity","0.0");
	    fx3.start("opacity","1.0");
		
	}; // end function
	
})(); // end class

var banners = new Banners();

window.addEvent("load",function(){
	
	banners.init();
	
}); // end ready