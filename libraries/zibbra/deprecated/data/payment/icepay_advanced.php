<?php

//include_once(ZIBBRA_INCLUDES_DIR."/icepay/icepay_api_advanced.php");

class ZPaymentIcepayAdvanced extends ZPayment {
	
	protected $merchantid;
	protected $secret_code;
	protected $checkout_title;
	protected $description;
	protected $postback_url;
	
	public function getCheckoutTitle() {
		
		$languagecode = ZController::getLanguage();

		if($this->checkout_title!=null) {
		
			$arr = unserialize($this->checkout_title);

			if(isset($arr[$languagecode])) {
			
				return $arr[$languagecode];
		
			} // end if
		
		} // end if
		
		return null;
		
	} // end function
	
	public function getDescription() {
		
		$languagecode = ZController::getLanguage();

		if($this->description!=null) {
		
			$arr = unserialize($this->description);

			if(isset($arr[$languagecode])) {
			
				return $arr[$languagecode];
		
			} // end if
		
		} // end if
		
		return null;
		
	} // end function
	
	public function dispatch($amount,$orderid) {
		
		// Set the amount and orderid
		
		$this->setAmount($amount);
		$this->setOrderid($orderid);
		
		// Clear session info
		
		$session = JFactory::getSession();		
		$session->clear("payment","zibbra");
		
		// Apply logging rules
		
		$app = JFactory::getApplication();
		$logdir = $app->getCfg("log_path");
		$logfile = "/icepay-basic-".date("Ymd").".log";
		
		$logger = Icepay_Api_Logger::getInstance();
		$logger->enableLogging()
			->setLoggingLevel(Icepay_Api_Logger::LEVEL_ALL)
			->logToFile()
			->setLoggingDirectory($logdir)
			->setLoggingFile($logfile)
			/*->logToScreen()*/;
		
		// Set the paymentObject
		
		$paymentObj = new Icepay_PaymentObject();
		$paymentObj->setAmount((int) ($this->amount * 100))
			/*->setCountry("BE")*/
			->setLanguage("NL")
			->setReference("Digigoods NV")
			->setDescription($this->getOrder()->getNumber())
			->setCurrency("EUR")
			->setOrderID($this->orderid);
		
		try {
			
			// Merchant Settings
			
			$basicmode = Icepay_Basicmode::getInstance();
			$basicmode->setMerchantID($this->merchantid)
				->setSecretCode($this->secret_code)
				->validatePayment($paymentObj);
		
			header("Location: ".$basicmode->getURL());
			exit;
		
		}catch(Exception $e){
			
			echo($e->getMessage());
			
		} // end try-catch
		
		exit;
		
	} // end function
	
} // end class