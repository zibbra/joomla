<?php

class ZPaymentTranscription extends ZPayment {
	
	public function dispatch($amount,$orderid) {
		
		// Set the amount and orderid
		
		$this->setAmount($amount);
		$this->setOrderid($orderid);
		
		// Send confirmation
		
		//ZOrder::sendConfirmation($orderid,"transcription");
		
		// Reset the cart
		
		ZCart::reset();
		
		// Clear session info
		
		$session = JFactory::getSession();
		$session->clear("payment","zibbra");
		$session->clear("shipping","zibbra");
		
		return;
		
	} // end function
	
} // end class