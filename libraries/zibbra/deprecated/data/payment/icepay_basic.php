<?php

include_once(ZIBBRA_INCLUDES_DIR."/icepay/icepay_api_basic.php");

class ZPaymentIcepayBasic extends ZPayment {
	
	protected $merchantid;
	protected $secret_code;
	protected $checkout_title;
	protected $description;
	protected $postback_url;
	
	public function getCheckoutTitle() {
		
		$languagecode = ZController::getLanguage();

		if($this->checkout_title!=null) {
		
			$arr = unserialize($this->checkout_title);

			if(isset($arr[$languagecode])) {
			
				return $arr[$languagecode];
		
			} // end if
		
		} // end if
		
		return null;
		
	} // end function
	
	public function getDescription() {
		
		$languagecode = ZController::getLanguage();

		if($this->description!=null) {
		
			$arr = unserialize($this->description);

			if(isset($arr[$languagecode])) {
			
				return $arr[$languagecode];
		
			} // end if
		
		} // end if
		
		return null;
		
	} // end function
	
	public function dispatch($amount,$orderid) {
		
		// Set the amount and orderid
		
		$this->setAmount($amount);
		$this->setOrderid($orderid);
		
		// Clear session info
		
		$session = JFactory::getSession();		
		$session->clear("payment","zibbra");
		
		// Apply logging rules
		
		$app = JFactory::getApplication();
		$logdir = $app->getCfg("log_path");
		$logfile = "/icepay-basic-".date("YmdHis").".log";
		
		$logger = Icepay_Api_Logger::getInstance();
		$logger->enableLogging()
			->setLoggingLevel(Icepay_Api_Logger::LEVEL_ALL)
			->logToFile()
			->setLoggingDirectory($logdir)
			->setLoggingFile($logfile)
			/*->logToScreen()*/;
		
		// Set the paymentObject
		
		$amount = (int) ($this->amount * 100);
		$reference = $_SERVER['HTTP_HOST'];
		$ordernr = $this->getOrder()->getNumber();
		
		$customer = ZCustomer::load();
		$languagecode = strtoupper(substr($customer->getLanguagecode(),0,2));
		$countrycode = strtoupper($customer->getBillingAddress()->getCountrycode());

		syslog(LOG_WARNING,"ICEPAY test: ".$languagecode." | ".$countrycode);
		
		$logger->log("Amount=".$amount.";Country=".$countrycode.";Language=".$languagecode.";Reference=".$reference.";Description=".$ordernr.";Currency=EUR;OrderID=".$this->orderid);
		
		$paymentObj = new Icepay_PaymentObject();
		$paymentObj->setAmount($amount)
			->setCountry($countrycode)
			->setLanguage($languagecode)
			->setReference($reference)
			->setDescription($ordernr)
			->setCurrency("EUR")
			->setOrderID($this->orderid);
		
		try {
			
			// Merchant Settings
			
			$basicmode = Icepay_Basicmode::getInstance();
			$basicmode->setMerchantID($this->merchantid)
				->setSecretCode($this->secret_code)
				->validatePayment($paymentObj);
		
			header("Location: ".$basicmode->getURL());
			exit;
		
		}catch(Exception $e){
			
			echo($e->getMessage());
			
		} // end try-catch
		
		exit;
		
	} // end function
	
} // end class