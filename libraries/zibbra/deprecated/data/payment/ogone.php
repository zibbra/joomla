<?php

class ZPaymentOgone extends ZPayment {
	
	const OGONE_URI = "https://secure.ogone.com/ncol/prod/orderstandard.asp";
	const OGONE_URI_TEST = "https://secure.ogone.com/ncol/test/orderstandard.asp";
	const OGONE_ACCEPTURL = "index.php?option=com_zibbra&task=ogone.onAccept";
	const OGONE_DECLINEURL = "index.php?option=com_zibbra&task=ogone.onDecline";
	const OGONE_EXCEPTIONURL = "index.php?option=com_zibbra&task=ogone.onException";
	const OGONE_CANCELURL = "index.php?option=com_zibbra&task=ogone.onCancel";
	const OGONE_BACKURL = "index.php?option=com_zibbra&task=ogone.onCancel";
	const OGONE_USERID = "zibbraApi";
	const OGONE_OPERATION = "SAL";
	const OGONE_LOGO = "https://api.zibbra.com/images/logo_20.png";
	const OGONE_TITLE = "Zibbra Checkout";
	
	protected $pspid;
	protected $sha_phrase;
	
	private function getApiUri() {
		
		return ($this->inSandboxMode() ? self::OGONE_URI_TEST : self::OGONE_URI);
		
	} // end function
	
	public function dispatch($amount,$orderid) {

		$this->setAmount($amount);
		$this->setOrderid($orderid);
		
		// Get the params and sha string
		
		$params = $this->getParams();
		$sha = $this->paramsToSha($params);
		
		// Draw the form and submit with javascript
		
		echo "<form id=\"zibbra-checkout-confirm-ogone\" action=\"".self::OGONE_URI."\" method=\"post\">";
		
		foreach($params as $key=>$value) {
			
			if($value!=="") {
			
				echo "<input type=\"hidden\" name=\"".$key."\" value=\"".$value."\" />";
			
			} // end if
			
		} // end foreach
		
		echo "<input type=\"hidden\" name=\"SHASIGN\" value=\"".$sha."\" />";		
		echo "</form>";
		echo "<script> document.getElementById('zibbra-checkout-confirm-ogone').submit(); </script>";
		exit;
		
	} // end function
	
	private function paramsToSha($params) {
		
		$sha = "";
		
		ksort($params);
		
		foreach($params as $key=>$value) {
		
			if($value!=="") {
		
				$sha .= $key."=".$value.$this->sha_phrase;
		
			} // end if
		
		} // end foreach
		
		return hash("sha1",$sha);
		
	} // end function
	
	private function getParams() {
		
		$uri = JURI::getInstance();
		$prefix = $uri->getScheme()."://".$uri->getHost()."/";
		$user = JFactory::getUser();
		
		return array(
			"PSPID"=>$this->pspid,
			"ORDERID"=>$this->getOrder()->getNumber(),
			"AMOUNT"=>$this->getAmount()*100,
			"CURRENCY"=>"EUR",
			"LANGUAGE"=>"nl_NL",
			"TITLE"=>self::OGONE_TITLE,
			"BGCOLOR"=>"",
			"TXTCOLOR"=>"",
			"TBLBGCOLOR"=>"",
			"TBLTXTCOLOR"=>"",
			"BUTTONBGCOLOR"=>"",
			"BUTTONTXTCOLOR"=>"",
			"FONTTYPE"=>"",
			"LOGO"=>self::OGONE_LOGO,
			"TP"=>"",
			"ACCEPTURL"=>$prefix.self::OGONE_ACCEPTURL,
			"DECLINEURL"=>$prefix.self::OGONE_DECLINEURL,
			"EXCEPTIONURL"=>$prefix.self::OGONE_EXCEPTIONURL,
			"CANCELURL"=>$prefix.self::OGONE_CANCELURL,
			"BACKURL"=>$prefix.self::OGONE_BACKURL,
			"HOMEURL"=>$prefix,
			"CATALOGURL"=>$prefix,
			"CN"=>$user->name,
			"EMAIL"=>$user->email,
			"PM"=>"",
			"BRAND"=>"",
			"OWNERZIP"=>"",
			"OWNERADDRESS"=>"",
			"OWNERADDRESS2"=>"",
			"ALIAS"=>"",
			"ALIASUSAGE"=>"",
			"ALIASOPERATION"=>"",
			"COM"=>"",
			"COMPLUS"=>"",
			"PARAMPLUS"=>"",
			"USERID"=>self::OGONE_USERID,
			"CREDITCODE"=>"",
			"OPERATION"=>self::OGONE_OPERATION,
			"SUBSCRIPTION_ID"=>"",
			"SUB_AMOUNT"=>"",
			"SUB_COM"=>"",
			"SUB_ORDERID"=>"",
			"SUB_PERIOD_UNIT"=>"",
			"SUB_PERIOD_NUMBER"=>"",
			"SUB_PERIOD_MOMENT"=>"",
			"SUB_STARTDATE"=>"",
			"SUB_ENDDATE"=>"",
			"SUB_STATUS"=>"",
			"SUB_COMMENT"=>""
		);
		
	} // end function
	
} // end class