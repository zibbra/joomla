<?php

class ZPaymentPaypal extends ZPayment {
	
	const PAYPAL_VERSION = "89.0";
	const PAYPAL_PAYMENTACTION = "Sale";
	const PAYPAL_RETURNURL = "?option=com_zibbra&task=paypal.onReturn";
	const PAYPAL_CANCELURL = "?option=com_zibbra&task=paypal.onCancel";
	const PAYPAL_NOTIFYURL = "https://api.zibbra.com/api/paymentadapters/paypal";
	const PAYPAL_NOSHIPPING = 1;
	
	protected $user;
	protected $pwd;
	protected $signature;
	
	private $token;
	
	private function getApiUri() {
		
		return "https://api-3t.".($this->inSandboxMode() ? "sandbox." : "")."paypal.com/nvp";
		
	} // end function
	
	private function getCheckoutUri() {
		
		return "https://www.".($this->inSandboxMode() ? "sandbox." : "")."paypal.com/cgi-bin/webscr";
		
	} // end function
	
	public function dispatch($amount,$orderid) {
		
		// Set the amount and orderid
		
		$this->setAmount($amount);
		$this->setOrderid($orderid);
		
		// Request an authentication token (will redirect to paypal login page)
		
		$this->requestToken();
		
		// Exit application
		
		exit;
		
	} // end function
	
	public function requestToken() {
			
		$request = array(
			"METHOD"=>"SetExpressCheckout",
			"USER"=>$this->user,
			"PWD"=>$this->pwd,
			"SIGNATURE"=>$this->signature,
			"VERSION"=>self::PAYPAL_VERSION,
			"ALLOWNOTE"=>0,
			"PAYMENTREQUEST_0_PAYMENTACTION"=>self::PAYPAL_PAYMENTACTION,
			"PAYMENTREQUEST_0_AMT"=>round($this->getAmount(),2),
			"PAYMENTREQUEST_0_CURRENCYCODE"=>"EUR",
			"PAYMENTREQUEST_0_CUSTOM"=>$this->getOrderid(),
			"PAYMENTREQUEST_0_DESC"=>"Order ".$this->getOrder()->getNumber(),
			"RETURNURL"=>parent::getCallbackHost().self::PAYPAL_RETURNURL,
			"CANCELURL"=>parent::getCallbackHost().self::PAYPAL_CANCELURL,
			"NOTIFYURL"=>self::PAYPAL_NOTIFYURL,
			"NOSHIPPING"=>self::PAYPAL_NOSHIPPING
		);
		
		parse_str($this->doPostRequest($this->getApiUri(),$request),$response);
		
		if(!isset($response['ACK']) || $response['ACK']!=="Success") {
			
			return new ZApiError("Unable to retreive token from paypal",$response['ACK']);
			
		} // end if
			
		// Redirect to Paypal secure payment page
		
		$uri = $this->getCheckoutUri()."?cmd=_express-checkout&token=".urlencode($response['TOKEN']);
		
		header("Location: ".$uri);
		exit;
		
	} // end function
	
	public function setToken($token) {
		
		$this->token = $token;
		
	} // end function
	
	public function requestPayment() {
			
		$request = array(
			"METHOD"=>"GetExpressCheckoutDetails",
			"USER"=>$this->user,
			"PWD"=>$this->pwd,
			"SIGNATURE"=>$this->signature,
			"VERSION"=>self::PAYPAL_VERSION,
			"TOKEN"=>$this->token
		);
		
		parse_str($this->doPostRequest($this->getApiUri(),$request),$response);
		
		if(!isset($response['ACK']) || $response['ACK']!=="Success") {
			
			return new ZApiError("Unable to request the payment details from paypal",$response['ACK']);
			
		} // end if
		
		// Confirm the payment
		
		return $this->confirmPayment($response['TOKEN'],$response['AMT'],$response['PAYERID']);
		
	} // end function
	
	private function confirmPayment($token,$amount,$payerid) {
			
		$request = array(
			"METHOD"=>"DoExpressCheckoutPayment",
			"USER"=>$this->user,
			"PWD"=>$this->pwd,
			"SIGNATURE"=>$this->signature,
			"VERSION"=>self::PAYPAL_VERSION,
			"TOKEN"=>$token,
			"PAYMENTREQUEST_0_PAYMENTACTION"=>"Sale",
			"PAYMENTREQUEST_0_AMT"=>round($amount,2),
			"PAYMENTREQUEST_0_CURRENCYCODE"=>"EUR",
			"PAYERID"=>$payerid,
			"NOTIFYURL"=>self::PAYPAL_NOTIFYURL
		);
		
		parse_str($this->doPostRequest($this->getApiUri(),$request),$response);
		
		if(!isset($response['ACK']) || $response['ACK']!=="Success") {
			
			return new ZApiError("Unable to confirm the payment to paypal",$response['ACK']);
			
		} // end if
		
		$session = JFactory::getSession();		
		$session->clear("payment","zibbra");
		
		return $response['PAYMENTINFO_0_ACK']=="Success";
		
	} // end function

	private function doPostRequest($url,$data,$optional_headers = null) {
		
		$data = http_build_query($data);
		
		$params = array('http' =>
			array(
				'method' => 'POST',
				'content' => $data
			)
		);
		
		if($optional_headers !== null) {
		
			$params['http']['header'] = $optional_headers;
			
		} // end if
		
		$ctx = stream_context_create($params);
		$fp = @fopen($url,'rb',false,$ctx);
		
		if(!$fp) {
			
			throw new Exception("Problem with $url,$php_errormsg");
			
		} // end if
		
		$response = @stream_get_contents($fp);
		
		$this->logData($data,$response);
		
		if($response === false) {
			
			throw new Exception("Problem reading data from $url,$php_errormsg");
			
		} // end if
		
		return $response;
	  
	} // end function
	
	private function logData($request,$response) {
		
		$app = JFactory::getApplication();
		$logfile = $app->getCfg("log_path")."/api-".date("Ymd").".log";
	
		$data = file_get_contents($logfile);
		
		parse_str($request,$arrRequest);
		parse_str($response,$arrResponse);
	
		$txt = "Date: ".date("d/m/Y H:i:s")."\n";
		$txt .= "Request: ".var_export($arrRequest,true)."\n";
		$txt .= "Response: ".var_export($arrResponse,true)."\n";
		$txt .= "\n";
		
		$data = $txt.$data;
	
		file_put_contents($logfile,$data);
	
	} // end function

} // end class
