<?php

class ZEmarketing extends ZObject {

	public static function subscribe($email) {
	
		$data = array(
			"languagecode"=>ZController::getLanguage(),
			"email"=>$email
		);
		
		$response = parent::load("emarketing","subscribe",$data);
		
		if($response instanceof ZApiError) return false;
		
		return true;
	
	} // end function
	
} // end class

?>