<?php

jimport("zibbra.data.cart.item");
jimport("zibbra.data.shipping.method");
jimport("zibbra.data.order");

class ZCart extends ZObject {

	protected $cartid;
	protected $customerid = null;
	protected $amount;
	protected $vat;
	protected $timestamp;
	
	private $arrItems = array();
	
	private static $instance = null;
	
	public function addItem(ZCartItem $oItem) {
		
		$this->arrItems[] = $oItem;
		
	} // end function
	
	public function getAmount() {
		
		return $this->amount;
		
	} // end function
	
	public function getVat() {
		
		$vat = $this->vat;
		
		$shipping = $this->getShippingMethod();
		
		if($shipping) {
			
			$vat += $shipping->getVat();
			
		} // end if
		
		return $vat;
		
	} // end function
	
	public function getTotal() {
		
		$shipping = $this->getShippingMethod();
		$payment = $this->getPayment();
		
		$total = $this->amount + $this->vat;
		
		if($shipping) {
			
			$total += $shipping->getPriceVatIncl();
			
		} // end if
		
		if($payment) {
			
			$total += $payment->getPrice();
			
		} // end if
	
		return $total;
	
	} // end function
	
	public function getValutaSymbol() {
		
		return "&euro;";
		
	} // end function
	
	public function isEmpty() {
		
		return count($this->arrItems)==0;
		
	} // end function
	
	public function save() {
		
		foreach($this->arrItems as $oItem) {
			
			if($oItem->isNew()) {
				
				$oItem->save();
				
			} // end if
			
		} // end foreach
		
	} // end function
	
	public function getItems() {
	
		return $this->arrItems;
	
	} // end function
	
	public function getItem($cartitemid) {
	
		foreach($this->arrItems as $oItem) {
		
			if($oItem->getCartitemid()==$cartitemid) return $oItem;
		
		} // end foreach
		
		return false;
	
	} // end function
	
	public function clear() {
		
		$params = array(
			"sessionid"=>session_id()
		);
		
		parent::load("cart","clear",$params);
	
	} // end function
	
	public function setCustomer($customerid) {
	
		if($this->customerid!=null && $customerid!=null) return;
	
		$params = array(
			"sessionid"=>session_id(),
			"customerid"=>(int) $customerid
		);
		
		parent::load("cart","updateCart",$params);
		
		return true;
	
	} // end function
	
	public function clearCustomer() {
	
		if($this->customerid==null) return;
	
		$params = array(
			"sessionid"=>session_id(),
			"customerid"=>null
		);
		
		parent::load("cart","updateCart",$params);
		
		return true;
	
	} // end function
	
	public function getShippingMethod() {
		
		$session = JFactory::getSession();
		$shipping = $session->get("shipping",false,"zibbra");
		
		return $shipping ? ZShippingMethod::get($shipping) : false;		
		
	} // end function
	
	public function setShippingMethod($enterpriseshippingmethodid) {
			
		$session = JFactory::getSession();
		$session->set("shipping",$enterpriseshippingmethodid,"zibbra");
		
	} // end function
	
	public function getPayment() {
			
		$session = JFactory::getSession();
		$payment = $session->get("payment",false,"zibbra");
		return $payment ? ZPayment::get($payment) : false;
		
	} // end function
	
	public function setPayment($type) {
		
		$session = JFactory::getSession();
		
		$session->set("payment",$type,"zibbra");
		
	} // end function
	
	/**
	 * @todo setPayment
	 * @return ZOrder
	 */
	public function cartToOrder($comments=null) {
		
		$session = JFactory::getSession();
		
		$params = array(
			"sessionid"=>session_id(),
			"return_order"=>true
		);
		
		if($comments!==null && $comments) {
						
			$params['comments'] = $comments;
			
		} // end if
		
		if($order_data = $session->get("order",false,"zibbra")) {
						
			return ZOrder::parseItem($order_data);
			
		} // end if

		$oShipping = $this->getShippingMethod();
		$oPayment = $this->getPayment();
		$response = parent::call("cart","cartToOrder",$params);
		$session->set("order",$response,"zibbra");
		$session->set("shipping.price",$oShipping->getPrice(),"zibbra");
		
		$oOrder = ZOrder::parseItem($response);
		
		// Add shipping-cost and payment-cost to the order
		
		if(is_numeric($response['orderid']) && $oShipping) {
			
			// Set the shipping adapter

			$params = array(
				"orderid"=> (int) $oOrder->getOrderid(),
				"enterpriseshippingmethodid"=> (int) $oShipping->getEnterpriseshippingmethodid(),
				"price"=> (float) $oShipping->getPrice()
			);

			parent::call("order","setShippingMethod",$params);
			
			//$session->clear("shipping","zibbra");
			
			// Set the payment adapter

			$params = array(
				"orderid"=> (int) $oOrder->getOrderid(),
				"paymentadapterid"=> $oPayment->getType()
			);

			parent::call("order","setPaymentAdapter",$params);
			
			//$session->clear("payment","zibbra");
			
			if($oPayment->getType()==="transcription") {
			
				// Send confirmation email
	
				$params = array(
					"orderid"=> (int) $oOrder->getOrderid(),
					"payment_method"=> $oPayment->getType()
				);
	
				parent::call("order","sendConfirmation",$params);
			
			} // end if
		
		} // end if
		
		return $oOrder;
		
	} // end function
	
	public static function parse($data) {
		
		$oCart = new ZCart();
		
		$oCart->cartid = $data['cartid'];
		$oCart->customerid = $data['customerid'];
		$oCart->amount = $data['amount'];
		$oCart->vat = $data['vat'];
		$oCart->timestamp = $data['timestamp'];
		
		if(isset($data['items']) && count($data['items'])>0) {
		
			foreach($data['items'] as $item) {
			
				$oCart->addItem(ZCartItem::parse($item));
			
			} // end foreach
		
		} // end if
		
		return $oCart;
		
	} // end function
	
	public static function getInstance() {
	
		if(self::$instance===null) {
		
			$params = array(
				"sessionid"=>session_id()
			);
			
			$data = parent::load("cart","getCart",$params);
			
			if($data instanceof ZApiError) return false;
			
			self::$instance = self::parse($data);
		
		} // end if
		
		if(is_null(self::$instance->customerid)) {
			
			if(!isset($_SESSION['zibbra']['user'])) {
				
				return false;
				
			} // end if
			
			if(!$oCustomerUser = unserialize($_SESSION['zibbra']['user'])) {
				
				return false;
				
			} // end if
			
			self::$instance->setCustomer($oCustomerUser->getCustomerid());
			
		} // end if
		
		return self::$instance;
	
	} // end function
	
	public static function reset() {
		
		$params = array(
			"sessionid"=>session_id()
		);
		
		parent::load("cart","reset",$params);
		
		$session = JFactory::getSession();
		$session->clear("order","zibbra");
		$session->clear("orderid","zibbra");
		$session->clear("order_amount","zibbra");
		
	} // end function

} // end class
