<?php

class ZManufacturer extends ZObject {
	
	protected $manufacturerid;
	protected $name;
	
	public function getManufacturerid() {
		
		return $this->manufacturerid;
		
	} // end function
	
	public function getName() {
		
		return $this->name;
		
	} // end function
	
	public static function parseItem($item) {
		
		$oManufacturer = new ZManufacturer();
		$oManufacturer->manufacturerid = $item['id'];
		$oManufacturer->name = $item['name'];
				
		return $oManufacturer;
		
	} // end function
	
	public static function getManufacturers() {
		
		$data = array(
			"languagecode"=>ZController::getLanguage()
		);
		
		$items = parent::load("product","getManufacturers",$data);
		
		$arrManufacturers = array();
		
		if(is_array($items)) {
		
			foreach($items as $item) {
				
				$arrManufacturers[] = self::parseItem($item);
				
			} // end foreach
		
		} // end if
		
		return $arrManufacturers;
		
	} // end function
	
} // end class