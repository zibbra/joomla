<?php

class ZCategoryImage extends ZImage {
	
	protected $max_size = 200;
	
	public static function parseItem($item,$max_size=200) {
		
		$oImage = new ZCategoryImage();
		$oImage->max_size = $max_size;
		
		return parent::parseItem($oImage,$item);
		
	} // end function
	
} // end class

?>