<?php

class ZCountry extends ZObject {

	protected $countrycode;
	protected $name;
	protected $default = false;
	
	private static $oDefaultCountry = null;
	private static $arrCountries = null;
	
	public function getCountrycode() {
		
		return $this->countrycode;
		
	} // end function
	
	public function getName() {
		
		return $this->name;
		
	} // end function
	
	public function isDefault() {
		
		return $this->default;
		
	} // end function
	
	public static function parse($data) {
		
		$oCountry = new ZCountry();
					
		$oCountry->countrycode = $data['countrycode'];
		$oCountry->name = $data['name'];
		$oCountry->default = $data['default']=="Y";
		
		return $oCountry;
		
	} // end function
	
	public static function load() {
		
		if(self::$arrCountries===null) {
			
			self::$arrCountries = array();
			
			$params = array(
				"languagecode"=>ZController::getLanguage()
			);
			
			$response = parent::load("customer","getCountries",$params);
				
			if($response instanceof ZApiError) {
				
				self::$arrCountries = $response;
				
			}else{
					
				foreach($response as $data) {
					
					$oCountry = ZCountry::parse($data);
					
					if($oCountry->isDefault()) self::$oDefaultCountry = $oCountry;
					
					self::$arrCountries[] = $oCountry;
					
				} // end foreach
			
			} // end if
			
		} // end if
		
		return self::$arrCountries;
		
	} // end function
	
	public static function getDefaultCountry() {
		
		if(self::$oDefaultCountry===null) self::load();
		
		return self::$oDefaultCountry;
		
	} // end function

} // end class
