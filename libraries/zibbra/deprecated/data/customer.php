<?php

jimport("zibbra.data.cart");
jimport("zibbra.data.customer.company");
jimport("zibbra.data.customer.user");
jimport("zibbra.data.customer.address");
jimport("zibbra.data.customer.contact");
jimport("zibbra.data.customer.document");
jimport("zibbra.data.customer.order");
jimport("zibbra.data.customer.invoice");

class ZCustomer extends ZObject {

	private $customerid;
	private $languagecode;	
	private $company;
	private $billing_address;
	private $shipping_address;	
	private $arrContacts = array();
	private $arrUsers = array();
	private $arrOrders = null;
	private $arrInvoices = null;
	
	public function getLanguagecode() {
		
		return $this->languagecode;
		
	} // end function
	
	public function setLanguagecode($languagecode) {
		
		$this->languagecode = $languagecode;
		
	} // end function
	
	/**
	 * @return ZCustomerCompany
	 */
	public function getCompany() {
		
		return $this->company;
		
	} // end function
	
	public function setCompany(ZCustomerCompany $oCompany) {
		
		$this->company = $oCompany;
		
	} // end function
	
	public function addContact(ZCustomerContact $oContact) {
	
		$this->arrContacts[] = $oContact;
	
	} // end function
	
	public function addUser(ZCustomerUser $oUser) {
		
		$this->arrUsers[] = $oUser; 
		
	} // end function
	
	/**
	 * @return ZCustomerAddress
	 */
	public function getBillingAddress() {
		
		return $this->billing_address;
		
	} // end function
	
	public function setBillingAddress(ZCustomerAddress $oBillingAddress) {
		
		$this->billing_address = $oBillingAddress;
		
	} // end function
	
	/**
	 * @return ZCustomerAddress
	 */
	public function getShippingAddress() {
		
		return $this->shipping_address;
		
	} // end function
	
	public function setShippingAddress(ZCustomerAddress $oShippingAddress) {
		
		$this->shipping_address = $oShippingAddress;
		
	} // end function
	
	/**
	 * @return ZCustomerContact
	 */
	public function getPrimaryContact() {
		
		foreach($this->arrContacts as $oContact) {
			
			if($oContact->isPrimary()) return $oContact;
			
		} // end foreach
		
		return false;
		
	} // end function
	
	public function getContacts() {
	
		return $this->arrContacts;
	
	} // end function
	
	public function save() {
		
		$params = array(
			"customer_type"=>"customer",
			"customer_languagecode"=>$this->languagecode,
			"customer_remark"=>"Registered on ".date("Y-m-d H:i:s")
		);
		
		$this->company->toArray($params);
		$this->arrContacts[0]->toArray($params);
		$this->billing_address->toArray($params,"billing");
		$this->shipping_address->toArray($params,"delivery");
		
		if($this->customerid===null) {
			
			$customerid = parent::load("customer","addCustomer",$params);
			
			if($customerid instanceof ZApiError) {
				
				return false;
				
			} // end if
			
			$params = array(
				"customerid"=>$customerid
			);
			$this->arrUsers[0]->toArray($params);
			
			$response = parent::load("customer","addUser",$params);
			
			if($response instanceof ZApiError) {
				
				return false;
				
			} // end if
			
		}else{
			
			$params['customerid'] = $this->customerid;
			$params['customercontactid'] = (int) $this->arrContacts[0]->getContactid();
			$params['companyid'] = (int) $this->company->getCompanyid();
			
			$response = parent::load("customer","updateCustomer",$params);
			
			if($response instanceof ZApiError) {
				
				return false;
				
			} // end if
			
		} // end if
		
		return true;
		
	} // end function
	
	public function addOrder(ZCustomerOrder $oOrder) {
	
		$this->arrOrders[] = $oOrder;
	
	} // end function
	
	public function getOrders($limit=false) {
		
		if($this->arrOrders===null) {
			
			$params = array(
				"customerid"=>(int) $this->customerid,
				"languagecode"=>ZController::getLanguage()
			);
			
			if($limit!==false) $params['limit'] = (int) $limit;
		
			$response = parent::load("order","getOrders",$params);
			
			if($response instanceof ZApiError) return false;
			
			foreach($response as $data) {
			
				$oCustomerOrder = ZCustomerOrder::parse($data);
				
				$this->addOrder($oCustomerOrder);
			
			} // end foreach
			
		} // end if
	
		return $this->arrOrders;
	
	} // end function
	
	public function addInvoice(ZCustomerInvoice $oInvoice) {
	
		$this->arrInvoices[] = $oInvoice;
	
	} // end function
	
	public function getInvoices($limit=false) {
		
		if($this->arrInvoices===null) {
			
			$params = array(
				"customerid"=>(int) $this->customerid
			);
			
			if($limit!==false) $params['limit'] = (int) $limit;
		
			$response = parent::load("invoice","getInvoices",$params);
			
			if($response instanceof ZApiError) return false;
			
			foreach($response as $data) {
			
				$oCustomerInvoice = ZCustomerInvoice::parse($data);
				
				$this->addInvoice($oCustomerInvoice);
			
			} // end foreach
			
		} // end if
	
		return $this->arrInvoices;
	
	} // end function
	
	/*
	
	public static function reset($email) {
		
		$api = ZibbraApi::getInstance();
		return $api->call("customer","passwordLost",array("email"=>$email));
		
	} // end function
	
	public static function getCountries() {

		$api = new Zibbra_Api();
		$params = array();
		
		return $api->call("customer","getCountries",array("languagecode"=>ZibbraApi::getActiveLanguage()));
	
	} // end function
	
	*/
	
	public static function login($username,$password) {
	
		$data = array(
			"sessionid"=>session_id(),
			"username"=>$username,
			"password"=>$password
		);
		
		$response = parent::load("customer","login",$data);
		
		if(!$response) {
			
			unset($_SESSION['zibbra']['user']);
			return false;
			
		} // end if
		
		$oCustomerUser = ZCustomerUser::parse($response);
		$oCustomerUser->setPassword($password);
		
		if(!isset($_SESSION['zibbra'])) $_SESSION['zibbra'] = array();

		$_SESSION['zibbra']['user'] = serialize($oCustomerUser);
		
		$oCart = ZCart::getInstance();
		$oCart->setCustomer($response['customerid']);
		
		return true;
	
	} // end function
	
	public static function logout() {

		unset($_SESSION['zibbra']);
		
		$oCart = ZCart::getInstance();
		$oCart->clearCustomer();
		
	} // end function
	
	public static function isAuthenticated() {

		return isset($_SESSION['zibbra']['user']);
	
	} // end function
	
	/*
	public static function getUser() {
		
		if(self::isAuthenticated()) {
		
			$oUser = unserialize($_SESSION['zibbra']['user']);
			
			return $oUser;
		
		} // end if

		return null;
	
	} // end function
	
	public static function getCustomerid() {

		if(self::isAuthenticated()) {
		
			$oCustomerUser = unserialize($_SESSION['zibbra']['user']);
			
			return $oCustomerUser->getCustomerid();
		
		} // end if

		return null;
	
	} // end function
	*/
	
	public static function load($document_type=null) {

		if(!self::isAuthenticated()) {
		
			return false;
		
		} // end if
		
		$oCustomerUser = unserialize($_SESSION['zibbra']['user']);

		// Load customer
		
		$data = parent::load("customer","getCustomer",array("customerid"=>(int) $oCustomerUser->getCustomerid()));
		
		$oCustomer = ZCustomer::parseItem($data);
		
		return $oCustomer;
	
	} // end function
	
	public static function parseItem($data) {
		
		$oCustomer = new ZCustomer();
		
		$oCustomer->customerid = (int) $data['customerid'];
		$oCustomer->languagecode = $data['languagecode'];
		$oCustomer->company = ZCustomerCompany::parse($data['company']);
		$oCustomer->billing_address = ZCustomerAddress::parse(ZCustomerAddress::TYPE_BILLING,$data['company']);
		$oCustomer->shipping_address = ZCustomerAddress::parse(ZCustomerAddress::TYPE_SHIPPING,$data['company']);
		
		if(isset($data['users']) && is_array($data['users'])) {
			
			foreach($data['users'] as $user) {
					
				$oCustomer->addUser(ZCustomerUser::parse($user));
				
			} // end foreach
			
		} // end if
		
		if(isset($data['contacts']) && is_array($data['contacts'])) {
			
			foreach($data['contacts'] as $contact) {
					
				$oCustomer->addContact(ZCustomerContact::parse($contact));
				
			} // end foreach
			
		} // end if
		
		return $oCustomer;
		
	} // end function
	
} // end class

?>