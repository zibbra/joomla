<?php

class ZCartItem extends ZObject {
	
	protected $cartitemid;
	protected $productid;
	protected $description;
	protected $quantity;
	protected $amount;
	protected $vat;
	protected $parentid;
	protected $period;
	protected $variations = null;
	
	public function getCartitemid() {
		
		return $this->cartitemid;
		
	} // end function
	
	public function getProductid() {
		
		return $this->productid;
		
	} // end function
	
	public function getDescription() {
		
		return $this->description;
		
	} // end function
	
	public function getQuantity() {
		
		return $this->quantity;
		
	} // end function
	
	public function getAmount() {
		
		return $this->amount;
		
	} // end function
	
	public function getVat() {
		
		return $this->vat;
		
	} // end function
	
	public function getTotal() {
		
		return $this->quantity * $this->amount;
		
	} // end function
	
	public function setProductid($productid) {
		
		$this->productid = $productid;
		
	} // end function
	
	public function setQuantity($quantity) {
		
		$this->quantity = $quantity;
		
	} // end function
	
	public function setVariations($variationids) {
		
		$this->variations = $variationids;
		
	} // end function
	
	public function isNew() {
		
		return empty($this->cartitemid);
		
	} // end function
	
	public function save() {
		
		if(!empty($this->cartitemid)) {

			$params = array(
				"sessionid"=>session_id(),
				"cartitemid"=>(int) $this->cartitemid,
				"quantity"=>(int) $this->quantity
			);
			
			if($this->variations!==null) $params['variationoptions'] = $this->variations;
			
			$response = parent::load("cart","updateItem",$params);
			
			/*
			echo "<pre>";
			var_dump($params);
			var_dump($response);
			echo "</pre>";
			*/
			
		}else{

			$params = array(
				"sessionid"=>session_id(),
				"quantity"=>(int) $this->quantity,
				"productid"=>(int) $this->productid,
				"languagecode"=>ZController::getLanguage()
			);
			
			if($this->variations!==null) $params['variationoptions'] = $this->variations;
			
			$response = parent::load("cart","addItem",$params);
			
			/*
			echo "<pre>";
			var_dump($params);
			var_dump($response);
			echo "</pre>";
			exit;
			*/
			
			if($response instanceof ZApiError) return false;
			
			$this->cartitemid = $response['cartitemid'];
			
		} // end if
		
	} // end function

	public function delete() {

		$params = array(
			"sessionid"=>session_id(),
			"cartitemid"=>(int) $this->cartitemid
		);
		
		$response = parent::load("cart","removeItem",$params);

		return $response;

	} // end function
	
	public static function parse($data) {
		
		$oCartItem = new ZCartItem();
		
		$oCartItem->cartitemid = $data['cartitemid'];
		$oCartItem->productid = $data['productid'];
		$oCartItem->quantity = $data['quantity'];
		$oCartItem->description = $data['description'];
		$oCartItem->amount = $data['amount'];
		$oCartItem->vat = $data['vat'];
		$oCartItem->parentid = isset($data['parentid']) ? $data['parentid'] : null;
		$oCartItem->period = isset($data['period']) ? $data['period'] : null;
		$oCartItem->variations = isset($data['variations']) ? $data['variations'] : null;
		
		return $oCartItem;
		
	} // end function
	
} // end class

?>