<?php

class ZProductFilter {
	
	protected $type;
	protected $field;
	protected $name;
	protected $min;
	protected $max;
	protected $unit;
	protected $quantity;
	protected $value;
	protected $value_min;
	protected $value_max;
	protected $arrOptions = array();

	const TYPE_RANGE = "range";
	const TYPE_LIST = "list";
	
	public function __construct($type,$field,$name=null) {
		
		$this->type = $type;
		$this->field = $field;
		$this->name = $name;
		
	} // end function
	
	public function setMin($min) {
		
		$this->min = $min;
		
	} // end function
	
	public function setMax($max) {
		
		$this->max = $max;
		
	} // end function
	
	public function setUnit($unit) {
		
		$this->unit = $unit;
		
	} // end function
	
	public function setQuantity($quantity) {
		
		$this->quantity = $quantity;
		
	} // end function
	
	public function setOptions($arrOptions) {
		
		$this->arrOptions = $arrOptions;
		
	} // end function
	
	public function setValue($value) {
		
		$this->value = $value;
		
	} // end function
	
	public function setValueMin($value_min) {
		
		$this->value_min = $value_min;
		
	} // end function
	
	public function setValueMax($value_max) {
		
		$this->value_max = $value_max;
		
	} // end function
	
	public function setName($name) {
		
		$this->name = $name;
		
	} // end function
	
	public function getType() {
		
		return $this->type;
		
	} // end function
	
	public function getField() {
		
		return $this->field;
		
	} // end function
	
	public function getName() {
		
		return $this->name;
		
	} // end function
	
	public function getMin() {
		
		return $this->min;
		
	} // end function
	
	public function getMax() {
		
		return $this->max;
		
	} // end function
	
	public function hasUnit() {
		
		return $this->unit!==null;
		
	} // end function
	
	public function getUnit() {
		
		return $this->unit;
		
	} // end function
	
	public function getQuantity() {
		
		return $this->quantity;
		
	} // end function
	
	public function getOptions() {
		
		return $this->arrOptions;
		
	} // end function
	
	public function getFilter($key=null) {
		
		if($this->type==self::TYPE_RANGE) {
		
			return array(
				array(
						"key"=>!empty($key) ? $key : $this->field,
						"condition"=>">=",
						"value"=>$this->value_min
				),
				array(
						"key"=>!empty($key) ? $key : $this->field,
						"condition"=>"<=",
						"value"=>$this->value_max
				)
			);
		
		}elseif($this->type==self::TYPE_LIST) {
			
			$filter = array();
			
			foreach($this->arrOptions as $option) {
				
				$filter[] = array(
					"key"=>!empty($key) ? $key : $this->field,
					"condition"=>"=",
					"value"=>$option['value']
				);
				
			} // end foreach
			
			return $filter;
			
		} // end if
		
	} // end function
	
} // end class

?>