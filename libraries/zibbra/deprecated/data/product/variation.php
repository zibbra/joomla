<?php

class ZProductVariation {
	
	private $id;
	private $name;
	private $options = array();
	
	public function getID() {
		
		return $this->id;
		
	} // end function
	
	public function getName() {
		
		return $this->name;
		
	} // end function
	
	public function isInStock() {
		
		return $this->in_stock=="Y";
		
	} // end function
	
	public function getOptions() {
		
		return $this->options;
		
	} // end function
	
	public static function parseItem($data) {
		
		$oVariation = new ZProductVariation();
		$oVariation->id = $data['id'];
		$oVariation->name = $data['name'];
		
		foreach($data['options'] as $option) {
			
			$o = new StdClass();
			$o->id = $option['id'];
			$o->name = $option['name'];
			$o->in_stock = $option['in_stock']=="Y";
		
			$oVariation->options[$option['id']] = $o;
		
		} // end foreach
		
		return $oVariation;
		
	} // end function
	
} // end class

?>