<?php

class ZProductPrice extends ZObject {
	
	private $productpriceid;
	private $level;
	private $level_description;
	private $period;
	private $period_description;
	private $type;
	private $price;
	private $base_price;
	private $vat;
	
	public function getLevel() {
		
		return $this->level;
		
	} // end function
	
	public function getLevelDescription() {
		
		return $this->level_description;
		
	} // end function
	
	public function getPeriod() {
		
		return $this->period;
		
	} // end function
	
	public function getPeriodDescription() {
		
		return $this->period_description;
		
	} // end function
	
	public function getType() {
		
		return $this->type;
		
	} // end function
	
	public function getPrice() {
		
		return $this->price;
		
	} // end function
	
	public function getBasePrice() {
		
		return $this->base_price;
		
	} // end function
	
	public function getVat() {
		
		return $this->vat;
		
	} // end function
	
	public static function parseItem($item) {
		
		$oProductPrice = new ZProductPrice();
		
		$oProductPrice->productpriceid = $item['productpriceid'];
		$oProductPrice->level = $item['level'];
		$oProductPrice->level_description = $item['level_description'];
		$oProductPrice->period = $item['period'];
		$oProductPrice->period_description = $item['period_description'];
		$oProductPrice->type = $item['type'];
		$oProductPrice->price = $item['price'];
		$oProductPrice->vat = $item['vat'];
		
		if(isset($item['base_price'])) {
		
			$oProductPrice->base_price = $item['base_price'];
		
		} // end if
		
		return $oProductPrice;
		
	} // end function
	
} // end class

?>