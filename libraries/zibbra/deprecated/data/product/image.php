<?php

class ZProductImage extends ZImage {
	
	protected $max_size = 300;
	
	public static function parseItem($item,$max_size=300) {
		
		$oImage = new ZProductImage();
		$oImage->max_size = $max_size;
		
		return parent::parseItem($oImage,$item);
		
	} // end function
	
} // end class

?>