<?php

class ZProductProperty extends ZObject {
	
	private $productpropertyid;
	private $categorypropertyid;
	private $categorypropertygroupid;
	private $group;
	private $name;
	private $unit;
	private $value;
	
	public function getProductPropertyID() {
		
		return $this->productpropertyid;
		
	} // end function
	
	public function getCategoryPropertyID() {
		
		return $this->categorypropertyid;
		
	} // end function
	
	public function getCategoryPropertyGroupID() {
		
		return $this->categorypropertygroupid;
		
	} // end function
	
	public function getGroup() {
		
		return $this->group;
		
	} // end function
	
	public function getName() {
		
		return $this->name;
		
	} // end function
	
	public function hasUnit() {
		
		return !empty($this->unit);
		
	} // end function
	
	public function getUnit() {
		
		return $this->unit;
		
	} // end function
	
	public function getValue() {
		
		$value = $this->value;
		
		if($value=="Y") $value = JText::_("Yes");
		if($value=="N") $value = JText::_("No");
		
		return $value;
		
	} // end function
	
	public static function parseItem($item) {
		
		$oProductProperty = new ZProductProperty();
		$oProductProperty->productpropertyid = $item['productpropertyid'];
		$oProductProperty->categorypropertyid = $item['categorypropertyid'];
		$oProductProperty->categorypropertygroupid = $item['categorypropertygroupid'];
		$oProductProperty->group = $item['group'];
		$oProductProperty->name = $item['name'];
		$oProductProperty->unit = $item['unit'];
		$oProductProperty->value = $item['value'];
				
		return $oProductProperty;
		
	} // end function
	
} // end class

?>