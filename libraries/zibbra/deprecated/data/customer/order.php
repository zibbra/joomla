<?php

jimport("zibbra.data.customer.order.item");

class ZCustomerOrder extends ZCustomerDocument {
	
	protected $orderid;
	protected $number;
	protected $amount;
	protected $vat;
	protected $confirmed;
	protected $cancelled;
	protected $status;
	protected $timestamp_insert;

	private $arrItems;
	
	public function getNumber() {
		
		return $this->number;
		
	} // end function
	
	public function getStatus() {
		
		return $this->status;
		
	} // end function
	
	public function getDate() {

		return date("d-m-Y",strtotime($this->timestamp_insert));

	} // end function
	
	public function getAmount() {

		return ($this->amount + $this->vat);

	} // end function

	public function addItem(ZCustomerOrderItem $oItem) {

		$this->arrItems[] = $oItem;

	} // end function

	public function getItems() {

		return $this->arrItems;

	} // end function
	
	public static function parse($data) {
		
		$oOrder = new ZCustomerOrder();
		
		parent::parse($oOrder,$data);
		
		if(isset($data['items'])) {
		
			foreach($data['items'] as $item) {
			
				$oItem = ZCustomerOrderItem::parse($item);
					
				$oOrder->addItem($oItem);
			
			} // end foreach
		
		} // end if
		
		return $oOrder;
		
	} // end function
	
	public static function download($document_nr) {

		return parent::downloadByType("order",$document_nr);

	} // end function
	
} // end class

?>