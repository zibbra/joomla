<?php
/**
 * libraries/zibbra/data/customer/user.php
 *
 * @package Zibbra.Library
 * @subpackage Object.Customer
 * @author Zibbra Technologies <info@zibbra.com>
 * @copyright Copyright (C) 2012 Zibbra Technologies - All rights reserved.
 * @license BSD
 */

/**
 * This class represents a Zibbra Enterprise Customer User
 *
 * @package Zibbra.Library
 * @subpackage Object.Customer
 * @author Zibbra Technologies <info@zibbra.com>
 * @todo Cleanup and documentation
 */
class ZCustomerUser extends ZObject {

	protected $customerid;
	protected $firstname;
	protected $lastname;
	protected $email;
	protected $username;
	protected $password;
	
	public function getCustomerid() {
		
		return $this->customerid;
		
	} // end function
	
	public function getEmail() {
		
		return $this->email;
		
	} // end function
	
	public function setFirstname($firstname) {
		
		$this->firstname = $firstname;
		
	} // end function
	
	public function setLastname($lastname) {
		
		$this->lastname = $lastname;
		
	} // end function
	
	public function setEmail($email) {
		
		$this->email = $email;
		
	} // end function
	
	public function getUsername() {
		
		return $this->username;
		
	} // end function
	
	public function setUsername($username) {
		
		$this->username = $username;
		
	} // end function
	
	public function getPassword() {
		
		return $this->password;
		
	} // end function
	
	public function setPassword($password) {
		
		$this->password = $password;
		
	} // end function
	
	public function toArray(&$arr) {
		
		$arr['email'] = $this->email;
		$arr['firstname'] = $this->firstname;
		$arr['lastname'] = $this->lastname;
		$arr['password'] = $this->password;
		
	} // end function
	
	public function changePassword($old_password,$new_password) {
		
		$params = array(
			"username"=>$this->username,
			"old_password"=>$old_password,
			"new_password"=>$new_password
		);
		
		$response = parent::load("customer","changePassword",$params);
		
		if(!$response) {
		
			return false;
		
		} // end if
		
		$this->password = $new_password;
		
		return true;
		
	} // end function
	
	public static function parse($data) {
		
		$oCustomerUser = new ZCustomerUser();
		
		if(isset($data['customerid'])) {
		
			$oCustomerUser->customerid = (int) $data['customerid'];
		
		} // end if
		
		$oCustomerUser->firstname = $data['firstname'];
		$oCustomerUser->lastname = $data['lastname'];
		$oCustomerUser->email = $data['email'];
		$oCustomerUser->username = $data['username'];
		
		return $oCustomerUser;
		
	} // end function
	
	public static function resetPassword($email) {
		
		$params = array(
			"email"=>$email
		);
		
		$response = parent::load("customer","passwordLost",$params);
		
		if(!$response) {
		
			return false;
		
		} // end if
		
		return true;
		
	} // end function
	
	/*
	public function save() {
	
		$api = ZibbraApi::getInstance();
		$arr = $this->deflate();
		$arr['customerid'] = (int) $arr['customerid'];
		unset($arr['username']);
		
		return $api->call("customer","addUser",$arr);
	
	} // end function
	*/
} // end class

?>
