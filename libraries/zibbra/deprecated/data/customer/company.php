<?php 

class ZCustomerCompany extends ZObject {
	
	protected $companyid;
	protected $name;
	protected $phone;
	protected $fax;
	protected $email;
	protected $website;
	protected $vat_nr;
	
	public function getCompanyid() {
		
		return $this->companyid;
		
	} // end function
	
	public function getName() {
		
		return $this->name;
		
	} // end function
	
	public function getPhone() {
		
		return $this->phone;
		
	} // end function
	
	public function getFax() {
		
		return $this->fax;
		
	} // end function
	
	public function getEmail() {
		
		return $this->email;
		
	} // end function
	
	public function getWebsite() {
		
		return $this->website;
		
	} // end function
	
	public function getVatNr() {
		
		return $this->vat_nr;
		
	} // end function
	
	public function setName($name) {
		
		$this->name = !empty($name) ? $name : null;
		
	} // end function
	
	public function setPhone($phone) {
		
		$this->phone = !empty($phone) ? $phone : null;
		
	} // end function
	
	public function setFax($fax) {
		
		$this->fax = !empty($fax) ? $fax : null;
		
	} // end function
	
	public function setEmail($email) {
		
		$this->email = !empty($email) ? $email : null;
		
	} // end function
	
	public function setWebsite($website) {
		
		$this->website = !empty($website) ? $website : null;
		
	} // end function
	
	public function setVatNr($vat_nr) {
		
		$this->vat_nr = !empty($vat_nr) ? $vat_nr : null;
		
	} // end function
	
	public function toArray(&$arr) {
		
		if(!empty($this->name)) $arr['company_name'] = $this->name;
		if(!empty($this->phone)) $arr['company_phone'] = $this->phone;
		if(!empty($this->fax)) $arr['company_fax'] = $this->fax;
		if(!empty($this->email)) $arr['company_email'] = $this->email;
		if(!empty($this->website)) $arr['company_website'] = $this->website;
		if(!empty($this->vat_nr)) $arr['company_vat_nr'] = $this->vat_nr;
		
	} // end function
	
	/**
	 * Parses data into an object
	 * 
	 * @param array $data
	 * @return ZCustomerCompany
	 */
	public static function parse($data,$prefix="company") {
		
		$oCustomerCompany = new ZCustomerCompany();
		
		parent::parse($oCustomerCompany,$data,$prefix);
		
		return $oCustomerCompany;
		
	} // end function
	
} // end class

?>