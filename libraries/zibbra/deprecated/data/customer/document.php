<?php

class ZCustomerDocument extends ZObject {
	/*
	public function getDocumentId() {
		
		$prop = $this->getDocumentType()."id";
		
		return isset($this->$prop) ? $this->$prop : false;
		
	} // end function
	
	public function getDocumentType() {
		
		return strtolower(str_replace("ZibbraObjectCustomer","",get_class($this)));
		
	} // end function
*/
	protected static function downloadByType($doctype,$document_nr) {

		if(!ZCustomer::isAuthenticated()) {

			return false;

		} // end if
		
		$oCustomerUser = unserialize($_SESSION['zibbra']['user']);
		
		$params = array(
			"customerid"=>(int) $oCustomerUser->getCustomerid(),
			"username"=>$oCustomerUser->getUsername(),
			"password"=>$oCustomerUser->getPassword(),
			"document_nr"=>$document_nr
		);
		
		$response = parent::call($doctype,"download",$params);

		return base64_decode($response);

	} // end function
	
} // end class

?>