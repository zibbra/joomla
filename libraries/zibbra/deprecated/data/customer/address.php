<?php

class ZCustomerAddress extends ZObject {
	
	private $street;
	private $streetnr;
	private $box;
	private $zipcode;
	private $city;
	private $province;
	private $countrycode;
	
	const TYPE_BILLING = "billing";
	const TYPE_SHIPPING = "delivery";
	
	public function getStreet() {
		
		return $this->street;
		
	} // end function
	
	public function setStreet($street) {
		
		$this->street = $street;
		
	} // end function
	
	public function getStreetnr() {
		
		return $this->streetnr;
		
	} // end function
	
	public function setStreetnr($streetnr) {
		
		$this->streetnr = $streetnr;
		
	} // end function
	
	public function getBox() {
		
		return $this->box;
		
	} // end function
	
	public function setBox($box) {
		
		$this->box = $box;
		
	} // end function
	
	public function getZipcode() {
		
		return $this->zipcode;
		
	} // end function
	
	public function setZipcode($zipcode) {
		
		$this->zipcode = $zipcode;
		
	} // end function
	
	public function getCity() {
		
		return $this->city;
		
	} // end function
	
	public function setCity($city) {
		
		$this->city = $city;
		
	} // end function
	
	public function getProvince() {
		
		return $this->province;
		
	} // end function
	
	public function setProvince($province) {
		
		$this->province = $province;
		
	} // end function
	
	public function getCountrycode() {
		
		return $this->countrycode;
		
	} // end function
	
	public function setCountrycode($countrycode) {
		
		$this->countrycode = $countrycode;
		
	} // end function
	
	public function toArray(&$arr,$prefix) {
		
		if(!empty($this->street)) $arr[$prefix.'_street'] = $this->street;
		if(!empty($this->streetnr)) $arr[$prefix.'_streetnr'] = $this->streetnr;
		if(!empty($this->box)) $arr[$prefix.'_box'] = $this->box;
		if(!empty($this->zipcode)) $arr[$prefix.'_zipcode'] = $this->zipcode;
		if(!empty($this->city)) $arr[$prefix.'_city'] = $this->city;
		if(!empty($this->province)) $arr[$prefix.'_province'] = $this->province;
		if(!empty($this->countrycode)) $arr[$prefix.'_countrycode'] = $this->countrycode;
		
	} // end function
	
	/**
	 * Parses data into an object
	 * 
	 * @param array $data
	 * @return ZCustomerAddress
	 */
	public static function parse($type,$data,$prefix=true) {
		
		$oCustomerAddress = new ZCustomerAddress();
		
		$oCustomerAddress->street = $data[$prefix ? $type.'_street' : 'street'];
		$oCustomerAddress->streetnr = $data[$prefix ? $type.'_streetnr' : 'streetnr'];
		$oCustomerAddress->box = $data[$prefix ? $type.'_box' : 'box'];
		$oCustomerAddress->zipcode = $data[$prefix ? $type.'_zipcode' : 'zipcode'];
		$oCustomerAddress->city = $data[$prefix ? $type.'_city' : 'city'];
		$oCustomerAddress->province = $data[$prefix ? $type.'_province' : 'province'];
		$oCustomerAddress->countrycode = $data[$prefix ? $type.'_countrycode' : 'countrycode'];
		
		return $oCustomerAddress;
		
	} // end function
	
} // end class

?>