<?php

class ZCustomerOrderItem extends ZObject {

	protected $orderitemid;
	protected $productid;
	protected $customerproductid;
	protected $name;
	protected $description;
	protected $quantity;
	protected $amount;
	protected $vat;
	protected $enterprisepricelevelid;
	protected $enterprisepriceperiodid;
	protected $price_override;
	protected $discount;
	protected $displayorder;
	
	public static function parse($data) {
		
		$oItem = new ZCustomerOrderItem();
		
		parent::parse($oItem,$data);
		
		return $oItem;
		
	} // end function

} // end class

?>
