<?php

jimport("zibbra.data.customer.invoice.item");

class ZCustomerInvoice extends ZCustomerDocument {

	protected $invoiceid;
	protected $number;
	protected $type;
	protected $amount;
	protected $vat;
	protected $paid;
	protected $booked;
	protected $timestamp_insert;
	protected $timestamp_expiry;

	private $arrItems;
	
	public function getNumber() {
		
		return $this->number;
		
	} // end function
	
	public function isPaid() {
		
		return $this->paid;
		
	} // end function

	public function getInvoiceDate() {

		return date("d/m/Y",strtotime($this->timestamp_insert));

	} // end function

	public function getExpiryDate() {

		return date("d/m/Y",strtotime($this->timestamp_expiry));

	} // end function

	public function getAmount() {

		return ($this->amount + $this->vat);

	} // end function

	public function addItem(ZCustomerInvoiceItem $oItem) {

		$this->arrItems[] = $oItem;

	} // end function

	public function getItems() {

		return $this->arrItems;

	} // end function
	
	public static function parse($data) {
		
		$oInvoice = new ZCustomerInvoice();
		
		parent::parse($oInvoice,$data);
		
		if(isset($data['items'])) {
		
			foreach($data['items'] as $item) {
			
				$oItem = ZCustomerInvoiceItem::parse($item);
					
				$oInvoice->addItem($oItem);
			
			} // end foreach
		
		} // end if
		
		return $oInvoice;
		
	} // end function

	public static function download($document_nr) {

		return parent::downloadByType("invoice",$document_nr);

	} // end function

} // end class

?>
