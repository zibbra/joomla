<?php

class ZCustomerInvoiceItem extends ZObject {

	protected $invoiceitemid;
	protected $productid;
	protected $description;
	protected $quantity;
	protected $amount;
	protected $vat;
	
	public static function parse($data) {
		
		$oItem = new ZCustomerInvoiceItem();
		
		parent::parse($oItem,$data);
		
		return $oItem;
		
	} // end function

} // end class

?>
