<?php

class ZCustomerContact extends ZObject {
	
	protected $customercontactid;
	protected $title;
	protected $firstname;
	protected $lastname;
	protected $gender;
	protected $phone;
	protected $mobile;
	protected $email;
	protected $primary;
	
	public function getContactid() {
		
		return $this->customercontactid;
		
	} // end function
	
	public function getTitle() {
		
		return $this->title;
		
	} // end function
	
	public function setTitle($title) {
		
		$this->title = $title;
		
	} // end function
	
	public function getFirstname() {
		
		return $this->firstname;
		
	} // end function
	
	public function setFirstname($firstname) {
		
		$this->firstname = $firstname;
		
	} // end function
	
	public function getLastname() {
		
		return $this->lastname;
		
	} // end function
	
	public function setLastname($lastname) {
		
		$this->lastname = $lastname;
		
	} // end function
	
	public function getGender() {
		
		return $this->gender;
		
	} // end function
	
	public function setGender($gender) {
		
		$this->gender = $gender;
		
	} // end function
	
	public function getPhone() {
		
		return $this->phone;
		
	} // end function
	
	public function setPhone($phone) {
		
		$this->phone = $phone;
		
	} // end function
	
	public function getMobile() {
		
		return $this->mobile;
		
	} // end function
	
	public function setMobile($mobile) {
		
		$this->mobile = $mobile;
		
	} // end function
	
	public function getEmail() {
		
		return $this->email;
		
	} // end function
	
	public function setEmail($email) {
		
		$this->email = $email;
		
	} // end function
	
	public function isPrimary() {
		
		return $this->primary;
		
	} // end function
	
	public function toArray(&$arr) {
		
		if(!empty($this->title)) $arr['contact_title'] = $this->title;
		if(!empty($this->firstname)) $arr['contact_firstname'] = $this->firstname;
		if(!empty($this->lastname)) $arr['contact_lastname'] = $this->lastname;
		if(!empty($this->gender)) $arr['contact_gender'] = $this->gender;
		if(!empty($this->email)) $arr['contact_email'] = $this->email;
		if(!empty($this->phone)) $arr['contact_phone'] = $this->phone;
		if(!empty($this->mobile)) $arr['contact_mobile'] = $this->mobile;
		
	} // end function
	
	public static function parse($data) {
		
		$oCustomerContact = new ZCustomerContact();
		
		parent::parse($oCustomerContact,$data);
		
		return $oCustomerContact;
		
	} // end function

} // end class

?>
