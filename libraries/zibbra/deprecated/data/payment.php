<?php

jimport("zibbra.data.payment.ogone");
jimport("zibbra.data.payment.paypal");
jimport("zibbra.data.payment.transcription");
jimport("zibbra.data.payment.icepay_basic");
jimport("zibbra.data.payment.icepay_advanced");

class ZPayment extends ZObject {
	
	protected $type;
	protected $price;
	protected $amount;
	protected $fixed_cost;
	protected $percentual_cost;
	protected $lower_limit;
	protected $orderid;
	protected $sandbox;
	protected $order;
	
	private static $callbackHost = null;
	
	public function getType() {
		
		return $this->type;
		
	} // end function
	
	public function getPrice() {
		
		return $this->price;
		
	} // end function
	
	public function getAmount() {
		
		return $this->amount;
		
	} // end function
	
	public function setAmount($amount) {
		
		$this->amount = $amount;
		
	} // end function
	
	public function getOrderid() {
		
		return $this->orderid;
		
	} // end function
	
	public function setOrderid($orderid) {
		
		$this->orderid = $orderid;
		
	} // end function
	
	public function getOrder() {
		
		if(!$this->order instanceof ZOrder) {
			
			$this->order = ZOrder::load($this->orderid);
			
		} // end if
		
		return $this->order;
		
	} // end function
	
	public function inSandboxMode() {
		
		return $this->sandbox=="Y";
		
	} // end function
	
	public static function parse($type,$data) {
		
		$class = "ZPayment".str_replace(" ","",ucwords(str_replace("_"," ",$type)));
		
		$oPayment = new $class();
		
		parent::parse($oPayment,$data,($type=="icepay_basic" ? "basic" : ($type=="icepay_advanced" ? "advanced" : false)));
		
		$oPayment->type = $type;
		
		return $oPayment;
		
	} // end function
	
	public static function load() {
		
		$arrAdapters = array();
		
		$params = array(
			"sessionid"=>session_id()
		);
		
		$response = parent::call("cart","getPaymentAdapters",$params);
		
		if($response instanceof ZApiError) return false;
		
		foreach($response as $type=>$data) {
			
			$arrAdapters[] = ZPayment::parse($type,$data);
			
		} // end foreach
		
		return $arrAdapters;
		
	} // end function
	
	public static function get($type) {
		
		foreach(self::load() as $adapter) {
			
			if($adapter->getType()==$type) return $adapter;
			
		} // end foreach
		
		return false;
		
	} // end function
	
	public static function getCallbackHost() {
		
		if(self::$callbackHost===null) {
		
			$s = empty($_SERVER["HTTPS"]) ? "" : ($_SERVER["HTTPS"] == "on") ? "s" : "";
			$proto = explode("/",$_SERVER["SERVER_PROTOCOL"]);
			$protocol = strtolower($proto[0]).$s;
			$port = ($_SERVER["SERVER_PORT"] == "80") ? "" : (":".$_SERVER["SERVER_PORT"]);	
			$getVD = explode("/", $_SERVER['PHP_SELF']);
			
			if(count($getVD)<=2) {
				
				$virtualdir = "";
				
			}else{
				
				$virtualdir = "/".$getVD[1];
				
			} // end if	
		
			self::$callbackHost = $protocol."://".$_SERVER['SERVER_NAME'].$port.$virtualdir;
		
		} // end if
		
		return self::$callbackHost;
				
	} // end function
	
} // end class