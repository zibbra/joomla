<?php

class ZImage extends ZObject {
	
	protected $id;
	protected $path;
	
	protected $max_size = 150;
	
	public function getID() {
		
		return $this->id;
		
	} // end function
	
	public function getPath($max_size=null) {
		
		$max_size = ($max_size!=null ? $max_size : $this->max_size);
		
		$path = $this->path.(substr_count($this->path,"?") ? "&" : "?")."width=".$max_size."&height=".$max_size;

		return $path;
		
	} // end function
	
	public function __toString() {
		
		return $this->getPath();
		
	} // end function
	
	protected static function parseItem($oImage,$item) {
			
		$oImage->id = $item['id'];		
		$oImage->path = $item['path'];
		
		return $oImage;
		
	} // end function
	
} // end class

?>
