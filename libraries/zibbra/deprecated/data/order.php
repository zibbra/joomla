<?php

jimport("zibbra.data.order.item");

class ZOrder extends ZObject {
	
	protected $orderid;
	protected $customerid;
	protected $customer;
	protected $number;
	protected $amount_incl;
	protected $vat;
	protected $status;
	protected $weight;
	private static $instance = null;
	
	private $arrItems = array();
	
	public function getOrderid() {
		
		return $this->orderid;
		
	} // end function
	
	public function getCustomerid() {
		
		return $this->customerid;
		
	} // end function
	
	public function getCustomer() {
		
		return $this->customer;
		
	} // end function
	
	public function getNumber() {
		
		return $this->number;
		
	} // end function
	
	public function getAmountIncl() {
		
		return $this->amount_incl;
		
	} // end function
	
	public function getVat() {
		
		return $this->vat;
		
	} // end function
	
	public function getStatus() {
		
		return $this->status;
		
	} // end function
	
	public function addItem(ZOrderItem $oItem) {
		
		$this->arrItems[] = $oItem;
		
	} // end function
	
	public function getValutaSymbol() {
		
		return "&euro;";
		
	} // end function
	
	public function isEmpty() {
		
		return count($this->arrItems)==0;
		
	} // end function
	
	public function getItems() {
	
		return $this->arrItems;
	
	} // end function
	
	public function getWeight() {
		
		return $this->weight;
		
	} // end function
	
	public static function cancel($orderid) {
		
		$data = array(
			"orderid"=>(int) $orderid
		);
		
		parent::load("order","cancel",$data);
		
	} // end function
	
	public static function load($orderid) {
		
		$data = parent::load("order","getOrder",array("orderid"=>(int) $orderid));
		
		$oOrder = ZOrder::parseItem($data);
		
		return $oOrder;
		
	} // end function
	
	public static function parseItem($data) {
		
		$oOrder = new ZOrder();
		
		$oOrder->orderid = $data['orderid'];
		$oOrder->customerid = $data['customerid'];
		$oOrder->customer = $data['customer'];
		$oOrder->number = $data['number'];
		$oOrder->amount_incl = $data['amount_incl'];
		$oOrder->vat = $data['vat'];
		$oOrder->status = $data['status'];
		
		if(isset($data['items']) && count($data['items'])>0) {
		
			foreach($data['items'] as $item) {
			
				$oOrder->addItem(ZOrderItem::parse($item));
			
			} // end foreach
		
		} // end if
		
		return $oOrder;
		
	} // end function
	
	public static function addCreditCardPayment($orderid,$amount,$adapter) {
		
		$params = array(
			"orderid"=>(int) $orderid,
			"amount"=>(float) $amount,
			"method"=>"online",
			"adapter"=>$adapter
		);
		
		return parent::load("order","addPayment",$params);
		
	} // end function
	
	public static function getInstance() {
		
		$session = JFactory::getSession();
		
		if(self::$instance===null && $data = $session->get("order",false,"zibbra")) {

			self::$instance = self::parseItem($data);
		
		} // end if
		
		return self::$instance;		
		
	} // end function
	
	public static function sendConfirmation($orderid,$payment_method=null) {
		
		$params = array();
		$params['orderid'] = (int) $orderid;
		
		if(!is_null($payment_method)) {
			
			$params['payment_method'] = $payment_method;
			
		} // end if
		
		return parent::call("order","sendConfirmation",$params);		
		
	} // end function
	
} // end class