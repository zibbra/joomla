<?php

class ZOrderItem extends ZObject {
	
	protected $orderitemid;
	protected $productid;
	protected $description;
	protected $quantity;
	protected $amount;
	protected $vat;
	protected $discount;
	
	public function getOrderitemid() {
		
		return $this->orderitemid;
		
	} // end function
	
	public function getProductid() {
		
		return $this->productid;
		
	} // end function
	
	public function getDescription() {
		
		return $this->description;
		
	} // end function
	
	public function getQuantity() {
		
		return $this->quantity;
		
	} // end function
	
	public function getAmount() {
		
		return $this->amount;
		
	} // end function
	
	public function getVat() {
		
		return $this->vat;
		
	} // end function
	
	public function getTotal() {
		
		return $this->quantity * $this->amount;
		
	} // end function
	
	public function getDiscount() {
		
		return $this->discount;
		
	} // end function
	
	public static function parse($data) {
		
		$oOrderItem = new ZOrderItem();
		
		$oOrderItem->orderitemid = $data['orderitemid'];
		$oOrderItem->productid = $data['productid'];
		$oOrderItem->description = $data['description'];
		$oOrderItem->quantity = $data['quantity'];
		$oOrderItem->amount = $data['amount'];
		$oOrderItem->vat = $data['vat'];
		$oOrderItem->discount = $data['discount'];
		
		return $oOrderItem;
		
	} // end function
	
} // end function

?>