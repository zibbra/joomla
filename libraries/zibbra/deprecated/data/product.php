<?php

jimport("zibbra.data.image");
jimport("zibbra.data.product.image");
jimport("zibbra.data.product.property");
jimport("zibbra.data.product.price");
jimport("zibbra.data.product.addon");
jimport("zibbra.data.product.suggestion");
jimport("zibbra.data.product.bestseller");
jimport("zibbra.data.product.filter");
jimport("zibbra.data.product.variation");

class ZProduct extends ZObject {
	
	private $productid;
	private $code;
	private $name;
	private $short_description;
	private $long_description;
	private $valuta_symbol;
	private $price;
	private $base_price;
	private $vat;
	private $in_stock;
	private $allow_backorder = false;
	private $arrCategories = array();
	private $arrImages = array();
	private $arrProperties = array();
	private $arrSuggestions = array();
	private $arrPrices = array();
	private $arrAddons = array();
	private $arrVariations = array();
	
	private static $arrFilters = array();
	
	public function getProductid() {
		
		return $this->productid;
		
	} // end function
	
	public function getCode() {
		
		return $this->code;
		
	} // end function
	
	public function getName() {
		
		return $this->name;
		
	} // end function
	
	public function getShortDescription() {
		
		return $this->short_description;
		
	} // end function
	
	public function getLongDescription() {
		
		return !empty($this->long_description) ? $this->long_description : $this->short_description;
		
	} // end function
	
	public function getValutaSymbol() {
		
		return $this->valuta_symbol;
		
	} // end function
	
	public function getPrice() {
		
		$price = null;
		
		if(count($this->arrPrices)==1 && $this->arrPrices[0]->getPeriod()==0 && $this->arrPrices[0]->getType()=="normal") {
			
			$price = $this->arrPrices[0]->getPrice() * (1 + $this->arrPrices[0]->getVat());
			
		}else{
			
			$price = $this->price * (1 + $this->vat);
			
		} // end if
		
		return $price;
		
	} // end function
	
	public function getBasePrice() {
		
		$base_price = null;
		
		if(count($this->arrPrices)==1 && $this->arrPrices[0]->getPeriod()==0 && $this->arrPrices[0]->getType()=="normal") {
			
			$base_price = $this->arrPrices[0]->getBasePrice() * (1 + $this->arrPrices[0]->getVat());
			
		}else{
			
			$base_price = $this->base_price * (1 + $this->vat);
			
		} // end if
		
		return $base_price;
		
	} // end function
	
	public function hasDiscount() {
		
		return $this->getPrice() != $this->getBasePrice();
		
	} // end function
	
	public function getURL() {
		
		return JRoute::_("index.php?option=com_zibbra&view=product&id=".$this->getIdString());
		
	} // end function
	
	public function getIdString() {
		
		$name = $this->getName();
		$name = strtolower($name);
		$name = str_replace(" ","-",$name);
		$name = preg_replace("/[^\w\s\.\-]/i","",$name);
		
		return $this->getProductid()."-".urlencode($name);
		
	} // end function
	
	public function hasCategories() {
		
		return count($this->arrCategories)>0;
		
	} // end function
	
	public function getCategories() {
		
		return $this->arrCategories;
		
	} // end function
	
	private function addCategory(ZCategory $oCategory) {
		
		$this->arrCategories[] = $oCategory;
		
	} // end function
	
	public function allowBackorder() {
		
		return $this->allow_backorder;
		
	} // end function
	
	public function isInStock() {
		
		return !empty($this->in_stock) && $this->in_stock > 0;
		
	} // end function
	
	public function getStock() {
		
		return !empty($this->in_stock) ? $this->in_stock : 0;
		
	} // end function
	
	public function hasImages() {
		
		return count($this->arrImages)>0;
		
	} // end function
	
	public function getImageCount() {
		
		return count($this->arrImages);
		
	} // end function
	
	public function getImages() {
		
		return $this->arrImages;
		
	} // end function
	
	public function getFirstImage() {
		
		return $this->hasImages() ? $this->arrImages[0] : false;
		
	} // end function
	
	public function getImageByIndex($index) {
		
		return $this->hasImages() ? (isset($this->arrImages[$index]) ? $this->arrImages[$index] : $this->arrImages[0]) : false;
		
	} // end function
	
	private function addImage(ZImage $oImage) {
		
		$this->arrImages[] = $oImage;
		
	} // end function
	
	public function hasProperties() {
		
		return count($this->arrProperties)>0;
		
	} // end function
	
	public function getProperties() {
		
		return $this->arrProperties;
		
	} // end function
	
	private function addProperty(ZProductProperty $oProductProperty) {
		
		$this->arrProperties[] = $oProductProperty;
		
	} // end function
	
	public function hasSuggestions() {
		
		return count($this->arrSuggestions)>0;
		
	} // end function
	
	public function getSuggestions() {
		
		return $this->arrSuggestions;
		
	} // end function
	
	private function addSuggestion(ZProductSuggestion $oProduct) {
		
		$this->arrSuggestions[] = $oProduct;
		
	} // end function
	
	private function addPrice(ZProductPrice $oProductPrice) {
		
		$this->arrPrices[] = $oProductPrice;
		
	} // end function
	
	public function hasAddons() {
		
		return count($this->arrAddons)>0;
		
	} // end function
	
	public function getAddons() {
		
		return $this->arrAddons;
		
	} // end function
	
	private function addAddons(ZProductAddon $oProduct) {
		
		$this->arrAddons[] = $oProduct;
		
	} // end function
	
	public function hasVariations() {
		
		return count($this->arrVariations)>0;
		
	} // end function
	
	public function getVariations() {
		
		return $this->arrVariations;
		
	} // end function
	
	private function addVariation(ZProductVariation $oVariation) {
		
		$this->arrVariations[$oVariation->getID()] = $oVariation;
		
	} // end function
	
	public function getVariationCombinations($variations) {
		
		$data = array(
			"languagecode"=>ZController::getLanguage(),
			"productid"=>(int) $this->productid,
			"variationoptions"=>$variations
		);
		
		$arrVariations = parent::load("product","getVariationOptions",$data);
		
		return $arrVariations;
		
	} // end function
	
	public static function parseItem($item,$max_size=300,$obj=null) {
		
		$oProduct = $obj!==null ? $obj : new ZProduct();
		$oProduct->productid = $item['productid'];
		
		if(isset($item['code'])) {
		
			$oProduct->code = $item['code'];
		
		} // end if
		
		$oProduct->name = $item['name'];
		
		if(isset($item['valuta_symbol'])) {
		
			$oProduct->valuta_symbol = $item['valuta_symbol'];
			
		} // end if
		
		if(isset($item['short_description'])) {
		
			$oProduct->short_description = $item['short_description'];
			
		} // end if
		
		if(isset($item['long_description'])) {
		
			$oProduct->long_description = $item['long_description'];
			
		} // end if
		
		if(isset($item['in_stock'])) {
		
			$oProduct->in_stock = (int) $item['in_stock'];
			
		} // end if
		
		if(isset($item['allow_backorder'])) {
		
			$oProduct->allow_backorder = $item['allow_backorder']=="Y";
			
		} // end if
		
		if(isset($item['prices']) && count($item['prices'])==1) {
		
			if(isset($item['prices'][0]['price'])) {
			
				$oProduct->price = $item['prices'][0]['price'];
			
			} // end if
			
			if(isset($item['prices'][0]['vat'])) {
				
				$oProduct->vat = $item['prices'][0]['vat'];
			
			} // end if
			
			if(isset($item['prices'][0]['base_price'])) {
			
				$oProduct->base_price = $item['prices'][0]['base_price'];
			
			} // end if
		
		} // end if
		
		if(isset($item['categories']) && count($item['categories'])>0) {
			
			foreach($item['categories'] as $category) {
				
				$oProduct->addCategory(ZCategory::parseItem($category));
				
			} // end foreach
			
		} // end if
		
		if(isset($item['images']) && count($item['images'])>0) {
			
			foreach($item['images'] as $image) {
				
				$oProduct->addImage(ZProductImage::parseItem($image,$max_size));
				
			} // end foreach
			
		} // end if
		
		if(isset($item['properties']) && count($item['properties'])>0) {
			
			foreach($item['properties'] as $property) {
				
				$oProduct->addProperty(ZProductProperty::parseItem($property));
				
			} // end foreach
			
		} // end if
		
		if(isset($item['prices']) && count($item['prices'])>0) {
			
			foreach($item['prices'] as $price) {
				
				$oProduct->addPrice(ZProductPrice::parseItem($price));
				
			} // end foreach
			
		} // end if
		
		if(isset($item['suggestions']) && count($item['suggestions'])>0) {
			
			foreach($item['suggestions'] as $suggestion) {
				
				$oProduct->addSuggestion(ZProductSuggestion::parseItem($suggestion,100));
				
			} // end foreach
			
		} // end if
		
		if(isset($item['variations']) && count($item['variations'])>0) {
			
			foreach($item['variations'] as $variation) {
				
				$oProduct->addVariation(ZProductVariation::parseItem($variation));
				
			} // end foreach
			
		} // end if
				
		return $oProduct;
		
	} // end function
	
	public static function setPriceFilter($min,$max) {
		
		$oFilter = new ZProductFilter(ZProductFilter::TYPE_RANGE,"price");
		$oFilter->setValueMin($min);
		$oFilter->setValueMax($max);
		
		self::$arrFilters[] = $oFilter;
		
	} // end function
	
	public static function setManufacturerFilter($options) {
		
		$oFilter = new ZProductFilter(ZProductFilter::TYPE_LIST,"manufacturer");
		
		$o = array();
		
		foreach($options as $option) $o[] = array("field"=>"manufacturer","condition"=>"=","value"=>$option);
		
		$oFilter->setOptions($o);
		
		self::$arrFilters[] = $oFilter;
		
	} // end function
	
	public static function setStockFilter($options) {
		
		$oFilter = new ZProductFilter(ZProductFilter::TYPE_LIST,"in_stock");
		
		$oFilter->setOptions(array(array("field"=>"in_stock","condition"=>"=","value"=>"Y")));
		
		self::$arrFilters[] = $oFilter;
		
	} // end function
	
	public static function setPropertyFilter($id,$options) {
		
		$oFilter = new ZProductFilter(ZProductFilter::TYPE_LIST,$id);
		
		$o = array();
		
		foreach($options as $option) $o[] = array("field"=>$id,"condition"=>"=","value"=>$option);
		
		$oFilter->setOptions($o);
		
		self::$arrFilters[] = $oFilter;
		
	} // end function
	
	public static function getProduct($productid) {
		
		$data = array(
			"languagecode"=>ZController::getLanguage(),
			"productid"=>(int) $productid,
			"get_properties"=>true,
			"get_prices"=>true,
			"get_suggestions"=>2
		);
		
		$item = parent::load("product","getProduct",$data);		
		
		$oProduct = self::parseItem($item);
		/*
		echo "<pre>";
		var_dump($oProduct);
		exit;
		*/
		return $oProduct;
		
	} // end function
	
	public static function getFilter($field) {
		
		foreach(self::$arrFilters as $oFilter) {
			
			if($oFilter->getField()==$field) return $oFilter;			
			
		} // end foreach
		
		return false;
		
	} // end function
	
	public static function getProducts(&$pagination,$categoryid=null,$get_prices=false,$sort_type=null,$sort_dir=null) {
		
		$data = array(
			"languagecode"=>ZController::getLanguage(),
			"get_prices"=>$get_prices,
			"limit"=>isset($pagination['limit']) ? (int) $pagination['limit'] : 10,
			"page"=>isset($pagination['page']) ? (int) $pagination['page'] : 1
		);
		
		if($categoryid!==null) $data['categoryid'] = (int) $categoryid;
		
		if($sort_type!==null && $sort_dir!==null) {
			
			$data['sort'] = array();
			$data['sort'][] = array(
				"key"=>$sort_type,
				"direction"=>strtoupper($sort_dir)
			);
			
		} // end if
		
		if(($oFilter = self::getFilter("price"))!==false) $data['price_filters'] = $oFilter->getFilter();
		
		$data['property_filters'] = array();
		$data['filters'] = array();
				
		/*
		echo "<pre>";
		var_dump(self::$arrFilters);
		exit;		
		*/
		
		foreach(self::$arrFilters as $oFilter) {
			
			if($oFilter->getType()==ZProductFilter::TYPE_LIST) {
			
				if($oFilter->getField()=="manufacturer") {
				
					$data['filters'] = array_merge($data['filters'],$oFilter->getFilter("manufacturerid"));
					
				}elseif($oFilter->getField()=="in_stock") {
				
					$data['filters'] = array_merge($data['filters'],$oFilter->getFilter("in_stock"));
					
				}else{
				
					$data['property_filters'] = array_merge($data['property_filters'],$oFilter->getFilter());
			
				} // end if
			
			} // end if
			
		} // end foreach
		
		if(count($data['property_filters'])==0) unset($data['property_filters']);
		if(count($data['filters'])==0) unset($data['filters']);
				
		/*
		echo "<pre>";
		var_dump($data['filters']);
		exit;		
		*/
		
		$items = parent::load("product","getProducts",$data);
		
		/*
		echo "<pre>";
		var_dump($items);
		exit;		
		*/
		
		$arrProducts = array();
		
		$pagination = $items['pagination'];
		$pagination['page'] = $data['page'];
		
		self::$arrFilters = array();
		
		if(isset($items['filters']) && is_array($items['filters'])) {
			
			/*
			echo "<pre>";
			var_dump($items['filters']);
			exit;
			*/
			
			foreach($items['filters'] as $field=>$item) {
				
				$oFilter = new ZProductFilter($item['type'],$field,$item['name']);
				
				$oFilter->setName($item['name']);
				
				if($oFilter->getType()==ZProductFilter::TYPE_RANGE) {
					
					$oFilter->setMin($item['min']);
					$oFilter->setMax($item['max']);
					
				} // end if
					
				if($oFilter->getType()==ZProductFilter::TYPE_LIST) {
					
					if(isset($item['unit'])) $oFilter->setUnit($item['unit']);
					$oFilter->setQuantity($item['quantity']);
					$oFilter->setOptions($item['options']);
					
				} // end if
				
				self::$arrFilters[] = $oFilter;
				
			} // end foreach
		
		} // end if
		
		if(isset($items['products']) && is_array($items['products'])) {
		
			foreach($items['products'] as $item) {
				
				$arrProducts[] = self::parseItem($item,150);
				
			} // end foreach
		
		} // end if
		
		/*
		echo "<pre>";
		var_dump($arrProducts);
		exit;
		*/
		
		return $arrProducts;
		
	} // end function
	
	public static function search($text,$limit=10) {
		
		$arrProducts = array();
		
		if(!empty($text)) {
		
			$params = array(
				"languagecode"=>ZController::getLanguage(),
				"limit"=>$limit,
				"text"=>$text
			);
		
			$response = parent::load("product","search",$params);
			
			if($response instanceof ZApiError) return false;

			if(is_array($response)) {
			
				foreach($response as $data) {
			
					$arrProducts[] = self::parseItem($data,100);
			
				} // end foreach
			
			} // end if
		
		} // end if
		
		return $arrProducts;
		
	} // end function
	
	public static function getBestsellers($limit) {
		
		$data = array(
			"languagecode"=>ZController::getLanguage(),
			"limit"=>(int) $limit
		);
		
		$items = parent::load("product","getBestsellers",$data);
		
		$arrProducts = array();
		
		if(is_array($items)) {
		
			foreach($items as $item) {
				
				$arrProducts[] = self::parseItem($item,100);
				
			} // end foreach
		
		} // end if
		
		return $arrProducts;
		
	} // end function
	
	public static function getFilters() {
		
		return self::$arrFilters;
		
	} // end function
	
} // end class

?>