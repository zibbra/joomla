<?php

class ZShipping extends ZObject {
	
	protected $enterpriseshippingmethodid;
	protected $name;
	protected $description;
	protected $price;
	protected $vat;
	protected $shippingadapter;
	
	private static $arrMethods = null;
	
	public function getEnterpriseshippingmethodid() {
		
		return $this->enterpriseshippingmethodid;
		
	} // end function	
	
	public function getName() {
	
		return $this->name;
	
	} // end function
	
	public function getDescription() {
	
		return $this->description;
	
	} // end function
	
	public function getPrice() {
	
		return $this->price;
	
	} // end function
	
	public function getVat() {
	
		return $this->vat;
	
	} // end function
	
	public function getPriceVatIncl() {
	
		return $this->price * (1 + $this->vat);
	
	} // end function
	
	public function getShippingAdapter() {
		
		if(!$this->shippingadapter) {
			
			return false;
			
		} // end if
		
		return $this->shippingadapter;
		
	} // end function
	
	public function setShippingAdapter(ZShippingAdapter $oShippingAdapter) {
		
		$this->shippingadapter = $oShippingAdapter;
		
	} // end function
	
	public static function parse($data) {
		
		$oShipping = new ZShipping();
		
		parent::parse($oShipping,$data);
		
		$oShipping->setShippingAdapter(ZShippingAdapter::parse($data['adapter']));
		
		return $oShipping;
		
	} // end function
	
	public static function load() {
		
		if(self::$arrMethods===null) {
		
			$params = array(
				"languagecode"=>ZController::getLanguage(),
				"sessionid"=>session_id()
			);
			
			$response = parent::call("cart","getShippingMethods",$params);
			
			if($response instanceof ZApiError) return false;
			
			foreach($response as $data) {
				
				$oMethod = ZShipping::parse($data);

				self::$arrMethods[] = $oMethod;
				
			} // end foreach
		
		} // end if
		
		return self::$arrMethods;
		
	} // end function
	
	public static function get($id) {
		
		foreach(self::load() as $method) {
			
			if($method->getEnterpriseshippingmethodid()==$id) {
				
				return $method;
				
			} // end if
			
		} // end foreach
		
		return false;
		
	} // end function
	
} // end class

?>
