<?php

jimport("zibbra.data.image");
jimport("zibbra.data.category.image");

class ZCategory extends ZObject {
	
	private $categoryid;
	private $name;
	private $description;
	private $arrChildren = array();	
	private $arrImages = array();
	private $parent;
	
	public function getCategoryid() {
		
		return $this->categoryid;
		
	} // end function
	
	public function getName() {
		
		return $this->name;
		
	} // end function
	
	public function hasDescription() {
		
		return !empty($this->description);
		
	} // end function
	
	public function getDescription() {
		
		return $this->description;
		
	} // end function
	
	public function hasChildren() {
		
		return count($this->arrChildren)>0;
		
	} // end function
	
	public function getChildren() {
		
		return $this->arrChildren;
		
	} // end function
	
	public function hasParent() {
		
		return $this->parent!==null;
		
	} // end function
	
	public function getParent() {
		
		return $this->parent;
		
	} // end function
	
	public function getIdString() {
		
		$name = $this->getName();
		$name = strtolower($name);
		$name = str_replace(" ","-",$name);
		
		return $this->getCategoryid()."-".urlencode($name);
		
	} // end function
	
	private function addChild(ZCategory $oCategory) {
		
		$oCategory->parent = $this;
		
		$this->arrChildren[] = $oCategory;
		
	} // end function
	
	public function hasImages() {
		
		return count($this->arrImages)>0;
		
	} // end function
	
	public function getImageCount() {
		
		return count($this->arrImages);
		
	} // end function
	
	public function getImages() {
		
		return $this->arrImages;
		
	} // end function
	
	public function getFirstImage() {
		
		return $this->hasImages() ? $this->arrImages[0] : false;
		
	} // end function
	
	private function addImage(ZImage $oImage) {
		
		$this->arrImages[] = $oImage;
		
	} // end function
	
	public static function parseItem($item) {
			
		$oCategory = new ZCategory();
		$oCategory->categoryid = $item['categoryid'];
		$oCategory->name = $item['name'];
		
		if(isset($item['description']))	$oCategory->description = $item['description'];
		
		if(isset($item['images']) && is_array($item['images']) && count($item['images'])>0) {
			
			foreach($item['images'] as $image) {
				
				$oCategory->addImage(ZCategoryImage::parseItem($image,200));
				
			} // end foreach
			
		} // end if
		
		if(isset($item['children']) && count($item['children'])>0) {
		
			foreach($item['children'] as $subitem) {
				
				$oChild = self::parseItem($subitem);
				$oChild->parent = $oCategory;
			
				$oCategory->addChild($oChild);
				
			} // end foreach
			
		} // end if
		
		return $oCategory;
		
	} // end function
	
	public static function getById($categoryid,$get_images=false) {
		
		$categories = self::getCategories(true,$get_images);
		
		foreach($categories as $oCategory) {
			
			if($oCategory->getCategoryid()==$categoryid) return $oCategory;
			
		}  // end foreach
		
		return false;
		
	} // end function
	
	public static function getCategories($get_whole_tree=true,$get_images=false) {
		
		$data = array(
			"languagecode"=>ZController::getLanguage(),
			"get_whole_tree"=>$get_whole_tree,
			"get_images"=>$get_images
		);
		
		$items = parent::load("product","getCategories",$data);
		
		$arrCategories = array();
		
		foreach($items as $item) {
			
			self::parseCategory($arrCategories,$item);
			
		} // end foreach
		
		return $arrCategories;
		
	} // end function
	
	private static function parseCategory(&$arrCategories,$category,$parent=null) {
			
		$oCategory = self::parseItem($category);
		
		if($parent!==null) {
			
			$oCategory->parent = $parent;
			
		} // end if
		
		$arrCategories[] = $oCategory;
		
		if(count($category['children'])>0) {
		
			foreach($category['children'] as $item) {
				
				self::parseCategory($arrCategories,$item,$oCategory);
				
			} // end foreach
			
		} // end if
		
	} // end function
	
} // end class

?>