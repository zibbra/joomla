<?php

abstract class ZShippingAdapter extends ZObject {
	
	protected $enterpriseshippingadapterid;
	protected $name;
	protected $shippingadapterid;
	protected $active;
	protected $settings;
	protected $shipping_method;
	
	abstract public function dispatch(ZCustomer $oCustomer, ZOrder $oOrder);

	public function getEnterpriseshippingadapterid() {
		
		return $this->enterpriseshippingadapterid;
		
	} // end function
	
	public function getName() {
		
		return $this->name;
		
	} // end function
	
	public function getShippingadapterid() {
		
		return $this->shippingadapterid;
		
	} // end function
	
	public function getActive() {
		
		return $this->active;
		
	} // end function
	
	public function getSettings() {
		
		return $this->settings;
		
	} // end function
	
	public function setShippingMethod(ZShippingMethod $oMethod) {
		
		$this->shipping_method = $oMethod;
		
	} // end function
	
	public function getShippingMethod() {
		
		return $this->shipping_method;
		
	} // end function
	
	public static function parse($data) {
		
		switch($data['name']) {
			
			case "bpost": {
				
				$oShippingAdapter = new ZShippingAdapterBpost();
			
			};break;
			
			default: throw new ZException("shipping adapter '".$data['name']."' is not supported");
						
		} // end switch
		
		parent::parse($oShippingAdapter,$data);
		
		return $oShippingAdapter;
		
	} // end function
	
} // end class

?>