<?php

jimport("zibbra.data.shipping.adapter");

class ZShippingAdapterBpost extends ZShippingAdapter {

	const BPOST_CONFIRMURL = "index.php?option=com_zibbra&task=bpost.onConfirm";
	const BPOST_CANCELURL = "index.php?option=com_zibbra&task=bpost.onCancel";
	const BPOST_ERRORURL = "index.php?option=com_zibbra&task=bpost.onError";	
	
	/**
	 * 
	 * 
	 * 
	 * @TODO need to pass url's
	 * @see ZShippingAdapter::dispatch()
	 */
	public function dispatch(ZCustomer $oCustomer, ZOrder $oOrder) {
				
		if(!$oContact = $oCustomer->getPrimaryContact()) {
			
			throw new ZException("Customer does not have a valid primary contact");
			
		} // end if
		
		if(!$oShippingAddress = $oCustomer->getShippingAddress()) {
			
			throw new ZException("Customer doesn't have a valid shipping address");
			
		} // end if
		
		$oCompany = $oCustomer->getCompany();
		
		$oShippingMethod = $this->getShippingMethod();
		
		$config = array(
			"accountId"=>$this->settings['accountid'],
			"action"=>"START",
			"orderReference"=>$oOrder->getNumber(),
			"lang"=>$this->getLanguage(),
			"orderTotalPrice"=>str_replace(".", "", number_format($oOrder->getAmountIncl(),2)),
			"customerFirstName"=>$oContact->getFirstname(),
			"customerLastName"=>$oContact->getLastname(),
			"customerCompany"=>$oCompany ? $oCompany->getName() : "",
			"customerStreet"=>$oShippingAddress->getStreet(),
			"customerStreetNumber"=>$oShippingAddress->getStreetnr(),
			"customerBox"=>$oShippingAddress->getBox(),
			"customerCity"=>$oShippingAddress->getCity(),
			"customerPostalCode"=>$oShippingAddress->getZipcode(),
			"customerCountry"=>$oShippingAddress->getCountrycode(),
			"customerEmail"=>$oContact->getEmail(),
			"customerPhoneNumber"=>$oContact->getPhone(),
			"orderWeight"=>(int) $oOrder->getWeight(),
			"confirmUrl"=>JRoute::_(JURI::base().self::BPOST_CONFIRMURL,false),
			"cancelUrl"=>JRoute::_(JURI::base().self::BPOST_CANCELURL,false),
			"errorUrl"=>JRoute::_(JURI::base().self::BPOST_ERRORURL,false),
			"orderLine"=>array(),
			"deliveryMethodOverrides"=>array()				
		);
		
		// Override delivery methods
		
		foreach($this->settings['methods'] as $method=>$visible) {
			
			if(!$oShippingMethod->isInternational() && substr($method,0,3)==="int") {
				
				continue;
				
			} // end if
			
			$method = self::back2front($method);
			
			if($visible==="Y") {
				
				$config['deliveryMethodOverrides'][self::overrideOrder($method)] = implode("|",array($method,"VISIBLE",((int) $oShippingMethod->getPrice())*100));
				
			}else{
				
				$config['deliveryMethodOverrides'][self::overrideOrder($method)] = implode("|",array($method,"INVISIBLE"));
				
			} // end if
			
		} // end foreach
		
		ksort($config['deliveryMethodOverrides']);
		
		// Order lines
		
		foreach($oOrder->getItems() as $oItem) {
			
			if(is_numeric($oItem->getProductid())) {
			
				$config['orderLine'][] = implode("|",array($oItem->getDescription(),(int) $oItem->getQuantity()));
				
			} // end if
			
		} // end foreach
		
		$config['checksum'] = $this->checksum($config,$this->settings['passphrase']);
		
		$session = JFactory::getSession();
		$session->set("shipping.type","bpost","zibbra");
		$session->set("shipping.bpost",$config,"zibbra");
		$session->set("shipping.settings",$this->settings,"zibbra");
		
		header("Location: ".JRoute::_("index.php?option=com_zibbra&view=shipping&layout=bpost",false));
		exit;
		
	} // end function
	
	private function checksum($config, $passphrase) {
		
		$arr = array(
			"accountId"=>$config['accountId'],
			"action"=>$config['action'],
			"customerCountry"=>$config['customerCountry'],
			"deliveryMethodOverrides"=>$config['deliveryMethodOverrides'],
			"orderReference"=>$config['orderReference'],
			"orderWeight"=>$config['orderWeight']
		);

		$query = "";
		 
		foreach($arr as $key=>$value) {
		
			if(is_array($value)) {
			 	
				foreach($value as $subvalue) {
		
					$query .= "&".$key."=".$subvalue;
		
				} // end foreach
			 	
			}else{
		
				$query .= "&".$key."=".$value;
		
			} // end if
		
		} // end foreach
	
		$query = utf8_encode(substr($query,1)."&".$passphrase);
		
		return hash("sha256",$query);		
		
	} // end function
	
	private function getLanguage() {
		
		list($lang,$country) = explode("-",ZController::getLanguage());
		$lang = strtoupper($lang);

		if(!in_array($lang,array("EN","NL","FR","DE"))) {
			
			return "EN";
			
		} // end if
		
		return $lang;
		
	} // end function
	
	public static function parse($data) {
			
		$oBPost = new ZShippingAdapterBpost();
	
		ZObject::parse($oBPost,$data);
	
		return $oBPost;
	
	} // end function	
	
	private static function back2front($key) {
		
		$keys = array(
			"atHome"=>"Regular",
			"atShop"=>"Pugo",
			"at24-7"=>"Parcels depot",
			"intExpress"=>"bpack EXPRESS",
			"intBusiness"=>"bpack BUSINESS"
		);
		
		if(!array_key_exists($key,$keys)) {
			
			throw new ZException("key '".$key."' not defined in BPost translation array");
			
		} // end if
		
		return $keys[$key];
		
	} // end function
	
	private static function overrideOrder($key) {
		
		$keys = array(
			"Regular"=>4,
			"Pugo"=>3,
			"Parcels depot"=>2,
			"bpack EXPRESS"=>0,
			"bpack BUSINESS"=>1	
		);
		
		if(!array_key_exists($key,$keys)) {
				
			throw new ZException("key '".$key."' not defined in BPost order array");
				
		} // end if

		return $keys[$key];
		
	} // end function
	
	public static function confirm(ZOrder $oOrder, $enterpriseshippingmethodid, $price, $data) {
		
		$params = array(
			"orderid"=>(int) $oOrder->getOrderid(),
			"enterpriseshippingmethodid"=>(int) $enterpriseshippingmethodid,
			"price"=>(float) $price,
			"languagecode"=>ZController::getLanguage(),
			"remark"=>"",
			"settings"=>$data
		);
		
		$response = parent::call("order", "setShippingMethod", $params);
	
	} // end function	
	
} // end function

?>