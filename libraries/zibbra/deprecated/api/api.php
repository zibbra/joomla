<?php
/**
 * @package Zibbra
 * @subpackage API
 * @author Zibbra Technologies
 */

defined("JPATH_PLATFORM") or die;

/**
 * Zibbra API
 * 
 * @package Zibbra
 * @subpackage API
 * @author Zibbra Technologies
 */
class ZApi {
	
	private $api_client_id;
	private $api_client_secret;
	private $api_uri;
	
	public function __construct($api_client_id, $api_client_secret ,$api_uri, $api_key = null) {
	
		$this->api_client_id = $api_client_id;
		$this->api_client_secret = $api_client_secret;
		$this->api_uri = $api_uri;
		$this->api_key = $api_key;
		
	} // end function
	
	private function getToken() {
		
		$host = "https://oauth.zibbra.com";
		$path = "/token";
		$params = JComponentHelper::getParams("com_zibbra");
		$access_token = $params->get("api_client_token",false);
		$expiry = $params->get("api_client_expiry",false);
		
		if($access_token && $expiry>time()) {

			return $access_token;
			
		} // end if
		
		$post = array(
			"grant_type"=>"client_credentials",
			"client_id"=>$this->api_client_id,
			"client_secret"=>$this->api_client_secret
		);
		
		// REQUEST TOKEN
		
		$headers = array();
		$headers[] = "Host: ".$host;
		$headers[] = "Content-Type: application/x-www-form-urlencoded";
		$headers[] = "Content-Length: ".strlen(http_build_query($post));
		$headers[] = "\r\n";
		
		// Initialize curl
		
		$curl = curl_init();
		
		// Set various options
		
		curl_setopt($curl,CURLOPT_URL,$host.$path);
		curl_setopt($curl,CURLOPT_RETURNTRANSFER,1);
		curl_setopt($curl,CURLOPT_TIMEOUT,30);
		curl_setopt($curl,CURLOPT_POSTFIELDS,$post);
		
		// Execute request
		
		if(($response = curl_exec($curl))===false) {

			return false;
			
		} // end if
		
		if(($response = json_decode($response))===false) {
		
			return false;
		
		} // end if
		
		if(!isset($response->access_token)) {
			
			return false;
			
		} // end if
		
		$diff = $response->time-time();
		$params->set("api_client_token",$response->access_token);
		$params->set("api_client_expiry",time()+$response->expires_in-5+$diff); // Expire 5s earlier to make sure that we refresh the token before it expires in zibbra
		
		$db = &JFactory::getDBO();
		$db->setQuery("UPDATE #__extensions SET params=".$db->quote((string) $params)." WHERE `type`='component' AND `element`='com_zibbra'");
		$db->query();
				
		return $params->get("api_client_token",false);
		
	} // end function
	
	public function call($module,$method,$data=array()) {
		
		$uri = $this->api_uri."/".$module;
		
		$data = array_merge(array("access_token"=>$this->getToken()),$data);

		// Using the XML-RPC extension to format the XML package

		$request = xmlrpc_encode_request($method,$data);
		$curl = curl_init($uri);

		// Using the cURL extension to send it off,  first creating a custom header block

		$headers = array();
		array_push($headers,"Content-Type: text/xml");
		array_push($headers,"Content-Length: ".strlen($request));
		array_push($headers,"\r\n");

		// URL to post to

		curl_setopt($curl,CURLOPT_URL,$uri);

		// Setting options for a secure SSL based xmlrpc server

		curl_setopt($curl,CURLOPT_CUSTOMREQUEST,"POST");
		curl_setopt($curl,CURLOPT_RETURNTRANSFER,1);
		curl_setopt($curl,CURLOPT_HTTPHEADER,$headers);
		curl_setopt($curl,CURLOPT_POSTFIELDS,$request);

		// Finally run

		$response = curl_exec($curl);

		// Close the cURL connection

		curl_close($curl);

		// Decoding the response to be displayed
		
		//file_put_contents("/tmp/xmlrpc_response",print_r($response,true));
		
		$arrResponse = xmlrpc_decode($response);
		
		if(!isset($arrResponse['responseCode'])) {

			throw new ZException("Invalid response from API Server",array("request"=>array("uri"=>$uri,"module"=>$module,"method"=>$method,"data"=>$data),"response"=>$arrResponse));

		} // end if
		
		if($arrResponse['responseCode']!="OK") {
			
			if($arrResponse['responseCode']==="INVALID_ACCESS_TOKEN") {
				
				$params = JComponentHelper::getParams("com_zibbra");
				$expiry = $params->set("api_client_expiry",0);		

				$db = &JFactory::getDBO();
				$db->setQuery("UPDATE #__extensions SET params=".$db->quote((string) $params)." WHERE `type`='component' AND `element`='com_zibbra'");
				$db->query();				
				
			} // end if
			
			return new ZApiError($arrResponse['responseCode'],$arrResponse['errorMessage']);

		} // end if
		
		return isset($arrResponse['data']) ? $arrResponse['data'] : "OK";
		
	} // end function
	
	public static function parseValue($value) {
		
		if($value==="Y") return true;
		if($value==="N") return false;
		if(is_numeric($value) && preg_match("/^[0-9]+$/",$value)) return (int) $value;
		if(is_numeric($value)) return (float) $value;
		
		return $value;
		
	} // end function
	
} // end class

?>
