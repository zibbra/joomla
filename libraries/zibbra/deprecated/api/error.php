<?php

jimport("joomla.log.log");

class ZApiError {
	
	protected $code;
	protected $message;
	private static $logger = null;
	
	public function __construct($code,$message) {
		
		self::initLogger();
		
		$this->code = $code;
		$this->message = $message;
		
		JLog::add($message,JLog::ERROR,$code);
		
	} // end function
	
	public function getCode() {
		
		return $this->code;
		
	} // end function
	
	public function getMessage() {
		
		return $this->message;
		
	} // end function
	
	private static function initLogger() {
		
		if(self::$logger===null) {
		
			self::$logger = JLog::addLogger(array("text_file"=>"com_zibbra.php"),JLog::ERROR);
		
		} // end if
		
	} // end function
	
} // end class

?>