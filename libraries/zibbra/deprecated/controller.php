<?php
/**
 * @package Zibbra
 * @author Zibbra Technologies
 */

defined("JPATH_PLATFORM") or die;

jimport("zibbra.base.object");
jimport("zibbra.base.exception");
jimport("zibbra.api.api");
jimport("zibbra.api.error");

define("ZIBBRA_INCLUDES_DIR",realpath(__DIR__."/includes"));

/**
 * Zibbra Library Controller class
 * 
 * @package Zibbra
 * @author Zibbra Technologies
 */
abstract class ZController {
	
	/**
	 * @var ZApi
	 * @since 1.0.0
	 */
	private static $api = null;
	
	/**
	 * Return singleton instance of the API
	 * 
	 * @return ZApi
	 */
	public static function getApi() {
		
		if(self::$api===null) {
			
			$params = JComponentHelper::getParams("com_zibbra");
			
			$api_client_id = $params->get("api_client_id");
			$api_client_secret = $params->get("api_client_secret");
			$api_uri = $params->get("api_uri");
			$api_key = $params->get("api_key");

			if(empty($api_client_id) || empty($api_client_secret) || empty($api_uri)) {
			
				throw new ZException("You must set the client_id, client_secret and api uri");
			
			} // end if	
	
			self::$api = new ZApi($api_client_id, $api_client_secret, $api_uri);
			
		} // end if
		
		return self::$api;

	} // end function
	
	/**
	 * Get the Joomla active language code
	 *
	 * @return string
	 * @api
	 */
	public static function getLanguage() {
	
		$languagecode = JFactory::getLanguage()->getTag();
		
		if($languagecode=="en-GB") {
		
			return "en-us";
			
		} // end if
		
		if($languagecode=="fr-FR") {
		
			return "fr-be";
			
		} // end if		
		
		if($languagecode=="nl-NL" || !preg_match("/^[a-z]{2}-[a-z]{2}$/",strtolower($languagecode))) {
		
			return "nl-be";
			
		} // end if
	
		return strtolower($languagecode);
	
	} // end function
	
} // end class

?>
