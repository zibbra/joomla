<?php

jimport("joomla.log.log");

class ZException extends Exception {	
	
	private $debug = "";
	private static $logger = null;
	
	public function __construct($msg,$params=null) {
		
		self::initLogger();
	
		parent::__construct("Zibbra Exception: ".$msg);
		
		syslog(LOG_DEBUG,$this->getMessage());		
		JLog::add($msg,JLog::ERROR,"Exception");
		
		$this->debug = "Exception: ".$this->getMessage()."\n";
		$this->debug .= "\n";
	
		if($params!==null) {
		
			if(is_array($params)) {
			
				$this->debug .= print_r($params,true);
				
			}else{
			
				$this->debug .= $params;
				
			} // end if
			
			$this->debug .= "\n";
		
		} // end if
		
		$this->debug .= "Backtrace:\n";
		$this->debug .= "\n";
		
		array_walk($this->getTrace(),array($this,"printBacktrace"));
		
		$app =& JFactory::getApplication();
		
		$logdir = realpath($app->getCfg("log_path"));
		$logfile = $logdir."/".date("YmdHis")."_zibbra_exception.log";
		
		file_put_contents($logfile,$this->debug,FILE_APPEND);
		
	} // end function
	
	private function printBacktrace($e,$i) {
	
		$file = str_replace($_SERVER['DOCUMENT_ROOT'],"",$e['file']).":".$e['line'];
	
		$this->debug .= str_pad($file,70," ").(isset($e['class']) ? $e['class'] : "").(isset($e['type']) ? $e['type'] : "").$e['function']."()\n";
		
	} // end function
	
	private static function initLogger() {
		
		if(self::$logger===null) {
		
			self::$logger = JLog::addLogger(array("text_file"=>"com_zibbra.php"),JLog::ERROR);
		
		} // end if
		
	} // end function
	
} // end class

?>