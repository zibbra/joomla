<?php

class ZObject {
	
	public static function call($module,$method,$data=array()) {
		
		return self::load($module,$method,$data);
		
	} // end function
	
	public static function load($module,$method,$data=array()) {
		
		$api = ZController::getApi();
		
		return $api->call($module,$method,$data);
		
	} // end function
	
	public static function parse($obj,$data,$prefix=false) {
		
		foreach($data as $key=>$value) {
			
			$prop = $prefix!==false ? str_replace($prefix."_","",$key) : $key;
				
			if(property_exists($obj,$prop)) $obj->$prop = ZApi::parseValue($value);
				
		} // end foreach
		
	} // end function
	
} // end class

?>