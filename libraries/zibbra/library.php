<?php
/**
 * Main file for the Zibbra library
 * 
 * @author Zibbra <support@zibbra.com>
 * @package Joomla\Libraries
 */

/**
 * Check if this file is included by the Joomla core
 */
defined("JPATH_PLATFORM") or die;

/**
 * BASE directory of the joomla library
 * 
 * @var string
 */
define("ZIBBRA_BASE_DIR", realpath(__DIR__));

// Import joomla libraries

jimport("joomla.session.session");
jimport("joomla.log.log");

// Include the library core files

require_once(ZIBBRA_BASE_DIR."/lib/library.php");
require_once(ZIBBRA_BASE_DIR."/lib/adapter/joomla.php");

/**
 * Main Zibbra class
 * 
 * @author Zibbra <support@zibbra.com>
 * @package Joomla\Libraries
 */
class Zibbra {
	
	/**
	 * Instance of the library
	 * 
	 * @var ZLibrary
	 */
	private $library;
	
	/**
	 * Singleton instance
	 * 
	 * @var Zibbra
	 */
	private static $instance;
	
	/**
	 * Constructor
	 */
	private function __construct() {

		// Fetch information from Joomla component

		$app =& JFactory::getApplication();
		$logdir = realpath($app->getCfg("log_path"));
		$params = JComponentHelper::getParams("com_zibbra");
		$api_client_id = $params->get("api_client_id");
		$api_client_secret = $params->get("api_client_secret");

		// Instantiate the adapter
		
		$adapter = new ZLibrary_Adapter_Joomla();
		$adapter->setJoomlaSession(JSession::getInstance("none",array()));
		$adapter->setLogDir($logdir);
		
		// Instantiate the library
		
		$this->library = ZLibrary::getInstance($adapter);
		$this->library->setApiClientId($api_client_id);
		$this->library->setApiClientSecret($api_client_secret);
		
	} // end function
	
	/**
	 * Returns the library
	 * 
	 * @return ZLibrary
	 */
	public function getLibrary() {
		
		return $this->library;
		
	} // end function
	
	/**
	 * Returns the singleton instance of the Zibbra integration
	 * 
	 * @return Zibbra
	 */
	public static function getInstance() {
		
		if(self::$instance===null) {
			
			self::$instance = new Zibbra();
			
		} // end if
		
		return self::$instance;
		
	} // end if
	
} // end class

/**
 * Instantiate Zibbra
 * 
 * @var Zibbra Local variable $zibbra available througout the Joomla application
 * @todo Is this even used???
 */

$zibbra = Zibbra::getInstance();

?>