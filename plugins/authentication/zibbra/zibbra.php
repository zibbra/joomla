<?php
/**
 * plugins/authentication/zibbra/zibbra.php
 *
 * @package Zibbra.Plugin
 * @subpackage Authentication
 * @author Zibbra Technologies <info@zibbra.com>
 * @copyright Copyright (C) 2013 Zibbra Technologies - All rights reserved.
 * @license BSD
 */

// No direct access to this file

defined("_JEXEC") or die();

// Import joomla plugin library

jimport("joomla.event.plugin");
 
/**
 * Zibbra Authentication Plugin
 *
 * @package Zibbra.Plugin
 * @subpackage Authentication
 */
class plgAuthenticationZibbra extends JPlugin {
	
	/**
	 * Constructor
	 *
	 * @param string $subject
	 * @param array $config
	 */
	public function __construct(&$subject,$config) {
		
		parent::__construct($subject,$config);
		
	} // end function

	/**
	 * This method should handle any authentication and report back to the subject
	 *
	 * @param array $credentials Array holding the user credentials
	 * @param array $options Array of extra options
	 * @param object $response Authentication response object
	 * @return	boolean
	 */
	public function onUserAuthenticate($credentials,$options,&$response) {
		
		// Disable backend logins for Zibbra customers
		
		if(stripos($options['entry_url'],"administrator")) {
			
			if(strcasecmp($options['group'],"Public Backend")==1) {
				
				$response->status = JAUTHENTICATE_STATUS_FAILURE;
				$response->error_message = "You are not allow to login here.";
				return false;
				
			} // end if
			
		} // end if
		
		// Check if the library is loaded
		
		if(!class_exists("ZCustomer")) {
			
			return false;
			
		} // end if
		
		// Perform login
		
		try {
		
			$success = ZCustomer::login($credentials['username'],$credentials['password']);
		
		} catch(Exception $e) {
			
		} // end try-catch
		
		if(!ZCustomer::isAuthenticated()) {
			
			$response->status = JAUTHENTICATE_STATUS_FAILURE;
			$response->error_message = JText::_("COM_ZIBBRA_AUTHENTICATION_FAILED");
			return false;
			
		} // end if
		
		$customer = ZCustomer::load();
		$contact = $customer->getPrimaryContact();
		$billing_address = $customer->getBillingAddress();
		
		$response->type = "Zibbra";
		$response->status = JAUTHENTICATE_STATUS_SUCCESS;
		$response->username = $credentials['username'];
		$response->email = $credentials['username'];
		$response->name = $contact->getFirstname();
		$response->country = $billing_address->getCountrycode();
		$response->language = preg_replace("/_*$/","",$customer->getLanguagecode());
		$response->fullname = $contact->getFirstname()." ".$contact->getLastname();
		
		if(isset($options['remember']) && $options['remember']) {
			
			jimport("joomla.utilities.simplecrypt");
	
			// Create the encryption key, apply extra hardening using the user agent string
			
			$key = JApplication::getHash(@$_SERVER['HTTP_USER_AGENT']);
	
			$crypt = new JSimpleCrypt($key);
			$rcookie = $crypt->encrypt(serialize($credentials));
			$lifetime = time() + (30 * 24 * 60 * 60); // 30 days
	
			// Use domain and path set in config for cookie if it exists
			
			$app = JFactory::getApplication();
			$cookie_domain = $app->getCfg("cookie_domain","");
			$cookie_path = $app->getCfg("cookie_path","/");
			setcookie(JApplication::getHash("JLOGIN_REMEMBER"),$rcookie,$lifetime,$cookie_path,$cookie_domain);
			
		} // end if
		
		return true;
		
	} // end function
	
} // end class

?>
