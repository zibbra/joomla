<?php

// No direct access

defined("_JEXEC") or die("No access");

class plgSearchZibbra extends JPlugin {
	
	function onContentSearchAreas() {
		
		static $areas = array(
			"catalog"=>"Product Catalog"
		);
		return $areas;
		
	} // end function
	
	function onContentSearch($text,$phrase="",$ordering="",$areas=null) {
		
		$products = ZProduct::search($text);
		$results = array();
		
		if(is_array($products)) {
			
			foreach($products as $product) {
		
				$item = new StdClass();
				
				$item->title = $product->getName();
				$item->text = $product->getShortDescription();
				$item->section = "Product Catalog";
				$item->href = $product->getURL();
				
				$results[] = $item;
				
			} // end foreach
			
		} // end if
		
		return $results;
		
	} // end function
	
} // end class