<?php
/**
 * plugins/system/zibbra/zibbra.php
 *
 * @package Zibbra.Plugin
 * @subpackage System
 * @author Zibbra Technologies <info@zibbra.com>
 * @copyright Copyright (C) 2012 Zibbra Technologies - All rights reserved.
 * @license BSD
 */

// No direct access to this file

defined("_JEXEC") or die();

// Import joomla plugin library

jimport("joomla.plugin.plugin");

// Import Zibbra API library
		
jimport("zibbra.library");
 
/**
 * Zibbra Authentication Plugin
 *
 * @package Zibbra.Plugin
 * @subpackage System
 */
class plgSystemZibbra extends JPlugin {
	
	public function onAfterInitialise() {

		$app = JFactory::getApplication();
		$user = JFactory::getUser();
		
		$jinput = JFactory::getApplication()->input;
		
		if(!is_null($jinput->get->get("clear_cache",null,null))) {
		
			$files = array_diff(scandir(getcwd()."/cache/"),array("index.html",".",".."));			
			foreach($files as $file) unlink(getcwd()."/cache/".$file);
		
		} // end if
		
		// No remember me for admin
		
		if($app->isAdmin()) return;
		
		if($user->get("guest")) {
			
			/*
			echo "<pre>TODO: check that remember me works! (plugins/system/zibbra/zibbra.php:43)";
			var_dump($user);
			exit;
			*/
				
			jimport("joomla.utilities.utility");
			$hash = JApplication::getHash("JLOGIN_REMEMBER");
			
			if(($str = JRequest::getString($hash,"","cookie",JREQUEST_ALLOWRAW | JREQUEST_NOTRIM))!==false) {
				
				jimport("joomla.utilities.simplecrypt");
				
				// Create the encryption key, apply extra hardening using the user agent string.
				// Since we're decoding, no UA validity check is required.
				$key = JApplication::getHash(@$_SERVER['HTTP_USER_AGENT']);
			
				$crypt = new JSimpleCrypt($key);
				$str = $crypt->decrypt($str);
				$cookieData = @unserialize($str);
				// Deserialized cookie could be any object structure, so make sure the
				// credentials are well structured and only have user and password.
				$credentials = array();
				$filter = JFilterInput::getInstance();
				$goodCookie = true;
					
				if(isset($cookieData['username']) && is_string($cookieData['username'])) {
					
					$credentials['username'] = $filter -> clean($cookieData['username'], 'username');
					
				}else{
					
					$goodCookie = false;
					
				} // end if
				
				if(isset($cookieData['password']) && is_string($cookieData['password'])) {
					
					$credentials['password'] = $filter -> clean($cookieData['password'], 'string');
					
				}else{
					
					$goodCookie = false;
					
				} // end if
				
				if(!$goodCookie || !$app->login($credentials,array("silent"=>true))) {
					
					$config = JFactory::getConfig();
					$cookie_domain = $config->get("cookie_domain","");
					$cookie_path = $config->get("cookie_path","/");
					
					// Clear the remember me cookie
					
					setcookie(JApplication::getHash("JLOGIN_REMEMBER"),false,time()-86400,$cookie_path,$cookie_domain);
					
				} // end if
				
				/*
				echo "<pre>";
				var_dump($key);
				var_dump($crypt);
				var_dump($str);
				var_dump($cookieData);
				exit;
				*/
			} // end if
				
		} // end if
		
	} // end function
	
} // end class

?>